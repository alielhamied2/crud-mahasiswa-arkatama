<div>
    <x-mollecules.modal id="add-mahasiswa_modal" action="store" wire:ignore.self>
      <x-slot:title>Tambah Mahasiswa</x-slot:title>
      <div class="">
        <div class="mb-6">
          <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
          <x-atoms.input name="nama_lengkap" wire:model='nama_lengkap' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>NIM</x-atoms.form-label>
          <x-atoms.input name="NIM" wire:model='NIM' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
          <x-atoms.radio-group>
            <x-atoms.radio id="locat-1" name="jenis_kelamin" active value="L" wire:model='jenis_kelamin'>Laki-Laki</x-atoms.radio>
            <x-atoms.radio id="locat-0" name="jenis_kelamin" value="P" wire:model='jenis_kelamin'>Perempuan</x-atoms.radio>
          </x-atoms.radio-group>
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
          <x-atoms.input name="tempat_lahir" wire:model='tempat_lahir' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
          <x-atoms.input name="tanggal_lahir" wire:model='tanggal_lahir' type="date" />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Email</x-atoms.form-label>
          <x-atoms.input name="email" wire:model='email' type="email" />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>No. Telepon</x-atoms.form-label>
          <x-atoms.input name="no_telepon" wire:model='no_telepon' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Alamat Lengkap</x-atoms.form-label>
          <x-atoms.textarea name="alamat_lengkap" wire:model='alamat_lengkap' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Foto Profil</x-atoms.form-label>
          <x-atoms.input type="file" name="foto_profil" wire:model='foto_profil' />
        </div>
        <x-slot:footer>
          <button class="btn-primary btn" type="submit">Submit</button>
        </x-slot:footer>
      </div>
    </x-mollecules.modal>
    <x-mollecules.modal id="edit-mahasiswa_modal" action="update" wire:ignore.self>
      <x-slot:title>Edit Mahasiswa</x-slot:title>
      <div class="">
        <div class="mb-6">
          <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
          <x-atoms.input name="nama_lengkap" wire:model='nama_lengkap' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>NIM</x-atoms.form-label>
          <x-atoms.input name="NIM" wire:model='NIM' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
          <x-atoms.radio-group>
            <x-atoms.radio id="locat-1" name="jenis_kelamin" value="L" wire:model='jenis_kelamin'>Laki-Laki</x-atoms.radio>
            <x-atoms.radio id="locat-0" name="jenis_kelamin" value="P" wire:model='jenis_kelamin'>Perempuan</x-atoms.radio>
          </x-atoms.radio-group>
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
          <x-atoms.input name="tempat_lahir" wire:model='tempat_lahir' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
          <x-atoms.input name="tanggal_lahir" wire:model='tanggal_lahir' type="date" />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Email</x-atoms.form-label>
          <x-atoms.input name="email" wire:model='email' type="email" />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>No. Telepon</x-atoms.form-label>
          <x-atoms.input name="no_telepon" wire:model='no_telepon' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Alamat Lengkap</x-atoms.form-label>
          <x-atoms.textarea name="alamat_lengkap" wire:model='alamat_lengkap' />
        </div>
        <div class="mb-6">
          <x-atoms.form-label required>Foto Profil</x-atoms.form-label>
          <x-atoms.input type="file" name="foto_profil" wire:model='foto_profil' />
        </div>
        <x-slot:footer>
          <button class="btn-primary btn" type="submit">Submit</button>
        </x-slot:footer>
      </div>
    </x-mollecules.modal>
  </div>
  
  @push('scripts')
    <script>
      document.addEventListener('livewire:initialized', () => {
        function refreshTable() {
          window.LaravelDataTables['mahasiswas-table'].ajax.reload();
        };
        @this.on('mahasiswa-added', () => {
          $('#add-mahasiswa_modal').modal('hide');
          refreshTable();
          location.reload();
        });
        @this.on('mahasiswa-deleted', () => {
          refreshTable();
          location.reload();
        });
        @this.on('mahasiswa-edit', () => {
          $('#edit-mahasiswa_modal').modal('show');
          refreshTable();
        });
        @this.on('mahasiswa-updated', () => {
          $('#edit-mahasiswa_modal').modal('hide');
          refreshTable();
          location.reload();
        });
      });
    </script>
  @endpush
  