<x-layouts.app>
    <x-slot:title>Mahasiswa</x-slot:title>
    <livewire:pages.admin.mahasiswas.mahasiswa-modal />

    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <span class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></span>
                    <input type="text" data-table-id="mahasiswas-table" data-kt-user-table-filter="search" data-action-search
                        class="form-control form-control-solid w-250px ps-13 me-4" placeholder="Cari Mahasiswa" id="mySearchInput" />

                    <x-atoms.select data-table-id="mahasiswas-table" class="form-control form-control-solid w-250px" id="gender-filter-select">
                        <option value="">Jenis Kelamin</option>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                    </x-atoms.select>
                </div>
            </div>
            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    <!--begin::Add user-->
                    @can('admin-mahasiswa-create')
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#add-mahasiswa_modal">
                            <i class="ki-duotone ki-plus fs-2"></i>
                            <span>Tambah Mahasiswa</span>
                        </button>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card-body py-4">
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>
    
    @push('css')
        <link rel="stylesheet" href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
    @endpush

    @push('scripts')
    {{ $dataTable->scripts() }}
    <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            // Inisialisasi DataTable
            var table = $('#mahasiswas-table').DataTable();

            // Fungsi untuk melakukan pencarian
            $('#mySearchInput').on('keyup', function() {
                table.search(this.value).draw();
            });

            // Fungsi untuk melakukan filter berdasarkan kolom tertentu
            $('#filter-by-select').on('change', function() {
                var column = parseInt($(this).val()); // Ambil nilai dari combobox
                var keyword = $('#mySearchInput').val(); // Ambil kata kunci pencarian
                table.column(column).search(keyword).draw();
            });

            // Fungsi untuk melakukan filter berdasarkan jenis kelamin
            $('#gender-filter-select').on('change', function() {
                var gender = $(this).val(); // Ambil nilai jenis kelamin
                table.column(4).search(gender).draw(); // Disesuaikan dengan indeks kolom jenis kelamin di tabel
            });
        });
    </script>
@endpush
</x-layouts.app>
