<?php

namespace App\DataTables;

use App\Models\MahasiswaModel;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Livewire\Livewire;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class MahasiswasDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
{
    return (new EloquentDataTable($query))
        ->addColumn('action', 'mahasiswas.action')
        ->addIndexColumn()
        ->addColumn('foto_profil', function(MahasiswaModel $mahasiswa) {
            return '<img src="'.asset('storage/'.$mahasiswa['foto_profil']).'" class="rounded-full" alt="Foto Profil" width="45">';
        })
        ->addColumn('action', function(MahasiswaModel $val){
            return Livewire::mount('pages.admin.mahasiswas.mahasiswas-table-action', ['mahasiswas' => $val]);
        })
        ->rawColumns(['action', 'foto_profil'])
        ->setRowId('id');
}

    /**
     * Get the query source of dataTable.
     */
    public function query(MahasiswaModel $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('mahasiswas-table')
            ->columns($this->getColumns())
            ->minifiedAjax(script: "
                data._token = '" . csrf_token() . "';
                data._p = 'POST';
            ")
            ->dom('rt' . "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",)
            ->addTableClass('table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-gray-600 fw-semibold')
            ->setTableHeadClass('text-start text-muted fw-bold fs-7 text-uppercase gs-0')
            ->orderBy(2)
            ->drawCallbackWithLivewire(file_get_contents(public_path('assets/js/custom/table/_init.js')))
            ->select(false)
            ->buttons([]);
    }
    

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
{
    return [
        Column::computed("DT_RowIndex")
            ->title("No.")
            ->width(20),
        Column::make('foto_profil')
            ->title('Foto Profil'),
        Column::make('nama_lengkap')->title("Nama Lengkap"),
        Column::make("NIM"),
        Column::make('jenis_kelamin')->title("Jenis Kelamin"),
        Column::make('tempat_lahir')->title("Tempat Lahir"),
        Column::make('tanggal_lahir')->title("Tanggal Lahir"),
        Column::make('email')->title("Email"),
        Column::make('no_telepon')->title("No. Telepon"),
        Column::make('alamat_lengkap')->title("Alamat Lengkap"),
        Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
    ];
}

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Mahasiswas_' . date('YmdHis');
    }
}
