<?php

namespace App\Livewire\Pages\Admin\Mahasiswas;

use App\Models\MahasiswaModel;
use Livewire\Component;
use Livewire\Attributes\On;
use Livewire\Attributes\Rule;
use Livewire\WithFileUploads;

class MahasiswaModal extends Component
{
    use WithFileUploads;
    public $id;

    #[Rule('required|min:3|max:255')]
    public $nama_lengkap = "";
    #[Rule('required|max:10|unique:mahasiswas')]
    public $NIM = "";
    public $old_NIM;
    #[Rule('required')]
    public $jenis_kelamin = "L";
    #[Rule('required|max:255')]
    public $tempat_lahir = "";
    #[Rule('required')]
    public $tanggal_lahir = "";
    #[Rule('required|email|unique:mahasiswas')]
    public $email = "";
    #[Rule('required|numeric')]
    public $no_telepon = "";
    #[Rule('required')]
    public $alamat_lengkap = "" ;
    #[Rule('required|image|max:1024')]
    public $foto_profil;
    public $old_foto_profil;


    #[On('store')]
    public function store(){
        $this->validate();
        $student = new MahasiswaModel();
            $student->nama_lengkap = $this->nama_lengkap;
            $student->NIM = $this->NIM;
            $student->jenis_kelamin = $this->jenis_kelamin;
            $student->tempat_lahir = $this->tempat_lahir;
            $student->tanggal_lahir = $this->tanggal_lahir;
            $student->email = $this->email;
            $student->no_telepon = $this->no_telepon;
            $student->alamat_lengkap = $this->alamat_lengkap;
            if ($this->foto_profil) {
                $student->foto_profil = $this->foto_profil->store('photos', 'public');
            }
            $student->save();
        // $newMahasiswa = MahasiswaModel::create($validated);
        if($student){
            $this->dispatch("mahasiswa-added");
        }
    }
    
    #[On('delete')] 
    public function delete($id){
        $mahasiswa = MahasiswaModel::find($id);
        if($mahasiswa->delete()){
            $this->dispatch("mahasiswa-deleted");
        }
        
    }

    #[On('edit')]
    public function edit($id){
        $student = MahasiswaModel::find($id);
        $this->id = $id;
        $this->nama_lengkap = $student->nama_lengkap;
        $this->NIM = $student->NIM;
        $this->old_NIM = $student->NIM;
        $this->jenis_kelamin = $student->jenis_kelamin;
        $this->tempat_lahir = $student->tempat_lahir;
        $this->tanggal_lahir = $student->tanggal_lahir;
        $this->email = $student->email;
        $this->no_telepon = $student->no_telepon;
        $this->alamat_lengkap = $student->alamat_lengkap;
        $this->old_foto_profil = $student->foto_profil;
        $this->foto_profil = null;
        $this->dispatch("mahasiswa-edit");
    }

    public function update(){
        $rules = [
            'nama_lengkap' => 'required|min:3|max:255',
            'NIM' => 'required|unique:students',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'email' => 'required|email:dns',
            'no_telepon' => 'required|numeric',
            'alamat_lengkap' => 'required',
            'foto_profil' => 'image|max:2048', // max 2MB
        ];
    
        // Jika dalam mode update dan nim tidak berubah, aturan unique diabaikan
        if ($this->NIM == $this->old_NIM) {
            unset($rules['NIM']);
        }
        if ($this->foto_profil == null) {
            unset($rules['foto_profil']);
        }else{  
            unlink('storage/'.$this->old_foto_profil);
        }
        $this->validate($rules);
        if(MahasiswaModel::find($this->id)->update([
            'nama_lengkap' => $this->nama_lengkap,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tempat_lahir' => $this->tempat_lahir,
            'tanggal_lahir' => $this->tanggal_lahir,
            'email' => $this->email,
            'no_telepon' => $this->no_telepon,
            'alamat_lengkap' => $this->alamat_lengkap,
            'foto_profil'=> $this->foto_profil ? $this->foto_profil->store('photos','public') : $this->old_foto_profil
            ])){
            $this->reset();
            $this->dispatch("mahasiswa-updated");
        }
    }

    public function render()
    {
        return view('livewire.pages.admin.mahasiswas.mahasiswa-modal');
    }

    
}
