<?php

namespace App\Livewire\Pages\Admin\Mahasiswas;

use Livewire\Component;

class MahasiswasTableAction extends Component
{
    public $mahasiswas;
    public function mount($mahasiswas){
        $this->mahasiswas = $mahasiswas;
    }
    
    public function render()
    {
        return view('livewire.pages.admin.mahasiswas.mahasiswas-table-action');
    }
}
