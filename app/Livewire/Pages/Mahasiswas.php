<?php

namespace App\Livewire;

use App\Models\MahasiswaModel;
use Livewire\Component;

class Mahasiswas extends Component
{
    public $id,$nama,$old_nim, $nim, $jenis_kelamin, $tempat_lahir, $tanggal_lahir, $email, $nomor_telepon, $alamat_lengkap, $foto_profil,$old_foto_profil;
    public function render()
    {
        return view('livewire.mahasiswas',[
            'mahasiswas' => MahasiswaModel::paginate(2),
            'parents' => MahasiswaModel::where('parent_id', null)->get(),
        ]);
    }

    public function store(){
        $student = MahasiswaModel::create([
            'id' => uuid_create(),
            'nama_lengkap' => $this->nama,
            'NIM' => $this->nim,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tempat_lahir' => $this->tempat_lahir,
            'tanggal_lahir' => $this->tanggal_lahir,
            'email' => $this->email,
            'no_telepon' => $this->nomor_telepon,
            'alamat_lengkap' => $this->alamat_lengkap,
            'foto_profil' => $this->foto_profil->store('photos', 'public')
        ]);
        $this->dispatch('mahasiswa-added');
        back();
    }

    public function update(){
        $id = $this->id;
        $student = MahasiswaModel::find($id);
        $student->update([
            'nama_lengkap' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tempat_lahir' => $this->tempat_lahir,
            'tanggal_lahir' => $this->tanggal_lahir,
            'email' => $this->email,
            'no_telepon' => $this->nomor_telepon,
            'alamat_lengkap' => $this->alamat_lengkap,
        ]);
        if ($this->foto_profil) {
            unlink('storage/'.$this->old_foto_profil);
            $student->update([
                'foto_profil' => $this->foto_profil->store('photos', 'public'),
            ]);
        }
        if($student->NIM !== $this->nim) {
                $student->update([
            'NIM' => $this->nim,
            ]);
        }
        $this->dispatch('mahasiswa-updated');
        back();
    }

    public function delete($id){
        $student = MahasiswaModel::find($id);
        unlink('storage/'.$student->foto_profil);
        $student->delete();
        back();
    }

    public function show($id){
        $this->id = $id;
        $student = MahasiswaModel::find($id);
        $this->nama = $student->nama_lengkap;
        $this->nim = $student->NIM;
        $this->jenis_kelamin = $student->jenis_kelamin;
        $this->tempat_lahir = $student->tempat_lahir;
        $this->tanggal_lahir = $student->tanggal_lahir;
        $this->email = $student->email;
        $this->nomor_telepon = $student->no_telepon;
        $this->alamat_lengkap = $student->alamat_lengkap;
        $this->old_foto_profil = $student->foto_profil;
        $this->dispatch('show-mahasiswa');
    }
}
