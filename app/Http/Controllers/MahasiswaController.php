<?php

namespace App\Http\Controllers;

use App\DataTables\MahasiswasDataTable;

class MahasiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:admin-mahasiswa');
    }

    public function index(MahasiswasDataTable $datatable)
    {
        return $datatable->render("pages.admin.mahasiswa.index");
        
    }
}
