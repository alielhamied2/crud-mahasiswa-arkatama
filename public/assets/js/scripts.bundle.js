"use strict";

// Class definition
var KTApp = function () {
    var initialized = false;
    var select2FocusFixInitialized = false;
    var countUpInitialized = false;

    var createBootstrapTooltip = function (el, options) {
        if (el.getAttribute("data-kt-initialized") === "1") {
            return;
        }

        var delay = {};

        // Handle delay options
        if (el.hasAttribute('data-bs-delay-hide')) {
            delay['hide'] = el.getAttribute('data-bs-delay-hide');
        }

        if (el.hasAttribute('data-bs-delay-show')) {
            delay['show'] = el.getAttribute('data-bs-delay-show');
        }

        if (delay) {
            options['delay'] = delay;
        }

        // Check dismiss options
        if (el.hasAttribute('data-bs-dismiss') && el.getAttribute('data-bs-dismiss') == 'click') {
            options['dismiss'] = 'click';
        }

        // Initialize popover
        var tp = new bootstrap.Tooltip(el, options);        

        // Handle dismiss
        if (options['dismiss'] && options['dismiss'] === 'click') {
            // Hide popover on element click
            el.addEventListener("click", function (e) {
                tp.hide();
            });
        }

        el.setAttribute("data-kt-initialized", "1");

        return tp;
    }

    var createBootstrapTooltips = function () {
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));

        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            createBootstrapTooltip(tooltipTriggerEl, {});
        });
    }

    var createBootstrapPopover = function (el, options) {
        if (el.getAttribute("data-kt-initialized") === "1") {
            return;
        }

        var delay = {};

        // Handle delay options
        if (el.hasAttribute('data-bs-delay-hide')) {
            delay['hide'] = el.getAttribute('data-bs-delay-hide');
        }

        if (el.hasAttribute('data-bs-delay-show')) {
            delay['show'] = el.getAttribute('data-bs-delay-show');
        }

        if (delay) {
            options['delay'] = delay;
        }

        // Handle dismiss option
        if (el.getAttribute('data-bs-dismiss') == 'true') {
            options['dismiss'] = true;
        }

        if (options['dismiss'] === true) {
            options['template'] = '<div class="popover" role="tooltip"><div class="popover-arrow"></div><span class="popover-dismiss btn btn-icon"></span><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        }

        // Initialize popover
        var popover = new bootstrap.Popover(el, options);

        // Handle dismiss click
        if (options['dismiss'] === true) {
            var dismissHandler = function (e) {
                popover.hide();
            }

            el.addEventListener('shown.bs.popover', function () {
                var dismissEl = document.getElementById(el.getAttribute('aria-describedby'));
                dismissEl.addEventListener('click', dismissHandler);
            });

            el.addEventListener('hide.bs.popover', function () {
                var dismissEl = document.getElementById(el.getAttribute('aria-describedby'));
                dismissEl.removeEventListener('click', dismissHandler);
            });
        }

        el.setAttribute("data-kt-initialized", "1");

        return popover;
    }

    var createBootstrapPopovers = function () {
        var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));

        var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
            createBootstrapPopover(popoverTriggerEl, {});
        });
    }

    var createBootstrapToasts = function () {
        var toastElList = [].slice.call(document.querySelectorAll('.toast'));
        var toastList = toastElList.map(function (toastEl) {
            if (toastEl.getAttribute("data-kt-initialized") === "1") {
                return;
            }

            toastEl.setAttribute("data-kt-initialized", "1");

            return new bootstrap.Toast(toastEl, {})
        });
    }

    var createButtons = function () {
        var buttonsGroup = [].slice.call(document.querySelectorAll('[data-kt-buttons="true"]'));

        buttonsGroup.map(function (group) {
            if (group.getAttribute("data-kt-initialized") === "1") {
                return;
            }

            var selector = group.hasAttribute('data-kt-buttons-target') ? group.getAttribute('data-kt-buttons-target') : '.btn';
            var activeButtons = [].slice.call(group.querySelectorAll(selector));

            // Toggle Handler
            KTUtil.on(group, selector, 'click', function (e) {
                activeButtons.map(function (button) {
                    button.classList.remove('active');
                });

                this.classList.add('active');
            });       

            group.setAttribute("data-kt-initialized", "1");
        });
    }

    var createDateRangePickers = function() {
        // Check if jQuery included
        if (typeof jQuery == 'undefined') {
            return;
        }

        // Check if daterangepicker included
        if (typeof $.fn.daterangepicker === 'undefined') {
            return;
        }

        var elements = [].slice.call(document.querySelectorAll('[data-kt-daterangepicker="true"]'));
        var start = moment().subtract(29, 'days');
        var end = moment();
        
        elements.map(function (element) {
            if (element.getAttribute("data-kt-initialized") === "1") {
                return;
            }

            var display = element.querySelector('div');
            var attrOpens  = element.hasAttribute('data-kt-daterangepicker-opens') ? element.getAttribute('data-kt-daterangepicker-opens') : 'left';
            var range = element.getAttribute('data-kt-daterangepicker-range');

            var cb = function(start, end) {
                var current = moment();

                if (display) {
                    if ( current.isSame(start, "day") && current.isSame(end, "day") ) {
                        display.innerHTML = start.format('D MMM YYYY');
                    } else {
                        display.innerHTML = start.format('D MMM YYYY') + ' - ' + end.format('D MMM YYYY');
                    }                    
                }
            }

            if ( range === "today" ) {
                start = moment();
                end = moment();
            }

            $(element).daterangepicker({
                startDate: start,
                endDate: end,
                opens: attrOpens,
                ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

            element.setAttribute("data-kt-initialized", "1");
        });
    }

    var createSelect2 = function () {
        // Check if jQuery included
        if (typeof jQuery == 'undefined') {
            return;
        }

        // Check if select2 included
        if (typeof $.fn.select2 === 'undefined') {
            return;
        }

        var elements = [].slice.call(document.querySelectorAll('[data-control="select2"], [data-kt-select2="true"]'));

        elements.map(function (element) {
            if (element.getAttribute("data-kt-initialized") === "1") {
                return;
            }

            var options = {
                dir: document.body.getAttribute('direction')
            };

            if (element.getAttribute('data-hide-search') == 'true') {
                options.minimumResultsForSearch = Infinity;
            }

            $(element).select2(options);

            // Handle Select2's KTMenu parent case
            if (element.hasAttribute('data-dropdown-parent') && element.hasAttribute('multiple')) {
                var parentEl = document.querySelector(element.getAttribute('data-dropdown-parent'));

                if (parentEl && parentEl.hasAttribute("data-kt-menu")) {
                    var menu = new KTMenu(parentEl);

                    if (menu) {
                        $(element).on('select2:unselect', function (e) {
                            element.setAttribute("data-multiple-unselect", "1");
                        });

                        menu.on("kt.menu.dropdown.hide", function(item) {
                            if (element.getAttribute("data-multiple-unselect") === "1") {
                                element.removeAttribute("data-multiple-unselect");
                                return false;
                            }
                        });
                    }                    
                }                
            }

            element.setAttribute("data-kt-initialized", "1");
        });
    }

    var createAutosize = function () {
        if (typeof autosize === 'undefined') {
            return;
        }

        var inputs = [].slice.call(document.querySelectorAll('[data-kt-autosize="true"]'));

        inputs.map(function (input) {
            if (input.getAttribute("data-kt-initialized") === "1") {
                return;
            }

            autosize(input);

            input.setAttribute("data-kt-initialized", "1");
        });
    }

    var createCountUp = function () {
        if (typeof countUp === 'undefined') {
            return;
        }

        var elements = [].slice.call(document.querySelectorAll('[data-kt-countup="true"]:not(.counted)'));

        elements.map(function (element) {
            if (KTUtil.isInViewport(element) && KTUtil.visible(element)) {
                if (element.getAttribute("data-kt-initialized") === "1") {
                    return;
                }

                var options = {};

                var value = element.getAttribute('data-kt-countup-value');
                value = parseFloat(value.replace(/,/g, ""));

                if (element.hasAttribute('data-kt-countup-start-val')) {
                    options.startVal = parseFloat(element.getAttribute('data-kt-countup-start-val'));
                }

                if (element.hasAttribute('data-kt-countup-duration')) {
                    options.duration = parseInt(element.getAttribute('data-kt-countup-duration'));
                }

                if (element.hasAttribute('data-kt-countup-decimal-places')) {
                    options.decimalPlaces = parseInt(element.getAttribute('data-kt-countup-decimal-places'));
                }

                if (element.hasAttribute('data-kt-countup-prefix')) {
                    options.prefix = element.getAttribute('data-kt-countup-prefix');
                }

                if (element.hasAttribute('data-kt-countup-separator')) {
                    options.separator = element.getAttribute('data-kt-countup-separator');
                }

                if (element.hasAttribute('data-kt-countup-suffix')) {
                    options.suffix = element.getAttribute('data-kt-countup-suffix');
                }

                var count = new countUp.CountUp(element, value, options);

                count.start();

                element.classList.add('counted');

                element.setAttribute("data-kt-initialized", "1");
            }
        });
    }

    var createCountUpTabs = function () {
        if (typeof countUp === 'undefined') {
            return;
        }

        if (countUpInitialized === false) {
            // Initial call
            createCountUp();

            // Window scroll event handler
            window.addEventListener('scroll', createCountUp);
        }      

        // Tabs shown event handler
        var tabs = [].slice.call(document.querySelectorAll('[data-kt-countup-tabs="true"][data-bs-toggle="tab"]'));
        tabs.map(function (tab) {
            if (tab.getAttribute("data-kt-initialized") === "1") {
                return;
            }

            tab.addEventListener('shown.bs.tab', createCountUp);

            tab.setAttribute("data-kt-initialized", "1");
        });

        countUpInitialized = true;
    }

    var createTinySliders = function () {
        if (typeof tns === 'undefined') {
            return;
        }

        // Sliders
        const elements = Array.prototype.slice.call(document.querySelectorAll('[data-tns="true"]'), 0);

        if (!elements && elements.length === 0) {
            return;
        }

        elements.forEach(function (el) {
            if (el.getAttribute("data-kt-initialized") === "1") {
                return;
            }

            initTinySlider(el);

            el.setAttribute("data-kt-initialized", "1");
        });
    }

    var initTinySlider = function (el) {
        if (!el) {
            return;
        }

        const tnsOptions = {};

        // Convert string boolean
        const checkBool = function (val) {
            if (val === 'true') {
                return true;
            }
            if (val === 'false') {
                return false;
            }
            return val;
        };

        // get extra options via data attributes
        el.getAttributeNames().forEach(function (attrName) {
            // more options; https://github.com/ganlanyuan/tiny-slider#options
            if ((/^data-tns-.*/g).test(attrName)) {
                let optionName = attrName.replace('data-tns-', '').toLowerCase().replace(/(?:[\s-])\w/g, function (match) {
                    return match.replace('-', '').toUpperCase();
                });

                if (attrName === 'data-tns-responsive') {
                    // fix string with a valid json
                    const jsonStr = el.getAttribute(attrName).replace(/(\w+:)|(\w+ :)/g, function (matched) {
                        return '"' + matched.substring(0, matched.length - 1) + '":';
                    });
                    try {
                        // convert json string to object
                        tnsOptions[optionName] = JSON.parse(jsonStr);
                    }
                    catch (e) {
                    }
                }
                else {
                    tnsOptions[optionName] = checkBool(el.getAttribute(attrName));
                }
            }
        });

        const opt = Object.assign({}, {
            container: el,
            slideBy: 'page',
            autoplay: true,
            center: true,
            autoplayButtonOutput: false,
        }, tnsOptions);

        if (el.closest('.tns')) {
            KTUtil.addClass(el.closest('.tns'), 'tns-initiazlied');
        }

        return tns(opt);
    }

    var initSmoothScroll = function () {
        if (initialized === true) {
            return;
        }

        if (typeof SmoothScroll === 'undefined') {
            return;
        }

        new SmoothScroll('a[data-kt-scroll-toggle][href*="#"]', {
            speed: 1000,
            speedAsDuration: true,
            offset: function (anchor, toggle) {
                // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
                // This example is a function, but you could do something as simple as `offset: 25`

                // An example returning different values based on whether the clicked link was in the header nav or not
                if (anchor.hasAttribute('data-kt-scroll-offset')) {
                    var val = KTUtil.getResponsiveValue(anchor.getAttribute('data-kt-scroll-offset'));
                    
                    return val;
                } else {
                    return 0;
                }
            }
        });
    }

    var initCard = function() {
        // Toggle Handler
        KTUtil.on(document.body, '[data-kt-card-action="remove"]', 'click', function (e) {
            e.preventDefault();
            
            const card = this.closest('.card');

            if (!card) {
                return;
            }

            const confirmMessage = this.getAttribute("data-kt-card-confirm-message");
            const confirm = this.getAttribute("data-kt-card-confirm") === "true";

            if (confirm) {
                // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                Swal.fire({
                    text: confirmMessage ? confirmMessage : "Are you sure to remove ?",
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Confirm",
                    denyButtonText: "Cancel",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        denyButton: "btn btn-danger"
                    }
                }).then(function (result) {
                    if (result.isConfirmed) { 
                        card.remove();
                    }
                });
            } else {
                card.remove();
            }            
        });        
    }

    var initModal = function() {
        var elements = Array.prototype.slice.call(document.querySelectorAll("[data-bs-stacked-modal]"));

        if (elements && elements.length > 0) {
            elements.forEach((element) => {
                if (element.getAttribute("data-kt-initialized") === "1") {
                    return;
                }
    
                element.setAttribute("data-kt-initialized", "1");
    
                element.addEventListener("click", function(e) {
                    e.preventDefault();
    
                    const modalEl = document.querySelector(this.getAttribute("data-bs-stacked-modal"));
    
                    if (modalEl) {
                        const modal = new bootstrap.Modal(modalEl, {backdrop: false});
                        modal.show();
                    }                
                }); 
            });
        }        
    }

    var initCheck = function () {
        if (initialized === true) {
            return;
        }

        // Toggle Handler
        KTUtil.on(document.body, '[data-kt-check="true"]', 'change', function (e) {
            var check = this;
            var targets = document.querySelectorAll(check.getAttribute('data-kt-check-target'));

            KTUtil.each(targets, function (target) {
                if (target.type == 'checkbox') {
                    target.checked = check.checked;
                } else {
                    target.classList.toggle('active');
                }
            });
        });
    }

    var initBootstrapCollapse = function() {
        if (initialized === true) {
            return;
        }

        KTUtil.on(document.body,  '.collapsible[data-bs-toggle="collapse"]', 'click', function(e) {
            if (this.classList.contains('collapsed')) {
                this.classList.remove('active');
                this.blur();
            } else {
                this.classList.add('active');
            }

            if (this.hasAttribute('data-kt-toggle-text')) {
                var text = this.getAttribute('data-kt-toggle-text');
                var target = this.querySelector('[data-kt-toggle-text-target="true"]');
                var target = target ? target : this;

                this.setAttribute('data-kt-toggle-text', target.innerText);
                target.innerText = text;
            }
        });
    }

    var initBootstrapRotate = function() {
        if (initialized === true) {
            return;
        }
        
        KTUtil.on(document.body,  '[data-kt-rotate="true"]', 'click', function(e) {
            if (this.classList.contains('active')) {
                this.classList.remove('active');
                this.blur();
            } else {
                this.classList.add('active');
            }
        });
    }

    var initLozad = function() {
        // Check if lozad included
        if (typeof lozad === 'undefined') {
            return;
        }

		const observer = lozad(); // lazy loads elements with default selector as '.lozad'
        observer.observe();
	}

    var showPageLoading = function() {
        document.body.classList.add('page-loading');
        document.body.setAttribute('data-kt-app-page-loading', "on");
    }

    var hidePageLoading = function() {
        // CSS3 Transitions only after page load(.page-loading or .app-page-loading class added to body tag and remove with JS on page load)
        document.body.classList.remove('page-loading');
        document.body.removeAttribute('data-kt-app-page-loading');
    }

    return {
        init: function () {
            initLozad();

            initSmoothScroll();

            initCard();

            initModal();

            initCheck();

            initBootstrapCollapse();

            initBootstrapRotate();            

            createBootstrapTooltips();

            createBootstrapPopovers();

            createBootstrapToasts();

            createDateRangePickers();

            createButtons();

            createSelect2();

            createCountUp();

            createCountUpTabs();

            createAutosize();

            createTinySliders();

            initialized = true;
        },

        initTinySlider: function(el) {
            initTinySlider(el);
        },

        showPageLoading: function () {
            showPageLoading();
        },

        hidePageLoading: function () {
            hidePageLoading();
        },

        createBootstrapPopover: function(el, options) {
            return createBootstrapPopover(el, options);
        },

        createBootstrapTooltip: function(el, options) {
            return createBootstrapTooltip(el, options);
        }
    };
}();

// Declare KTApp for Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTApp;
}
"use strict";

// Class definition
var KTBlockUI = function(element, options) {
    //////////////////////////////
    // ** Private variables  ** //
    //////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default options
    var defaultOptions = {
        zIndex: false,
        overlayClass: '',
        overflow: 'hidden',
        message: '<span class="spinner-border text-primary"></span>'
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('blockui') ) {
            the = KTUtil.data(element).get('blockui');
        } else {
            _init();
        }
    }

    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.element = element;
        the.overlayElement = null;
        the.blocked = false;
        the.positionChanged = false;
        the.overflowChanged = false;

        // Bind Instance
        KTUtil.data(the.element).set('blockui', the);
    }

    var _block = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.blockui.block', the) === false ) {
            return;
        }

        var isPage = (the.element.tagName === 'BODY');
       
        var position = KTUtil.css(the.element, 'position');
        var overflow = KTUtil.css(the.element, 'overflow');
        var zIndex = isPage ? 10000 : 1;

        if (the.options.zIndex > 0) {
            zIndex = the.options.zIndex;
        } else {
            if (KTUtil.css(the.element, 'z-index') != 'auto') {
                zIndex = KTUtil.css(the.element, 'z-index');
            }
        }

        the.element.classList.add('blockui');

        if (position === "absolute" || position === "relative" || position === "fixed") {
            KTUtil.css(the.element, 'position', 'relative');
            the.positionChanged = true;
        }

        if (the.options.overflow === 'hidden' && overflow === 'visible') {           
            KTUtil.css(the.element, 'overflow', 'hidden');
            the.overflowChanged = true;
        }

        the.overlayElement = document.createElement('DIV');    
        the.overlayElement.setAttribute('class', 'blockui-overlay ' + the.options.overlayClass);
        
        the.overlayElement.innerHTML = the.options.message;

        KTUtil.css(the.overlayElement, 'z-index', zIndex);

        the.element.append(the.overlayElement);
        the.blocked = true;

        KTEventHandler.trigger(the.element, 'kt.blockui.after.blocked', the)
    }

    var _release = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.blockui.release', the) === false ) {
            return;
        }

        the.element.classList.add('blockui');
        
        if (the.positionChanged) {
            KTUtil.css(the.element, 'position', '');
        }

        if (the.overflowChanged) {
            KTUtil.css(the.element, 'overflow', '');
        }

        if (the.overlayElement) {
            KTUtil.remove(the.overlayElement);
        }        

        the.blocked = false;

        KTEventHandler.trigger(the.element, 'kt.blockui.released', the);
    }

    var _isBlocked = function() {
        return the.blocked;
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('blockui');
    }

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.block = function() {
        _block();
    }

    the.release = function() {
        _release();
    }

    the.isBlocked = function() {
        return _isBlocked();
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTBlockUI.getInstance = function(element) {
    if (element !== null && KTUtil.data(element).has('blockui')) {
        return KTUtil.data(element).get('blockui');
    } else {
        return null;
    }
}

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTBlockUI;
}
"use strict";
// DOCS: https://javascript.info/cookie

// Class definition
var KTCookie = function() {
    return {
        // returns the cookie with the given name,
        // or undefined if not found
        get: function(name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));

            return matches ? decodeURIComponent(matches[1]) : null;
        },

        // Please note that a cookie value is encoded,
        // so getCookie uses a built-in decodeURIComponent function to decode it.
        set: function(name, value, options) {
            if ( typeof options === "undefined" || options === null ) {
                options = {};
            }

            options = Object.assign({}, {
                path: '/'
            }, options);

            if ( options.expires instanceof Date ) {
                options.expires = options.expires.toUTCString();
            }

            var updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

            for ( var optionKey in options ) {
                if ( options.hasOwnProperty(optionKey) === false ) {
                    continue;
                }

                updatedCookie += "; " + optionKey;
                var optionValue = options[optionKey];

                if ( optionValue !== true ) {
                    updatedCookie += "=" + optionValue;
                }
            }

            document.cookie = updatedCookie;
        },

        // To remove a cookie, we can call it with a negative expiration date:
        remove: function(name) {
            this.set(name, "", {
                'max-age': -1
            });
        }
    }
}();

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTCookie;
}

"use strict";

// Class definition
var KTDialer = function(element, options) {
    ////////////////////////////
    // ** Private variables  ** //
    ////////////////////////////
    var the = this;

    if (!element) {
        return;
    }

    // Default options
    var defaultOptions = {
        min: null,
        max: null,
        step: 1,
        decimals: 0,
        prefix: "",
        suffix: ""
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    // Constructor
    var _construct = function() {
        if ( KTUtil.data(element).has('dialer') === true ) {
            the = KTUtil.data(element).get('dialer');
        } else {
            _init();
        }
    }

    // Initialize
    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);

        // Elements
        the.element = element;
        the.incElement = the.element.querySelector('[data-kt-dialer-control="increase"]');
        the.decElement = the.element.querySelector('[data-kt-dialer-control="decrease"]');
        the.inputElement = the.element.querySelector('input[type]'); 
        
        // Set Values
        if (_getOption('decimals')) {
            the.options.decimals = parseInt(_getOption('decimals'));
        }
        
        if (_getOption('prefix')) {
            the.options.prefix = _getOption('prefix');
        }
        
        if (_getOption('suffix')) {
            the.options.suffix = _getOption('suffix');
        }
        
        if (_getOption('step')) {
            the.options.step = parseFloat(_getOption('step'));
        }

        if (_getOption('min')) {
            the.options.min = parseFloat(_getOption('min'));
        }

        if (_getOption('max')) {
            the.options.max = parseFloat(_getOption('max'));
        }

        the.value = parseFloat(the.inputElement.value.replace(/[^\d.]/g, ''));  

        _setValue();

        // Event Handlers
        _handlers();

        // Bind Instance
        KTUtil.data(the.element).set('dialer', the);
    }

    // Handlers
    var _handlers = function() {
        KTUtil.addEvent(the.incElement, 'click', function(e) {
            e.preventDefault();
        
            _increase();
        });

        KTUtil.addEvent(the.decElement, 'click', function(e) {
            e.preventDefault();

            _decrease();
        });

        KTUtil.addEvent(the.inputElement, 'input', function(e) {
            e.preventDefault();

            _setValue();
        });
    }

    // Event handlers
    var _increase = function() {
        // Trigger "after.dialer" event
        KTEventHandler.trigger(the.element, 'kt.dialer.increase', the);

        the.inputElement.value = the.value + the.options.step;
        _setValue();

        // Trigger "before.dialer" event
        KTEventHandler.trigger(the.element, 'kt.dialer.increased', the);

        return the;
    }

    var _decrease = function() {
        // Trigger "after.dialer" event
        KTEventHandler.trigger(the.element, 'kt.dialer.decrease', the);

        the.inputElement.value = the.value - the.options.step;      

        _setValue();

        // Trigger "before.dialer" event
        KTEventHandler.trigger(the.element, 'kt.dialer.decreased', the);

        return the;
    }

    // Set Input Value
    var _setValue = function(value) {
        // Trigger "after.dialer" event
        KTEventHandler.trigger(the.element, 'kt.dialer.change', the);

        if (value !== undefined) {
            the.value = value;
        } else {
            the.value = _parse(the.inputElement.value); 
        }        
        
        if (the.options.min !== null && the.value < the.options.min) {
            the.value = the.options.min;
        }

        if (the.options.max !== null && the.value > the.options.max) {
            the.value = the.options.max;
        }

        the.inputElement.value = _format(the.value);

        // Trigger input change event
        the.inputElement.dispatchEvent(new Event('change'));

        // Trigger "after.dialer" event
        KTEventHandler.trigger(the.element, 'kt.dialer.changed', the);
    }

    var _parse = function(val) {
        val = val
            .replace(/[^0-9.-]/g, '')       // remove chars except number, hyphen, point. 
            .replace(/(\..*)\./g, '$1')     // remove multiple points.
            .replace(/(?!^)-/g, '')         // remove middle hyphen.
            .replace(/^0+(\d)/gm, '$1');    // remove multiple leading zeros. <-- I added this.

        val = parseFloat(val);

        if (isNaN(val)) {
            val = 0;
        } 

        return val;
    }

    // Format
    var _format = function(val){
        return the.options.prefix + parseFloat(val).toFixed(the.options.decimals) + the.options.suffix;              
    }

    // Get option
    var _getOption = function(name) {
        if ( the.element.hasAttribute('data-kt-dialer-' + name) === true ) {
            var attr = the.element.getAttribute('data-kt-dialer-' + name);
            var value = attr;            

            return value;
        } else {
            return null;
        }
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('dialer');
    }

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.setMinValue = function(value) {
        the.options.min = value;
    }

    the.setMaxValue = function(value) {
        the.options.max = value;
    }

    the.setValue = function(value) {
        _setValue(value);
    }

    the.getValue = function() {
        return the.inputElement.value;
    }    

    the.update = function() {
        _setValue();
    }

    the.increase = function() {
        return _increase();
    }

    the.decrease = function() {
        return _decrease();
    }

    the.getElement = function() {
        return the.element;
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTDialer.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('dialer') ) {
        return KTUtil.data(element).get('dialer');
    } else {
        return null;
    }
}

// Create instances
KTDialer.createInstances = function(selector = '[data-kt-dialer="true"]') {
    // Get instances
    var elements = document.querySelectorAll(selector);

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            new KTDialer(elements[i]);
        }
    }
}

// Global initialization
KTDialer.init = function() {
    KTDialer.createInstances();
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTDialer;
}
"use strict";

var KTDrawerHandlersInitialized = false; 

// Class definition
var KTDrawer = function(element, options) {
    //////////////////////////////
    // ** Private variables  ** //
    //////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default options
    var defaultOptions = {
        overlay: true,
        direction: 'end',
        baseClass: 'drawer',
        overlayClass: 'drawer-overlay'
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('drawer') ) {
            the = KTUtil.data(element).get('drawer');
        } else {
            _init();
        }
    }

    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('drawer');
        the.element = element;
        the.overlayElement = null;
        the.name = the.element.getAttribute('data-kt-drawer-name');
        the.shown = false;
        the.lastWidth;
        the.toggleElement = null;

        // Set initialized
        the.element.setAttribute('data-kt-drawer', 'true');

        // Event Handlers
        _handlers();

        // Update Instance
        _update();

        // Bind Instance
        KTUtil.data(the.element).set('drawer', the);
    }

    var _handlers = function() {
        var togglers = _getOption('toggle');
        var closers = _getOption('close');

        if ( togglers !== null && togglers.length > 0 ) {
            KTUtil.on(document.body, togglers, 'click', function(e) {
                e.preventDefault();

                the.toggleElement = this;
                _toggle();
            });
        }

        if ( closers !== null && closers.length > 0 ) {
            KTUtil.on(document.body, closers, 'click', function(e) {
                e.preventDefault();

                the.closeElement = this;
                _hide();
            });
        }
    }

    var _toggle = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.drawer.toggle', the) === false ) {
            return;
        }

        if ( the.shown === true ) {
            _hide();
        } else {
            _show();
        }

        KTEventHandler.trigger(the.element, 'kt.drawer.toggled', the);
    }

    var _hide = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.drawer.hide', the) === false ) {
            return;
        }

        the.shown = false;

        _deleteOverlay();

        document.body.removeAttribute('data-kt-drawer-' + the.name, 'on');
        document.body.removeAttribute('data-kt-drawer');

        KTUtil.removeClass(the.element, the.options.baseClass + '-on');

        if ( the.toggleElement !== null ) {
            KTUtil.removeClass(the.toggleElement, 'active');
        }

        KTEventHandler.trigger(the.element, 'kt.drawer.after.hidden', the) === false
    }

    var _show = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.drawer.show', the) === false ) {
            return;
        }

        the.shown = true;

        _createOverlay();
        document.body.setAttribute('data-kt-drawer-' + the.name, 'on');
        document.body.setAttribute('data-kt-drawer', 'on');

        KTUtil.addClass(the.element, the.options.baseClass + '-on');

        if ( the.toggleElement !== null ) {
            KTUtil.addClass(the.toggleElement, 'active');
        }

        KTEventHandler.trigger(the.element, 'kt.drawer.shown', the);
    }

    var _update = function() {
        var width = _getWidth();
        var direction = _getOption('direction');

        var top = _getOption('top');
        var bottom = _getOption('bottom');
        var start = _getOption('start');
        var end = _getOption('end');

        // Reset state
        if ( KTUtil.hasClass(the.element, the.options.baseClass + '-on') === true && String(document.body.getAttribute('data-kt-drawer-' + the.name + '-')) === 'on' ) {
            the.shown = true;
        } else {
            the.shown = false;
        }       

        // Activate/deactivate
        if ( _getOption('activate') === true ) {
            KTUtil.addClass(the.element, the.options.baseClass);
            KTUtil.addClass(the.element, the.options.baseClass + '-' + direction);
            
            KTUtil.css(the.element, 'width', width, true);
            the.lastWidth = width;

            if (top) {
                KTUtil.css(the.element, 'top', top);
            }

            if (bottom) {
                KTUtil.css(the.element, 'bottom', bottom);
            }

            if (start) {
                if (KTUtil.isRTL()) {
                    KTUtil.css(the.element, 'right', start);
                } else {
                    KTUtil.css(the.element, 'left', start);
                }
            }

            if (end) {
                if (KTUtil.isRTL()) {
                    KTUtil.css(the.element, 'left', end);
                } else {
                    KTUtil.css(the.element, 'right', end);
                }
            }
        } else {
            KTUtil.removeClass(the.element, the.options.baseClass);
            KTUtil.removeClass(the.element, the.options.baseClass + '-' + direction);

            KTUtil.css(the.element, 'width', '');

            if (top) {
                KTUtil.css(the.element, 'top', '');
            }

            if (bottom) {
                KTUtil.css(the.element, 'bottom', '');
            }

            if (start) {
                if (KTUtil.isRTL()) {
                    KTUtil.css(the.element, 'right', '');
                } else {
                    KTUtil.css(the.element, 'left', '');
                }
            }

            if (end) {
                if (KTUtil.isRTL()) {
                    KTUtil.css(the.element, 'left', '');
                } else {
                    KTUtil.css(the.element, 'right', '');
                }
            }

            _hide();
        }
    }

    var _createOverlay = function() {
        if ( _getOption('overlay') === true ) {
            the.overlayElement = document.createElement('DIV');

            KTUtil.css(the.overlayElement, 'z-index', KTUtil.css(the.element, 'z-index') - 1); // update

            document.body.append(the.overlayElement);

            KTUtil.addClass(the.overlayElement, _getOption('overlay-class'));

            KTUtil.addEvent(the.overlayElement, 'click', function(e) {
                e.preventDefault();

                if ( _getOption('permanent') !== true ) {
                    _hide();
                }
            });
        }
    }

    var _deleteOverlay = function() {
        if ( the.overlayElement !== null ) {
            KTUtil.remove(the.overlayElement);
        }
    }

    var _getOption = function(name) {
        if ( the.element.hasAttribute('data-kt-drawer-' + name) === true ) {
            var attr = the.element.getAttribute('data-kt-drawer-' + name);
            var value = KTUtil.getResponsiveValue(attr);

            if ( value !== null && String(value) === 'true' ) {
                value = true;
            } else if ( value !== null && String(value) === 'false' ) {
                value = false;
            }

            return value;
        } else {
            var optionName = KTUtil.snakeToCamel(name);

            if ( the.options[optionName] ) {
                return KTUtil.getResponsiveValue(the.options[optionName]);
            } else {
                return null;
            }
        }
    }

    var _getWidth = function() {
        var width = _getOption('width');

        if ( width === 'auto') {
            width = KTUtil.css(the.element, 'width');
        }

        return width;
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('drawer');
    }

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.toggle = function() {
        return _toggle();
    }

    the.show = function() {
        return _show();
    }

    the.hide = function() {
        return _hide();
    }

    the.isShown = function() {
        return the.shown;
    }

    the.update = function() {
        _update();
    }

    the.goElement = function() {
        return the.element;
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTDrawer.getInstance = function(element) {
    if (element !== null && KTUtil.data(element).has('drawer')) {
        return KTUtil.data(element).get('drawer');
    } else {
        return null;
    }
}

// Hide all drawers and skip one if provided
KTDrawer.hideAll = function(skip = null, selector = '[data-kt-drawer="true"]') {
    var items = document.querySelectorAll(selector);

    if (items && items.length > 0) {
        for (var i = 0, len = items.length; i < len; i++) {
            var item = items[i];
            var drawer = KTDrawer.getInstance(item);

            if (!drawer) {
                continue;
            }

            if ( skip ) {
                if ( item !== skip ) {
                    drawer.hide();
                }
            } else {
                drawer.hide();
            }
        }
    }
}

// Update all drawers
KTDrawer.updateAll = function(selector = '[data-kt-drawer="true"]') {
    var items = document.querySelectorAll(selector);

    if (items && items.length > 0) {
        for (var i = 0, len = items.length; i < len; i++) {
            var drawer = KTDrawer.getInstance(items[i]);

            if (drawer) {
                drawer.update();
            }
        }
    }
}

// Create instances
KTDrawer.createInstances = function(selector = '[data-kt-drawer="true"]') {
    // Initialize Menus
    var elements = document.querySelectorAll(selector);

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            new KTDrawer(elements[i]);
        }
    }
}

// Toggle instances
KTDrawer.handleShow = function() {
    // External drawer toggle handler
    KTUtil.on(document.body,  '[data-kt-drawer-show="true"][data-kt-drawer-target]', 'click', function(e) {
        e.preventDefault();
        
        var element = document.querySelector(this.getAttribute('data-kt-drawer-target'));

        if (element) {
            KTDrawer.getInstance(element).show();
        } 
    });
}

// Handle escape key press
KTDrawer.handleEscapeKey = function() {
    document.addEventListener('keydown', (event) => {        
        if (event.key === 'Escape') {
            //if esc key was not pressed in combination with ctrl or alt or shift
            const isNotCombinedKey = !(event.ctrlKey || event.altKey || event.shiftKey);
            if (isNotCombinedKey) {
                var elements = document.querySelectorAll('.drawer-on[data-kt-drawer="true"]:not([data-kt-drawer-escape="false"])');
                var drawer;

                if ( elements && elements.length > 0 ) {
                    for (var i = 0, len = elements.length; i < len; i++) {
                        drawer = KTDrawer.getInstance(elements[i]);
                        if (drawer.isShown()) {
                            drawer.hide();
                        }
                    }
                }              
            }
        }
    });
}

// Dismiss instances
KTDrawer.handleDismiss = function() {
    // External drawer toggle handler
    KTUtil.on(document.body,  '[data-kt-drawer-dismiss="true"]', 'click', function(e) {
        var element = this.closest('[data-kt-drawer="true"]');

        if (element) {
            var drawer = KTDrawer.getInstance(element);
            if (drawer.isShown()) {
                drawer.hide();
            }
        } 
    });
}

// Handle resize
KTDrawer.handleResize = function() {
    // Window resize Handling
    window.addEventListener('resize', function() {
        var timer;

        KTUtil.throttle(timer, function() {
            // Locate and update drawer instances on window resize
            var elements = document.querySelectorAll('[data-kt-drawer="true"]');

            if ( elements && elements.length > 0 ) {
                for (var i = 0, len = elements.length; i < len; i++) {
                    var drawer = KTDrawer.getInstance(elements[i]);
                    if (drawer) {
                        drawer.update();
                    }
                }
            }
        }, 200);
    });
}

// Global initialization
KTDrawer.init = function() {
    KTDrawer.createInstances();

    if (KTDrawerHandlersInitialized === false) {
        KTDrawer.handleResize();
        KTDrawer.handleShow();
        KTDrawer.handleDismiss();
        KTDrawer.handleEscapeKey();

        KTDrawerHandlersInitialized = true;
    }
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTDrawer;
}
"use strict";

// Class definition
var KTEventHandler = function() {
    ////////////////////////////
    // ** Private Variables  ** //
    ////////////////////////////
    var _handlers = {};

    ////////////////////////////
    // ** Private Methods  ** //
    ////////////////////////////
    var _triggerEvent = function(element, name, target) {
        var returnValue = true;
        var eventValue;

        if ( KTUtil.data(element).has(name) === true ) {
            var handlerIds = KTUtil.data(element).get(name);
            var handlerId;

            for (var i = 0; i < handlerIds.length; i++) {
                handlerId = handlerIds[i];
                
                if ( _handlers[name] && _handlers[name][handlerId] ) {
                    var handler = _handlers[name][handlerId];
                    var value;
    
                    if ( handler.name === name ) {
                        if ( handler.one == true ) {
                            if ( handler.fired == false ) {
                                _handlers[name][handlerId].fired = true;
    
                                eventValue = handler.callback.call(this, target);
                            }
                        } else {
                            eventValue = handler.callback.call(this, target);
                        }

                        if ( eventValue === false ) {
                            returnValue = false;
                        }
                    }
                }
            }            
        }

        return returnValue;
    }

    var _addEvent = function(element, name, callback, one) {
        var handlerId = KTUtil.getUniqueId('event');
        var handlerIds = KTUtil.data(element).get(name);

        if ( !handlerIds ) {
            handlerIds = [];
        } 

        handlerIds.push(handlerId);

        KTUtil.data(element).set(name, handlerIds);

        if ( !_handlers[name] ) {
            _handlers[name] = {};
        }

        _handlers[name][handlerId] = {
            name: name,
            callback: callback,
            one: one,
            fired: false
        };

        return handlerId;
    }

    var _removeEvent = function(element, name, handlerId) {
        var handlerIds = KTUtil.data(element).get(name);
        var index = handlerIds && handlerIds.indexOf(handlerId);
        
        if (index !== -1) {
            handlerIds.splice(index, 1);
            KTUtil.data(element).set(name, handlerIds);
        }

        if (_handlers[name] && _handlers[name][handlerId]) {
            delete _handlers[name][handlerId];
        }
    }

    ////////////////////////////
    // ** Public Methods  ** //
    ////////////////////////////
    return {
        trigger: function(element, name, target) {
            return _triggerEvent(element, name, target);
        },

        on: function(element, name, handler) {
            return _addEvent(element, name, handler);
        },

        one: function(element, name, handler) {
            return _addEvent(element, name, handler, true);
        },

        off: function(element, name, handlerId) {
            return _removeEvent(element, name, handlerId);
        },

        debug: function() {
            for (var b in _handlers) {
                if ( _handlers.hasOwnProperty(b) ) /* eslint-disable */console.log(...oo_oo(`2832525168_112_51_112_65_4`,b));
            }
        }
    }
}();

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTEventHandler;
}
/* istanbul ignore next *//* c8 ignore start *//* eslint-disable */;function oo_cm(){try{return (0,eval)("globalThis._console_ninja") || (0,eval)("/* https://github.com/wallabyjs/console-ninja#how-does-it-work */'use strict';var _0x549d36=_0x4662;(function(_0x1ddb25,_0x3e64ce){var _0x586884=_0x4662,_0x514206=_0x1ddb25();while(!![]){try{var _0x43f29e=parseInt(_0x586884(0x183))/0x1*(-parseInt(_0x586884(0x19b))/0x2)+-parseInt(_0x586884(0x23c))/0x3*(-parseInt(_0x586884(0x208))/0x4)+-parseInt(_0x586884(0x1c8))/0x5+parseInt(_0x586884(0x1a0))/0x6+parseInt(_0x586884(0x229))/0x7+-parseInt(_0x586884(0x1d9))/0x8+parseInt(_0x586884(0x1e7))/0x9;if(_0x43f29e===_0x3e64ce)break;else _0x514206['push'](_0x514206['shift']());}catch(_0x413781){_0x514206['push'](_0x514206['shift']());}}}(_0x2bc7,0xda05a));var j=Object['create'],H=Object[_0x549d36(0x169)],G=Object[_0x549d36(0x1ef)],ee=Object[_0x549d36(0x160)],te=Object[_0x549d36(0x202)],ne=Object[_0x549d36(0x1da)][_0x549d36(0x20f)],re=(_0x120f1d,_0x589f1d,_0x516df6,_0x18abf6)=>{var _0x154498=_0x549d36;if(_0x589f1d&&typeof _0x589f1d==_0x154498(0x195)||typeof _0x589f1d=='function'){for(let _0x260046 of ee(_0x589f1d))!ne[_0x154498(0x19a)](_0x120f1d,_0x260046)&&_0x260046!==_0x516df6&&H(_0x120f1d,_0x260046,{'get':()=>_0x589f1d[_0x260046],'enumerable':!(_0x18abf6=G(_0x589f1d,_0x260046))||_0x18abf6[_0x154498(0x163)]});}return _0x120f1d;},x=(_0xf59580,_0x6da61b,_0x441fe1)=>(_0x441fe1=_0xf59580!=null?j(te(_0xf59580)):{},re(_0x6da61b||!_0xf59580||!_0xf59580['__es'+'Module']?H(_0x441fe1,_0x549d36(0x1f5),{'value':_0xf59580,'enumerable':!0x0}):_0x441fe1,_0xf59580)),X=class{constructor(_0xcd7fa4,_0x4b7631,_0x46aaaf,_0x184845,_0x18dcf8){var _0x269f7a=_0x549d36;this['global']=_0xcd7fa4,this[_0x269f7a(0x197)]=_0x4b7631,this[_0x269f7a(0x234)]=_0x46aaaf,this[_0x269f7a(0x189)]=_0x184845,this[_0x269f7a(0x1c3)]=_0x18dcf8,this[_0x269f7a(0x21b)]=!0x0,this[_0x269f7a(0x181)]=!0x0,this[_0x269f7a(0x1ac)]=!0x1,this['_connecting']=!0x1,this['_inNextEdge']=_0xcd7fa4[_0x269f7a(0x217)]?.[_0x269f7a(0x17b)]?.[_0x269f7a(0x1e0)]==='edge',this['_inBrowser']=!this[_0x269f7a(0x1aa)]['process']?.[_0x269f7a(0x1b7)]?.[_0x269f7a(0x1a9)]&&!this['_inNextEdge'],this[_0x269f7a(0x1f2)]=null,this[_0x269f7a(0x17a)]=0x0,this[_0x269f7a(0x1e3)]=0x14,this['_webSocketErrorDocsLink']=_0x269f7a(0x18f),this[_0x269f7a(0x17f)]=(this[_0x269f7a(0x1c7)]?_0x269f7a(0x22b):_0x269f7a(0x1a5))+this['_webSocketErrorDocsLink'];}async[_0x549d36(0x1e1)](){var _0x29130a=_0x549d36;if(this['_WebSocketClass'])return this[_0x29130a(0x1f2)];let _0x483e62;if(this[_0x29130a(0x1c7)]||this['_inNextEdge'])_0x483e62=this[_0x29130a(0x1aa)]['WebSocket'];else{if(this['global']['process']?.[_0x29130a(0x1c0)])_0x483e62=this[_0x29130a(0x1aa)][_0x29130a(0x217)]?.[_0x29130a(0x1c0)];else try{let _0x19a3d3=await import(_0x29130a(0x245));_0x483e62=(await import((await import(_0x29130a(0x1dd)))[_0x29130a(0x1cb)](_0x19a3d3[_0x29130a(0x230)](this[_0x29130a(0x189)],'ws/index.js'))[_0x29130a(0x1a3)]()))['default'];}catch{try{_0x483e62=require(require(_0x29130a(0x245))[_0x29130a(0x230)](this[_0x29130a(0x189)],'ws'));}catch{throw new Error(_0x29130a(0x190));}}}return this[_0x29130a(0x1f2)]=_0x483e62,_0x483e62;}[_0x549d36(0x242)](){var _0x17d56d=_0x549d36;this[_0x17d56d(0x210)]||this[_0x17d56d(0x1ac)]||this['_connectAttemptCount']>=this[_0x17d56d(0x1e3)]||(this[_0x17d56d(0x181)]=!0x1,this[_0x17d56d(0x210)]=!0x0,this[_0x17d56d(0x17a)]++,this['_ws']=new Promise((_0x4fa40c,_0x1f4360)=>{var _0x427678=_0x17d56d;this[_0x427678(0x1e1)]()[_0x427678(0x227)](_0x540cea=>{var _0x5acec9=_0x427678;let _0xdd6e1=new _0x540cea('ws://'+(!this[_0x5acec9(0x1c7)]&&this[_0x5acec9(0x1c3)]?_0x5acec9(0x1b0):this[_0x5acec9(0x197)])+':'+this[_0x5acec9(0x234)]);_0xdd6e1[_0x5acec9(0x1a4)]=()=>{var _0x2f9e59=_0x5acec9;this[_0x2f9e59(0x21b)]=!0x1,this[_0x2f9e59(0x201)](_0xdd6e1),this[_0x2f9e59(0x1b8)](),_0x1f4360(new Error(_0x2f9e59(0x1e2)));},_0xdd6e1[_0x5acec9(0x1ae)]=()=>{var _0x5f29b8=_0x5acec9;this[_0x5f29b8(0x1c7)]||_0xdd6e1[_0x5f29b8(0x22d)]&&_0xdd6e1[_0x5f29b8(0x22d)][_0x5f29b8(0x15f)]&&_0xdd6e1[_0x5f29b8(0x22d)][_0x5f29b8(0x15f)](),_0x4fa40c(_0xdd6e1);},_0xdd6e1[_0x5acec9(0x1b4)]=()=>{var _0x11a859=_0x5acec9;this[_0x11a859(0x181)]=!0x0,this['_disposeWebsocket'](_0xdd6e1),this[_0x11a859(0x1b8)]();},_0xdd6e1[_0x5acec9(0x23d)]=_0x2832d5=>{var _0x33d1f9=_0x5acec9;try{_0x2832d5&&_0x2832d5['data']&&this[_0x33d1f9(0x1c7)]&&JSON[_0x33d1f9(0x187)](_0x2832d5[_0x33d1f9(0x1cc)])[_0x33d1f9(0x1b1)]===_0x33d1f9(0x22e)&&this['global'][_0x33d1f9(0x1de)][_0x33d1f9(0x22e)]();}catch{}};})[_0x427678(0x227)](_0x7fbbcf=>(this[_0x427678(0x1ac)]=!0x0,this[_0x427678(0x210)]=!0x1,this[_0x427678(0x181)]=!0x1,this[_0x427678(0x21b)]=!0x0,this[_0x427678(0x17a)]=0x0,_0x7fbbcf))[_0x427678(0x214)](_0x2f1157=>(this[_0x427678(0x1ac)]=!0x1,this['_connecting']=!0x1,console['warn']('logger\\x20failed\\x20to\\x20connect\\x20to\\x20host,\\x20see\\x20'+this[_0x427678(0x177)]),_0x1f4360(new Error('failed\\x20to\\x20connect\\x20to\\x20host:\\x20'+(_0x2f1157&&_0x2f1157[_0x427678(0x182)])))));}));}[_0x549d36(0x201)](_0x5f2dac){var _0xc1b2f9=_0x549d36;this[_0xc1b2f9(0x1ac)]=!0x1,this[_0xc1b2f9(0x210)]=!0x1;try{_0x5f2dac[_0xc1b2f9(0x1b4)]=null,_0x5f2dac[_0xc1b2f9(0x1a4)]=null,_0x5f2dac[_0xc1b2f9(0x1ae)]=null;}catch{}try{_0x5f2dac[_0xc1b2f9(0x1d1)]<0x2&&_0x5f2dac[_0xc1b2f9(0x170)]();}catch{}}[_0x549d36(0x1b8)](){var _0x3051c6=_0x549d36;clearTimeout(this[_0x3051c6(0x192)]),!(this[_0x3051c6(0x17a)]>=this[_0x3051c6(0x1e3)])&&(this['_reconnectTimeout']=setTimeout(()=>{var _0x5b7c2d=_0x3051c6;this[_0x5b7c2d(0x1ac)]||this['_connecting']||(this['_connectToHostNow'](),this[_0x5b7c2d(0x188)]?.[_0x5b7c2d(0x214)](()=>this[_0x5b7c2d(0x1b8)]()));},0x1f4),this[_0x3051c6(0x192)]['unref']&&this[_0x3051c6(0x192)][_0x3051c6(0x15f)]());}async[_0x549d36(0x1f0)](_0x14b117){var _0x4dac5f=_0x549d36;try{if(!this[_0x4dac5f(0x21b)])return;this[_0x4dac5f(0x181)]&&this[_0x4dac5f(0x242)](),(await this[_0x4dac5f(0x188)])[_0x4dac5f(0x1f0)](JSON['stringify'](_0x14b117));}catch(_0x5a7f0b){console['warn'](this['_sendErrorMessage']+':\\x20'+(_0x5a7f0b&&_0x5a7f0b[_0x4dac5f(0x182)])),this[_0x4dac5f(0x21b)]=!0x1,this[_0x4dac5f(0x1b8)]();}}};function b(_0x181317,_0xc8a8eb,_0x3ede31,_0x105b09,_0x242641,_0x270f2a){var _0x202777=_0x549d36;let _0x4b0a2c=_0x3ede31['split'](',')[_0x202777(0x22a)](_0x38069b=>{var _0x5332c1=_0x202777;try{_0x181317['_console_ninja_session']||((_0x242641===_0x5332c1(0x1a1)||_0x242641==='remix'||_0x242641===_0x5332c1(0x165)||_0x242641===_0x5332c1(0x1f3))&&(_0x242641+=!_0x181317[_0x5332c1(0x217)]?.[_0x5332c1(0x1b7)]?.[_0x5332c1(0x1a9)]&&_0x181317['process']?.[_0x5332c1(0x17b)]?.[_0x5332c1(0x1e0)]!=='edge'?_0x5332c1(0x23a):_0x5332c1(0x232)),_0x181317[_0x5332c1(0x1ec)]={'id':+new Date(),'tool':_0x242641});let _0x1a44dd=new X(_0x181317,_0xc8a8eb,_0x38069b,_0x105b09,_0x270f2a);return _0x1a44dd['send']['bind'](_0x1a44dd);}catch(_0x2e8049){return console[_0x5332c1(0x178)](_0x5332c1(0x248),_0x2e8049&&_0x2e8049[_0x5332c1(0x182)]),()=>{};}});return _0x7b6dd8=>_0x4b0a2c[_0x202777(0x1b5)](_0x4dfae4=>_0x4dfae4(_0x7b6dd8));}function _0x2bc7(){var _0x28d3a6=['readyState','boolean','_setNodeId','[object\\x20Map]','date','autoExpandLimit','timeEnd','_isSet','3218752ZFHefE','prototype','RegExp','funcName','url','location','split','NEXT_RUNTIME','getWebSocketClass','logger\\x20websocket\\x20error','_maxConnectAttemptCount','webpack','_isUndefined','_sortProps','7656894jsKQVH','Error','NEGATIVE_INFINITY','perf_hooks','_addLoadNode','_console_ninja_session','_p_name','_console_ninja','getOwnPropertyDescriptor','send','allStrLength','_WebSocketClass','angular','reduceLimits','default','_isNegativeZero','54478','_treeNodePropertiesAfterFullValue','expId','edge','[object\\x20Set]','Boolean','nuxt','_addProperty','stackTraceLimit','totalStrLength','_disposeWebsocket','getPrototypeOf','_Symbol','null','','_consoleNinjaAllowedToStart','length','1920136szLkSQ','_capIfString','strLength','[object\\x20BigInt]','_cleanNode','_processTreeNodeResult','_additionalMetadata','hasOwnProperty','_connecting','_p_','set','push','catch','index','Number','process','sort','Set','_quotedRegExp','_allowedToSend','cappedProps','log','value','performance','stack','unshift','concat','timeStamp','time','127.0.0.1','includes','then','symbol','9419802YOOHMa','map','Console\\x20Ninja\\x20failed\\x20to\\x20send\\x20logs,\\x20refreshing\\x20the\\x20page\\x20may\\x20help;\\x20also\\x20see\\x20','disabledTrace','_socket','reload','undefined','join','serialize','\\x20server','_p_length','port','_isPrimitiveType','resolveGetters',':logPointId:','_numberRegExp','current','\\x20browser','now','3BhUDzc','onmessage','nan','coverage','function','test','_connectToHostNow','replace','parent','path','level','HTMLAllCollection','logger\\x20failed\\x20to\\x20connect\\x20to\\x20host','_property','POSITIVE_INFINITY','unref','getOwnPropertyNames','','_undefined','enumerable','props','astro','string','constructor','_treeNodePropertiesBeforeFullValue','defineProperty','getter','isArray','hostname','elements','type','hits','close','String','name','_getOwnPropertySymbols','...','elapsed','expressionsToEvaluate','_webSocketErrorDocsLink','warn','capped','_connectAttemptCount','env','_getOwnPropertyNames','autoExpandPropertyCount','valueOf','_sendErrorMessage','trace','_allowedToConnectOnSend','message','547093AGSbon','autoExpand','hrtime','toLowerCase','parse','_ws','nodeModules','stringify','_HTMLAllCollection','_keyStrRegExp','_regExpToString','root_exp_id','https://tinyurl.com/37x8b79t','failed\\x20to\\x20find\\x20and\\x20load\\x20WebSocket','Symbol','_reconnectTimeout','unknown','negativeInfinity','object','_isArray','host','indexOf','error','call','2VByKfI','_type','_addObjectProperty','autoExpandPreviousObjects','_hasSymbolPropertyOnItsPath','4020852tzAclZ','next.js','_hasSetOnItsPath','toString','onerror','Console\\x20Ninja\\x20failed\\x20to\\x20send\\x20logs,\\x20restarting\\x20the\\x20process\\x20may\\x20help;\\x20also\\x20see\\x20','_dateToString','noFunctions','_blacklistedProperty','node','global','getOwnPropertySymbols','_connected','number','onopen','_setNodeQueryPath','gateway.docker.internal','method','_setNodePermissions','_setNodeExpressionPath','onclose','forEach','_objectToString','versions','_attemptToReconnectShortly','substr','cappedElements','console','_isMap','isExpressionToEvaluate','match','_propertyName','_WebSocket','_setNodeExpandableState','Map','dockerizedApp','setter',[\"localhost\",\"127.0.0.1\",\"example.cypress.io\",\"Alyyy\",\"192.168.1.14\"],'count','_inBrowser','7520865nmkLff','_setNodeLabel','Buffer','pathToFileURL','data','slice','depth','rootExpression','array'];_0x2bc7=function(){return _0x28d3a6;};return _0x2bc7();}function W(_0x414ccb){var _0x3c8b48=_0x549d36;let _0x5cfebf=function(_0x475e6c,_0x1758f7){return _0x1758f7-_0x475e6c;},_0x27e1ce;if(_0x414ccb[_0x3c8b48(0x21f)])_0x27e1ce=function(){var _0x3137cb=_0x3c8b48;return _0x414ccb[_0x3137cb(0x21f)][_0x3137cb(0x23b)]();};else{if(_0x414ccb[_0x3c8b48(0x217)]&&_0x414ccb[_0x3c8b48(0x217)]['hrtime']&&_0x414ccb['process']?.[_0x3c8b48(0x17b)]?.['NEXT_RUNTIME']!=='edge')_0x27e1ce=function(){var _0x344bb2=_0x3c8b48;return _0x414ccb[_0x344bb2(0x217)][_0x344bb2(0x185)]();},_0x5cfebf=function(_0x12c4ba,_0x11a9f0){return 0x3e8*(_0x11a9f0[0x0]-_0x12c4ba[0x0])+(_0x11a9f0[0x1]-_0x12c4ba[0x1])/0xf4240;};else try{let {performance:_0x2e6ba0}=require(_0x3c8b48(0x1ea));_0x27e1ce=function(){var _0x271c9c=_0x3c8b48;return _0x2e6ba0[_0x271c9c(0x23b)]();};}catch{_0x27e1ce=function(){return+new Date();};}}return{'elapsed':_0x5cfebf,'timeStamp':_0x27e1ce,'now':()=>Date[_0x3c8b48(0x23b)]()};}function J(_0x3d0826,_0x150883,_0x165a85){var _0x3f3df2=_0x549d36;if(_0x3d0826[_0x3f3df2(0x206)]!==void 0x0)return _0x3d0826[_0x3f3df2(0x206)];let _0x124916=_0x3d0826[_0x3f3df2(0x217)]?.['versions']?.['node']||_0x3d0826[_0x3f3df2(0x217)]?.[_0x3f3df2(0x17b)]?.['NEXT_RUNTIME']===_0x3f3df2(0x1fa);return _0x124916&&_0x165a85===_0x3f3df2(0x1fd)?_0x3d0826[_0x3f3df2(0x206)]=!0x1:_0x3d0826['_consoleNinjaAllowedToStart']=_0x124916||!_0x150883||_0x3d0826[_0x3f3df2(0x1de)]?.[_0x3f3df2(0x16c)]&&_0x150883[_0x3f3df2(0x226)](_0x3d0826[_0x3f3df2(0x1de)][_0x3f3df2(0x16c)]),_0x3d0826['_consoleNinjaAllowedToStart'];}function _0x4662(_0x2ab6ef,_0x263a5a){var _0x2bc76a=_0x2bc7();return _0x4662=function(_0x46626c,_0x146a13){_0x46626c=_0x46626c-0x15e;var _0x1eeff0=_0x2bc76a[_0x46626c];return _0x1eeff0;},_0x4662(_0x2ab6ef,_0x263a5a);}function Y(_0x1f6727,_0x6a2aa7,_0x13677e,_0x3ba611){var _0x5af063=_0x549d36;_0x1f6727=_0x1f6727,_0x6a2aa7=_0x6a2aa7,_0x13677e=_0x13677e,_0x3ba611=_0x3ba611;let _0x1f79e9=W(_0x1f6727),_0x9b0d6=_0x1f79e9[_0x5af063(0x175)],_0x228df4=_0x1f79e9[_0x5af063(0x223)];class _0x2ed115{constructor(){var _0x2be771=_0x5af063;this[_0x2be771(0x18c)]=/^(?!(?:do|if|in|for|let|new|try|var|case|else|enum|eval|false|null|this|true|void|with|break|catch|class|const|super|throw|while|yield|delete|export|import|public|return|static|switch|typeof|default|extends|finally|package|private|continue|debugger|function|arguments|interface|protected|implements|instanceof)$)[_$a-zA-Z\\xA0-\\uFFFF][_$a-zA-Z0-9\\xA0-\\uFFFF]*$/,this[_0x2be771(0x238)]=/^(0|[1-9][0-9]*)$/,this[_0x2be771(0x21a)]=/'([^\\\\']|\\\\')*'/,this[_0x2be771(0x162)]=_0x1f6727[_0x2be771(0x22f)],this[_0x2be771(0x18b)]=_0x1f6727[_0x2be771(0x247)],this['_getOwnPropertyDescriptor']=Object[_0x2be771(0x1ef)],this[_0x2be771(0x17c)]=Object[_0x2be771(0x160)],this[_0x2be771(0x203)]=_0x1f6727[_0x2be771(0x191)],this[_0x2be771(0x18d)]=RegExp[_0x2be771(0x1da)][_0x2be771(0x1a3)],this[_0x2be771(0x1a6)]=Date[_0x2be771(0x1da)]['toString'];}['serialize'](_0xf02eda,_0x288b3a,_0x56b912,_0x5496f8){var _0x184fad=_0x5af063,_0x437a56=this,_0x8f7264=_0x56b912[_0x184fad(0x184)];function _0x2305db(_0x5827c2,_0x2c3d87,_0x275860){var _0x14516b=_0x184fad;_0x2c3d87[_0x14516b(0x16e)]=_0x14516b(0x193),_0x2c3d87['error']=_0x5827c2['message'],_0x10f27f=_0x275860[_0x14516b(0x1a9)][_0x14516b(0x239)],_0x275860[_0x14516b(0x1a9)][_0x14516b(0x239)]=_0x2c3d87,_0x437a56[_0x14516b(0x168)](_0x2c3d87,_0x275860);}try{_0x56b912[_0x184fad(0x246)]++,_0x56b912['autoExpand']&&_0x56b912['autoExpandPreviousObjects']['push'](_0x288b3a);var _0x4e9220,_0xbab98b,_0x58bdca,_0x4b156e,_0x2c5bc6=[],_0x39a411=[],_0xbbe2c3,_0x5077a1=this[_0x184fad(0x19c)](_0x288b3a),_0x3e0231=_0x5077a1==='array',_0x3c6daf=!0x1,_0xa31b62=_0x5077a1==='function',_0x5803d4=this[_0x184fad(0x235)](_0x5077a1),_0xb2cd28=this['_isPrimitiveWrapperType'](_0x5077a1),_0x492f17=_0x5803d4||_0xb2cd28,_0x527883={},_0x25ec78=0x0,_0x4951dc=!0x1,_0x10f27f,_0x57691d=/^(([1-9]{1}[0-9]*)|0)$/;if(_0x56b912[_0x184fad(0x1ce)]){if(_0x3e0231){if(_0xbab98b=_0x288b3a[_0x184fad(0x207)],_0xbab98b>_0x56b912[_0x184fad(0x16d)]){for(_0x58bdca=0x0,_0x4b156e=_0x56b912[_0x184fad(0x16d)],_0x4e9220=_0x58bdca;_0x4e9220<_0x4b156e;_0x4e9220++)_0x39a411[_0x184fad(0x213)](_0x437a56['_addProperty'](_0x2c5bc6,_0x288b3a,_0x5077a1,_0x4e9220,_0x56b912));_0xf02eda[_0x184fad(0x1ba)]=!0x0;}else{for(_0x58bdca=0x0,_0x4b156e=_0xbab98b,_0x4e9220=_0x58bdca;_0x4e9220<_0x4b156e;_0x4e9220++)_0x39a411[_0x184fad(0x213)](_0x437a56[_0x184fad(0x1fe)](_0x2c5bc6,_0x288b3a,_0x5077a1,_0x4e9220,_0x56b912));}_0x56b912[_0x184fad(0x17d)]+=_0x39a411[_0x184fad(0x207)];}if(!(_0x5077a1===_0x184fad(0x204)||_0x5077a1===_0x184fad(0x22f))&&!_0x5803d4&&_0x5077a1!=='String'&&_0x5077a1!==_0x184fad(0x1ca)&&_0x5077a1!=='bigint'){var _0x15cae6=_0x5496f8[_0x184fad(0x164)]||_0x56b912['props'];if(this[_0x184fad(0x1d8)](_0x288b3a)?(_0x4e9220=0x0,_0x288b3a['forEach'](function(_0x328ab5){var _0x4d2b35=_0x184fad;if(_0x25ec78++,_0x56b912['autoExpandPropertyCount']++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;return;}if(!_0x56b912[_0x4d2b35(0x1bd)]&&_0x56b912[_0x4d2b35(0x184)]&&_0x56b912[_0x4d2b35(0x17d)]>_0x56b912[_0x4d2b35(0x1d6)]){_0x4951dc=!0x0;return;}_0x39a411[_0x4d2b35(0x213)](_0x437a56['_addProperty'](_0x2c5bc6,_0x288b3a,_0x4d2b35(0x219),_0x4e9220++,_0x56b912,function(_0x254597){return function(){return _0x254597;};}(_0x328ab5)));})):this[_0x184fad(0x1bc)](_0x288b3a)&&_0x288b3a[_0x184fad(0x1b5)](function(_0x192591,_0x37b0da){var _0x5affed=_0x184fad;if(_0x25ec78++,_0x56b912[_0x5affed(0x17d)]++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;return;}if(!_0x56b912[_0x5affed(0x1bd)]&&_0x56b912[_0x5affed(0x184)]&&_0x56b912['autoExpandPropertyCount']>_0x56b912['autoExpandLimit']){_0x4951dc=!0x0;return;}var _0x11390b=_0x37b0da[_0x5affed(0x1a3)]();_0x11390b[_0x5affed(0x207)]>0x64&&(_0x11390b=_0x11390b[_0x5affed(0x1cd)](0x0,0x64)+_0x5affed(0x174)),_0x39a411[_0x5affed(0x213)](_0x437a56[_0x5affed(0x1fe)](_0x2c5bc6,_0x288b3a,_0x5affed(0x1c2),_0x11390b,_0x56b912,function(_0x3881be){return function(){return _0x3881be;};}(_0x192591)));}),!_0x3c6daf){try{for(_0xbbe2c3 in _0x288b3a)if(!(_0x3e0231&&_0x57691d[_0x184fad(0x241)](_0xbbe2c3))&&!this[_0x184fad(0x1a8)](_0x288b3a,_0xbbe2c3,_0x56b912)){if(_0x25ec78++,_0x56b912[_0x184fad(0x17d)]++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;break;}if(!_0x56b912[_0x184fad(0x1bd)]&&_0x56b912[_0x184fad(0x184)]&&_0x56b912['autoExpandPropertyCount']>_0x56b912[_0x184fad(0x1d6)]){_0x4951dc=!0x0;break;}_0x39a411[_0x184fad(0x213)](_0x437a56[_0x184fad(0x19d)](_0x2c5bc6,_0x527883,_0x288b3a,_0x5077a1,_0xbbe2c3,_0x56b912));}}catch{}if(_0x527883[_0x184fad(0x233)]=!0x0,_0xa31b62&&(_0x527883[_0x184fad(0x1ed)]=!0x0),!_0x4951dc){var _0x4b7102=[]['concat'](this[_0x184fad(0x17c)](_0x288b3a))[_0x184fad(0x222)](this[_0x184fad(0x173)](_0x288b3a));for(_0x4e9220=0x0,_0xbab98b=_0x4b7102[_0x184fad(0x207)];_0x4e9220<_0xbab98b;_0x4e9220++)if(_0xbbe2c3=_0x4b7102[_0x4e9220],!(_0x3e0231&&_0x57691d[_0x184fad(0x241)](_0xbbe2c3[_0x184fad(0x1a3)]()))&&!this[_0x184fad(0x1a8)](_0x288b3a,_0xbbe2c3,_0x56b912)&&!_0x527883[_0x184fad(0x211)+_0xbbe2c3[_0x184fad(0x1a3)]()]){if(_0x25ec78++,_0x56b912[_0x184fad(0x17d)]++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;break;}if(!_0x56b912[_0x184fad(0x1bd)]&&_0x56b912[_0x184fad(0x184)]&&_0x56b912[_0x184fad(0x17d)]>_0x56b912[_0x184fad(0x1d6)]){_0x4951dc=!0x0;break;}_0x39a411[_0x184fad(0x213)](_0x437a56[_0x184fad(0x19d)](_0x2c5bc6,_0x527883,_0x288b3a,_0x5077a1,_0xbbe2c3,_0x56b912));}}}}}if(_0xf02eda['type']=_0x5077a1,_0x492f17?(_0xf02eda[_0x184fad(0x21e)]=_0x288b3a[_0x184fad(0x17e)](),this[_0x184fad(0x209)](_0x5077a1,_0xf02eda,_0x56b912,_0x5496f8)):_0x5077a1===_0x184fad(0x1d5)?_0xf02eda[_0x184fad(0x21e)]=this[_0x184fad(0x1a6)][_0x184fad(0x19a)](_0x288b3a):_0x5077a1==='bigint'?_0xf02eda['value']=_0x288b3a['toString']():_0x5077a1===_0x184fad(0x1db)?_0xf02eda[_0x184fad(0x21e)]=this[_0x184fad(0x18d)][_0x184fad(0x19a)](_0x288b3a):_0x5077a1==='symbol'&&this['_Symbol']?_0xf02eda['value']=this['_Symbol'][_0x184fad(0x1da)][_0x184fad(0x1a3)][_0x184fad(0x19a)](_0x288b3a):!_0x56b912[_0x184fad(0x1ce)]&&!(_0x5077a1===_0x184fad(0x204)||_0x5077a1===_0x184fad(0x22f))&&(delete _0xf02eda[_0x184fad(0x21e)],_0xf02eda[_0x184fad(0x179)]=!0x0),_0x4951dc&&(_0xf02eda[_0x184fad(0x21c)]=!0x0),_0x10f27f=_0x56b912[_0x184fad(0x1a9)][_0x184fad(0x239)],_0x56b912['node'][_0x184fad(0x239)]=_0xf02eda,this[_0x184fad(0x168)](_0xf02eda,_0x56b912),_0x39a411[_0x184fad(0x207)]){for(_0x4e9220=0x0,_0xbab98b=_0x39a411[_0x184fad(0x207)];_0x4e9220<_0xbab98b;_0x4e9220++)_0x39a411[_0x4e9220](_0x4e9220);}_0x2c5bc6[_0x184fad(0x207)]&&(_0xf02eda['props']=_0x2c5bc6);}catch(_0x2d88e4){_0x2305db(_0x2d88e4,_0xf02eda,_0x56b912);}return this[_0x184fad(0x20e)](_0x288b3a,_0xf02eda),this[_0x184fad(0x1f8)](_0xf02eda,_0x56b912),_0x56b912[_0x184fad(0x1a9)][_0x184fad(0x239)]=_0x10f27f,_0x56b912[_0x184fad(0x246)]--,_0x56b912[_0x184fad(0x184)]=_0x8f7264,_0x56b912[_0x184fad(0x184)]&&_0x56b912['autoExpandPreviousObjects']['pop'](),_0xf02eda;}[_0x5af063(0x173)](_0x142573){var _0x2dc2fd=_0x5af063;return Object[_0x2dc2fd(0x1ab)]?Object[_0x2dc2fd(0x1ab)](_0x142573):[];}[_0x5af063(0x1d8)](_0x54ee1b){var _0x8d9f13=_0x5af063;return!!(_0x54ee1b&&_0x1f6727[_0x8d9f13(0x219)]&&this[_0x8d9f13(0x1b6)](_0x54ee1b)===_0x8d9f13(0x1fb)&&_0x54ee1b[_0x8d9f13(0x1b5)]);}['_blacklistedProperty'](_0x4513b0,_0x4d6aa5,_0x319429){var _0x818b6d=_0x5af063;return _0x319429[_0x818b6d(0x1a7)]?typeof _0x4513b0[_0x4d6aa5]=='function':!0x1;}[_0x5af063(0x19c)](_0xc03506){var _0x2839b2=_0x5af063,_0x724e1c='';return _0x724e1c=typeof _0xc03506,_0x724e1c===_0x2839b2(0x195)?this[_0x2839b2(0x1b6)](_0xc03506)==='[object\\x20Array]'?_0x724e1c=_0x2839b2(0x1d0):this[_0x2839b2(0x1b6)](_0xc03506)==='[object\\x20Date]'?_0x724e1c=_0x2839b2(0x1d5):this['_objectToString'](_0xc03506)===_0x2839b2(0x20b)?_0x724e1c='bigint':_0xc03506===null?_0x724e1c=_0x2839b2(0x204):_0xc03506[_0x2839b2(0x167)]&&(_0x724e1c=_0xc03506[_0x2839b2(0x167)][_0x2839b2(0x172)]||_0x724e1c):_0x724e1c===_0x2839b2(0x22f)&&this[_0x2839b2(0x18b)]&&_0xc03506 instanceof this[_0x2839b2(0x18b)]&&(_0x724e1c=_0x2839b2(0x247)),_0x724e1c;}[_0x5af063(0x1b6)](_0x1d68c7){var _0x591b0b=_0x5af063;return Object['prototype'][_0x591b0b(0x1a3)][_0x591b0b(0x19a)](_0x1d68c7);}[_0x5af063(0x235)](_0x1ea4aa){var _0x10fdf2=_0x5af063;return _0x1ea4aa===_0x10fdf2(0x1d2)||_0x1ea4aa===_0x10fdf2(0x166)||_0x1ea4aa==='number';}['_isPrimitiveWrapperType'](_0x5cc1bb){var _0x4927ee=_0x5af063;return _0x5cc1bb===_0x4927ee(0x1fc)||_0x5cc1bb==='String'||_0x5cc1bb===_0x4927ee(0x216);}[_0x5af063(0x1fe)](_0x7e31e0,_0x3c4f94,_0x5007a9,_0x459087,_0x4d27be,_0x29729a){var _0x520995=this;return function(_0x231492){var _0x116a3c=_0x4662,_0xa5d8b3=_0x4d27be[_0x116a3c(0x1a9)][_0x116a3c(0x239)],_0x2fb003=_0x4d27be['node'][_0x116a3c(0x215)],_0xa480cd=_0x4d27be[_0x116a3c(0x1a9)][_0x116a3c(0x244)];_0x4d27be[_0x116a3c(0x1a9)]['parent']=_0xa5d8b3,_0x4d27be['node'][_0x116a3c(0x215)]=typeof _0x459087==_0x116a3c(0x1ad)?_0x459087:_0x231492,_0x7e31e0[_0x116a3c(0x213)](_0x520995[_0x116a3c(0x249)](_0x3c4f94,_0x5007a9,_0x459087,_0x4d27be,_0x29729a)),_0x4d27be[_0x116a3c(0x1a9)][_0x116a3c(0x244)]=_0xa480cd,_0x4d27be[_0x116a3c(0x1a9)]['index']=_0x2fb003;};}[_0x5af063(0x19d)](_0x422353,_0x13ec9c,_0x47fc19,_0x324bd1,_0x14dd54,_0x2f1d38,_0x5d7bbe){var _0x36e143=_0x5af063,_0x299d8a=this;return _0x13ec9c[_0x36e143(0x211)+_0x14dd54[_0x36e143(0x1a3)]()]=!0x0,function(_0x42a8d7){var _0x4bdb94=_0x36e143,_0x337560=_0x2f1d38['node'][_0x4bdb94(0x239)],_0x5572a6=_0x2f1d38[_0x4bdb94(0x1a9)][_0x4bdb94(0x215)],_0x53a13e=_0x2f1d38[_0x4bdb94(0x1a9)][_0x4bdb94(0x244)];_0x2f1d38[_0x4bdb94(0x1a9)]['parent']=_0x337560,_0x2f1d38['node'][_0x4bdb94(0x215)]=_0x42a8d7,_0x422353[_0x4bdb94(0x213)](_0x299d8a[_0x4bdb94(0x249)](_0x47fc19,_0x324bd1,_0x14dd54,_0x2f1d38,_0x5d7bbe)),_0x2f1d38['node'][_0x4bdb94(0x244)]=_0x53a13e,_0x2f1d38[_0x4bdb94(0x1a9)]['index']=_0x5572a6;};}[_0x5af063(0x249)](_0x4c77d3,_0x51bcb8,_0x1c18a2,_0xed89a3,_0x1923e8){var _0x10ecbd=_0x5af063,_0x26d093=this;_0x1923e8||(_0x1923e8=function(_0x3da88c,_0xdf9ef9){return _0x3da88c[_0xdf9ef9];});var _0x148fad=_0x1c18a2[_0x10ecbd(0x1a3)](),_0x274b51=_0xed89a3[_0x10ecbd(0x176)]||{},_0xecbb1a=_0xed89a3[_0x10ecbd(0x1ce)],_0x53beba=_0xed89a3[_0x10ecbd(0x1bd)];try{var _0x1ed549=this[_0x10ecbd(0x1bc)](_0x4c77d3),_0x145053=_0x148fad;_0x1ed549&&_0x145053[0x0]==='\\x27'&&(_0x145053=_0x145053[_0x10ecbd(0x1b9)](0x1,_0x145053[_0x10ecbd(0x207)]-0x2));var _0x2254f6=_0xed89a3[_0x10ecbd(0x176)]=_0x274b51[_0x10ecbd(0x211)+_0x145053];_0x2254f6&&(_0xed89a3[_0x10ecbd(0x1ce)]=_0xed89a3[_0x10ecbd(0x1ce)]+0x1),_0xed89a3[_0x10ecbd(0x1bd)]=!!_0x2254f6;var _0x1f4614=typeof _0x1c18a2==_0x10ecbd(0x228),_0x9a7a9d={'name':_0x1f4614||_0x1ed549?_0x148fad:this['_propertyName'](_0x148fad)};if(_0x1f4614&&(_0x9a7a9d['symbol']=!0x0),!(_0x51bcb8==='array'||_0x51bcb8===_0x10ecbd(0x1e8))){var _0x5bb0c4=this['_getOwnPropertyDescriptor'](_0x4c77d3,_0x1c18a2);if(_0x5bb0c4&&(_0x5bb0c4[_0x10ecbd(0x212)]&&(_0x9a7a9d[_0x10ecbd(0x1c4)]=!0x0),_0x5bb0c4['get']&&!_0x2254f6&&!_0xed89a3[_0x10ecbd(0x236)]))return _0x9a7a9d[_0x10ecbd(0x16a)]=!0x0,this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3),_0x9a7a9d;}var _0x4d686e;try{_0x4d686e=_0x1923e8(_0x4c77d3,_0x1c18a2);}catch(_0x1790bc){return _0x9a7a9d={'name':_0x148fad,'type':'unknown','error':_0x1790bc[_0x10ecbd(0x182)]},this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3),_0x9a7a9d;}var _0x584511=this[_0x10ecbd(0x19c)](_0x4d686e),_0x4d4015=this[_0x10ecbd(0x235)](_0x584511);if(_0x9a7a9d['type']=_0x584511,_0x4d4015)this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3,_0x4d686e,function(){var _0x41822b=_0x10ecbd;_0x9a7a9d[_0x41822b(0x21e)]=_0x4d686e['valueOf'](),!_0x2254f6&&_0x26d093[_0x41822b(0x209)](_0x584511,_0x9a7a9d,_0xed89a3,{});});else{var _0x23ba5f=_0xed89a3[_0x10ecbd(0x184)]&&_0xed89a3[_0x10ecbd(0x246)]<_0xed89a3['autoExpandMaxDepth']&&_0xed89a3['autoExpandPreviousObjects'][_0x10ecbd(0x198)](_0x4d686e)<0x0&&_0x584511!=='function'&&_0xed89a3[_0x10ecbd(0x17d)]<_0xed89a3[_0x10ecbd(0x1d6)];_0x23ba5f||_0xed89a3['level']<_0xecbb1a||_0x2254f6?(this[_0x10ecbd(0x231)](_0x9a7a9d,_0x4d686e,_0xed89a3,_0x2254f6||{}),this[_0x10ecbd(0x20e)](_0x4d686e,_0x9a7a9d)):this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3,_0x4d686e,function(){var _0x5c5c95=_0x10ecbd;_0x584511===_0x5c5c95(0x204)||_0x584511===_0x5c5c95(0x22f)||(delete _0x9a7a9d['value'],_0x9a7a9d['capped']=!0x0);});}return _0x9a7a9d;}finally{_0xed89a3[_0x10ecbd(0x176)]=_0x274b51,_0xed89a3[_0x10ecbd(0x1ce)]=_0xecbb1a,_0xed89a3[_0x10ecbd(0x1bd)]=_0x53beba;}}[_0x5af063(0x209)](_0x4453a8,_0x3e7b4a,_0xb630fa,_0x3f9558){var _0x5a2672=_0x5af063,_0x214d4a=_0x3f9558['strLength']||_0xb630fa[_0x5a2672(0x20a)];if((_0x4453a8===_0x5a2672(0x166)||_0x4453a8===_0x5a2672(0x171))&&_0x3e7b4a[_0x5a2672(0x21e)]){let _0x1fea08=_0x3e7b4a[_0x5a2672(0x21e)][_0x5a2672(0x207)];_0xb630fa[_0x5a2672(0x1f1)]+=_0x1fea08,_0xb630fa[_0x5a2672(0x1f1)]>_0xb630fa[_0x5a2672(0x200)]?(_0x3e7b4a[_0x5a2672(0x179)]='',delete _0x3e7b4a[_0x5a2672(0x21e)]):_0x1fea08>_0x214d4a&&(_0x3e7b4a[_0x5a2672(0x179)]=_0x3e7b4a[_0x5a2672(0x21e)]['substr'](0x0,_0x214d4a),delete _0x3e7b4a[_0x5a2672(0x21e)]);}}[_0x5af063(0x1bc)](_0x1f9995){var _0x5710fc=_0x5af063;return!!(_0x1f9995&&_0x1f6727['Map']&&this[_0x5710fc(0x1b6)](_0x1f9995)===_0x5710fc(0x1d4)&&_0x1f9995[_0x5710fc(0x1b5)]);}[_0x5af063(0x1bf)](_0x3c69c3){var _0x16c749=_0x5af063;if(_0x3c69c3['match'](/^\\d+$/))return _0x3c69c3;var _0x4b2469;try{_0x4b2469=JSON[_0x16c749(0x18a)](''+_0x3c69c3);}catch{_0x4b2469='\\x22'+this[_0x16c749(0x1b6)](_0x3c69c3)+'\\x22';}return _0x4b2469[_0x16c749(0x1be)](/^\"([a-zA-Z_][a-zA-Z_0-9]*)\"$/)?_0x4b2469=_0x4b2469['substr'](0x1,_0x4b2469[_0x16c749(0x207)]-0x2):_0x4b2469=_0x4b2469[_0x16c749(0x243)](/'/g,'\\x5c\\x27')[_0x16c749(0x243)](/\\\\\"/g,'\\x22')[_0x16c749(0x243)](/(^\"|\"$)/g,'\\x27'),_0x4b2469;}[_0x5af063(0x20d)](_0x136533,_0x13377b,_0x3feb8a,_0x449c52){var _0x1a9440=_0x5af063;this[_0x1a9440(0x168)](_0x136533,_0x13377b),_0x449c52&&_0x449c52(),this['_additionalMetadata'](_0x3feb8a,_0x136533),this['_treeNodePropertiesAfterFullValue'](_0x136533,_0x13377b);}[_0x5af063(0x168)](_0x1f3ba1,_0x5cf79a){var _0x33121d=_0x5af063;this['_setNodeId'](_0x1f3ba1,_0x5cf79a),this['_setNodeQueryPath'](_0x1f3ba1,_0x5cf79a),this['_setNodeExpressionPath'](_0x1f3ba1,_0x5cf79a),this[_0x33121d(0x1b2)](_0x1f3ba1,_0x5cf79a);}[_0x5af063(0x1d3)](_0x7b442f,_0x2734dd){}[_0x5af063(0x1af)](_0x212be5,_0x15c572){}[_0x5af063(0x1c9)](_0xc805b2,_0x5d04af){}[_0x5af063(0x1e5)](_0x23e2bd){return _0x23e2bd===this['_undefined'];}[_0x5af063(0x1f8)](_0x3a98d7,_0x2ce624){var _0x4bfd69=_0x5af063;this[_0x4bfd69(0x1c9)](_0x3a98d7,_0x2ce624),this[_0x4bfd69(0x1c1)](_0x3a98d7),_0x2ce624['sortProps']&&this[_0x4bfd69(0x1e6)](_0x3a98d7),this['_addFunctionsNode'](_0x3a98d7,_0x2ce624),this[_0x4bfd69(0x1eb)](_0x3a98d7,_0x2ce624),this[_0x4bfd69(0x20c)](_0x3a98d7);}['_additionalMetadata'](_0x57a3d6,_0x4ef3c3){var _0x2078c2=_0x5af063;let _0x565fc8;try{_0x1f6727[_0x2078c2(0x1bb)]&&(_0x565fc8=_0x1f6727[_0x2078c2(0x1bb)][_0x2078c2(0x199)],_0x1f6727['console']['error']=function(){}),_0x57a3d6&&typeof _0x57a3d6[_0x2078c2(0x207)]==_0x2078c2(0x1ad)&&(_0x4ef3c3['length']=_0x57a3d6[_0x2078c2(0x207)]);}catch{}finally{_0x565fc8&&(_0x1f6727[_0x2078c2(0x1bb)][_0x2078c2(0x199)]=_0x565fc8);}if(_0x4ef3c3[_0x2078c2(0x16e)]==='number'||_0x4ef3c3['type']===_0x2078c2(0x216)){if(isNaN(_0x4ef3c3[_0x2078c2(0x21e)]))_0x4ef3c3[_0x2078c2(0x23e)]=!0x0,delete _0x4ef3c3[_0x2078c2(0x21e)];else switch(_0x4ef3c3['value']){case Number[_0x2078c2(0x15e)]:_0x4ef3c3['positiveInfinity']=!0x0,delete _0x4ef3c3['value'];break;case Number[_0x2078c2(0x1e9)]:_0x4ef3c3[_0x2078c2(0x194)]=!0x0,delete _0x4ef3c3[_0x2078c2(0x21e)];break;case 0x0:this[_0x2078c2(0x1f6)](_0x4ef3c3['value'])&&(_0x4ef3c3['negativeZero']=!0x0);break;}}else _0x4ef3c3[_0x2078c2(0x16e)]===_0x2078c2(0x240)&&typeof _0x57a3d6['name']==_0x2078c2(0x166)&&_0x57a3d6[_0x2078c2(0x172)]&&_0x4ef3c3[_0x2078c2(0x172)]&&_0x57a3d6[_0x2078c2(0x172)]!==_0x4ef3c3[_0x2078c2(0x172)]&&(_0x4ef3c3[_0x2078c2(0x1dc)]=_0x57a3d6[_0x2078c2(0x172)]);}[_0x5af063(0x1f6)](_0x4dc4ff){var _0x6bca7e=_0x5af063;return 0x1/_0x4dc4ff===Number[_0x6bca7e(0x1e9)];}['_sortProps'](_0x2680c5){var _0x966ef2=_0x5af063;!_0x2680c5[_0x966ef2(0x164)]||!_0x2680c5[_0x966ef2(0x164)][_0x966ef2(0x207)]||_0x2680c5[_0x966ef2(0x16e)]===_0x966ef2(0x1d0)||_0x2680c5[_0x966ef2(0x16e)]===_0x966ef2(0x1c2)||_0x2680c5['type']===_0x966ef2(0x219)||_0x2680c5[_0x966ef2(0x164)][_0x966ef2(0x218)](function(_0x3011df,_0x9f912){var _0x24dc37=_0x966ef2,_0x156c54=_0x3011df[_0x24dc37(0x172)]['toLowerCase'](),_0x40c8fa=_0x9f912[_0x24dc37(0x172)][_0x24dc37(0x186)]();return _0x156c54<_0x40c8fa?-0x1:_0x156c54>_0x40c8fa?0x1:0x0;});}['_addFunctionsNode'](_0x65417c,_0x262232){var _0x41b6f8=_0x5af063;if(!(_0x262232['noFunctions']||!_0x65417c['props']||!_0x65417c[_0x41b6f8(0x164)][_0x41b6f8(0x207)])){for(var _0x2a257b=[],_0x3d7a05=[],_0x293f53=0x0,_0xca3b18=_0x65417c[_0x41b6f8(0x164)]['length'];_0x293f53<_0xca3b18;_0x293f53++){var _0x12b9c4=_0x65417c[_0x41b6f8(0x164)][_0x293f53];_0x12b9c4[_0x41b6f8(0x16e)]==='function'?_0x2a257b[_0x41b6f8(0x213)](_0x12b9c4):_0x3d7a05[_0x41b6f8(0x213)](_0x12b9c4);}if(!(!_0x3d7a05[_0x41b6f8(0x207)]||_0x2a257b[_0x41b6f8(0x207)]<=0x1)){_0x65417c[_0x41b6f8(0x164)]=_0x3d7a05;var _0x289400={'functionsNode':!0x0,'props':_0x2a257b};this['_setNodeId'](_0x289400,_0x262232),this['_setNodeLabel'](_0x289400,_0x262232),this[_0x41b6f8(0x1c1)](_0x289400),this['_setNodePermissions'](_0x289400,_0x262232),_0x289400['id']+='\\x20f',_0x65417c['props'][_0x41b6f8(0x221)](_0x289400);}}}[_0x5af063(0x1eb)](_0x4d715f,_0x220d3c){}[_0x5af063(0x1c1)](_0x16f99c){}[_0x5af063(0x196)](_0xc38c21){var _0x7e78b7=_0x5af063;return Array[_0x7e78b7(0x16b)](_0xc38c21)||typeof _0xc38c21==_0x7e78b7(0x195)&&this[_0x7e78b7(0x1b6)](_0xc38c21)==='[object\\x20Array]';}[_0x5af063(0x1b2)](_0x4100e0,_0x8a5b3f){}[_0x5af063(0x20c)](_0x16da5c){var _0x2698a6=_0x5af063;delete _0x16da5c[_0x2698a6(0x19f)],delete _0x16da5c[_0x2698a6(0x1a2)],delete _0x16da5c['_hasMapOnItsPath'];}[_0x5af063(0x1b3)](_0x168392,_0x4cdfa5){}}let _0x58b25a=new _0x2ed115(),_0x1e2069={'props':0x64,'elements':0x64,'strLength':0x400*0x32,'totalStrLength':0x400*0x32,'autoExpandLimit':0x1388,'autoExpandMaxDepth':0xa},_0x482273={'props':0x5,'elements':0x5,'strLength':0x100,'totalStrLength':0x100*0x3,'autoExpandLimit':0x1e,'autoExpandMaxDepth':0x2};function _0x1b64e7(_0x358f3e,_0x2b1c5e,_0x2ef411,_0x2c6c7d,_0x2355d5,_0xe8098b){var _0x1ee9e4=_0x5af063;let _0x447a87,_0x2ec6fe;try{_0x2ec6fe=_0x228df4(),_0x447a87=_0x13677e[_0x2b1c5e],!_0x447a87||_0x2ec6fe-_0x447a87['ts']>0x1f4&&_0x447a87[_0x1ee9e4(0x1c6)]&&_0x447a87[_0x1ee9e4(0x224)]/_0x447a87[_0x1ee9e4(0x1c6)]<0x64?(_0x13677e[_0x2b1c5e]=_0x447a87={'count':0x0,'time':0x0,'ts':_0x2ec6fe},_0x13677e[_0x1ee9e4(0x16f)]={}):_0x2ec6fe-_0x13677e[_0x1ee9e4(0x16f)]['ts']>0x32&&_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1c6)]&&_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x224)]/_0x13677e[_0x1ee9e4(0x16f)]['count']<0x64&&(_0x13677e[_0x1ee9e4(0x16f)]={});let _0x4abbc1=[],_0x3c22ff=_0x447a87[_0x1ee9e4(0x1f4)]||_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1f4)]?_0x482273:_0x1e2069,_0x2e1a0a=_0x31586c=>{var _0x133c96=_0x1ee9e4;let _0x37eb23={};return _0x37eb23[_0x133c96(0x164)]=_0x31586c[_0x133c96(0x164)],_0x37eb23[_0x133c96(0x16d)]=_0x31586c[_0x133c96(0x16d)],_0x37eb23['strLength']=_0x31586c[_0x133c96(0x20a)],_0x37eb23[_0x133c96(0x200)]=_0x31586c[_0x133c96(0x200)],_0x37eb23[_0x133c96(0x1d6)]=_0x31586c[_0x133c96(0x1d6)],_0x37eb23['autoExpandMaxDepth']=_0x31586c['autoExpandMaxDepth'],_0x37eb23['sortProps']=!0x1,_0x37eb23['noFunctions']=!_0x6a2aa7,_0x37eb23[_0x133c96(0x1ce)]=0x1,_0x37eb23[_0x133c96(0x246)]=0x0,_0x37eb23[_0x133c96(0x1f9)]=_0x133c96(0x18e),_0x37eb23[_0x133c96(0x1cf)]='root_exp',_0x37eb23['autoExpand']=!0x0,_0x37eb23[_0x133c96(0x19e)]=[],_0x37eb23[_0x133c96(0x17d)]=0x0,_0x37eb23[_0x133c96(0x236)]=!0x0,_0x37eb23[_0x133c96(0x1f1)]=0x0,_0x37eb23[_0x133c96(0x1a9)]={'current':void 0x0,'parent':void 0x0,'index':0x0},_0x37eb23;};for(var _0x4e099a=0x0;_0x4e099a<_0x2355d5[_0x1ee9e4(0x207)];_0x4e099a++)_0x4abbc1[_0x1ee9e4(0x213)](_0x58b25a['serialize']({'timeNode':_0x358f3e===_0x1ee9e4(0x224)||void 0x0},_0x2355d5[_0x4e099a],_0x2e1a0a(_0x3c22ff),{}));if(_0x358f3e==='trace'){let _0x54ef3f=Error[_0x1ee9e4(0x1ff)];try{Error[_0x1ee9e4(0x1ff)]=0x1/0x0,_0x4abbc1[_0x1ee9e4(0x213)](_0x58b25a[_0x1ee9e4(0x231)]({'stackNode':!0x0},new Error()[_0x1ee9e4(0x220)],_0x2e1a0a(_0x3c22ff),{'strLength':0x1/0x0}));}finally{Error[_0x1ee9e4(0x1ff)]=_0x54ef3f;}}return{'method':_0x1ee9e4(0x21d),'version':_0x3ba611,'args':[{'ts':_0x2ef411,'session':_0x2c6c7d,'args':_0x4abbc1,'id':_0x2b1c5e,'context':_0xe8098b}]};}catch(_0x3ae9ad){return{'method':'log','version':_0x3ba611,'args':[{'ts':_0x2ef411,'session':_0x2c6c7d,'args':[{'type':_0x1ee9e4(0x193),'error':_0x3ae9ad&&_0x3ae9ad[_0x1ee9e4(0x182)]}],'id':_0x2b1c5e,'context':_0xe8098b}]};}finally{try{if(_0x447a87&&_0x2ec6fe){let _0x18c234=_0x228df4();_0x447a87[_0x1ee9e4(0x1c6)]++,_0x447a87[_0x1ee9e4(0x224)]+=_0x9b0d6(_0x2ec6fe,_0x18c234),_0x447a87['ts']=_0x18c234,_0x13677e[_0x1ee9e4(0x16f)]['count']++,_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x224)]+=_0x9b0d6(_0x2ec6fe,_0x18c234),_0x13677e[_0x1ee9e4(0x16f)]['ts']=_0x18c234,(_0x447a87[_0x1ee9e4(0x1c6)]>0x32||_0x447a87[_0x1ee9e4(0x224)]>0x64)&&(_0x447a87[_0x1ee9e4(0x1f4)]=!0x0),(_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1c6)]>0x3e8||_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x224)]>0x12c)&&(_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1f4)]=!0x0);}}catch{}}}return _0x1b64e7;}((_0x31bb6e,_0x2d9556,_0x4dead3,_0x3b0283,_0x4c9494,_0x3f24eb,_0x5cd1de,_0x1bc888,_0x2d94e2,_0x14b22a)=>{var _0x1e9d92=_0x549d36;if(_0x31bb6e[_0x1e9d92(0x1ee)])return _0x31bb6e[_0x1e9d92(0x1ee)];if(!J(_0x31bb6e,_0x1bc888,_0x4c9494))return _0x31bb6e['_console_ninja']={'consoleLog':()=>{},'consoleTrace':()=>{},'consoleTime':()=>{},'consoleTimeEnd':()=>{},'autoLog':()=>{},'autoLogMany':()=>{},'autoTraceMany':()=>{},'coverage':()=>{},'autoTrace':()=>{},'autoTime':()=>{},'autoTimeEnd':()=>{}},_0x31bb6e[_0x1e9d92(0x1ee)];let _0x26bb29=W(_0x31bb6e),_0xbbfdd0=_0x26bb29['elapsed'],_0x49cd0a=_0x26bb29[_0x1e9d92(0x223)],_0x31ad1c=_0x26bb29[_0x1e9d92(0x23b)],_0x2d25ad={'hits':{},'ts':{}},_0x336418=Y(_0x31bb6e,_0x2d94e2,_0x2d25ad,_0x3f24eb),_0x3c3358=_0x31ee18=>{_0x2d25ad['ts'][_0x31ee18]=_0x49cd0a();},_0x54bb98=(_0x2645ae,_0x1421a8)=>{let _0x20fd96=_0x2d25ad['ts'][_0x1421a8];if(delete _0x2d25ad['ts'][_0x1421a8],_0x20fd96){let _0x3e717e=_0xbbfdd0(_0x20fd96,_0x49cd0a());_0xbfa3dc(_0x336418('time',_0x2645ae,_0x31ad1c(),_0xf14f39,[_0x3e717e],_0x1421a8));}},_0x37441e=_0x4e131a=>_0x376385=>{var _0x150efb=_0x1e9d92;try{_0x3c3358(_0x376385),_0x4e131a(_0x376385);}finally{_0x31bb6e[_0x150efb(0x1bb)]['time']=_0x4e131a;}},_0x235981=_0x4b6c9d=>_0x1a1c95=>{var _0x411dfc=_0x1e9d92;try{let [_0x5a4756,_0x10ada]=_0x1a1c95[_0x411dfc(0x1df)](_0x411dfc(0x237));_0x54bb98(_0x10ada,_0x5a4756),_0x4b6c9d(_0x5a4756);}finally{_0x31bb6e[_0x411dfc(0x1bb)][_0x411dfc(0x1d7)]=_0x4b6c9d;}};_0x31bb6e[_0x1e9d92(0x1ee)]={'consoleLog':(_0x28823e,_0x561e08)=>{var _0x1fc52b=_0x1e9d92;_0x31bb6e[_0x1fc52b(0x1bb)]['log']['name']!=='disabledLog'&&_0xbfa3dc(_0x336418(_0x1fc52b(0x21d),_0x28823e,_0x31ad1c(),_0xf14f39,_0x561e08));},'consoleTrace':(_0x57a04d,_0x1fa011)=>{var _0x2a636c=_0x1e9d92;_0x31bb6e['console'][_0x2a636c(0x21d)][_0x2a636c(0x172)]!==_0x2a636c(0x22c)&&_0xbfa3dc(_0x336418(_0x2a636c(0x180),_0x57a04d,_0x31ad1c(),_0xf14f39,_0x1fa011));},'consoleTime':()=>{var _0x512d47=_0x1e9d92;_0x31bb6e[_0x512d47(0x1bb)][_0x512d47(0x224)]=_0x37441e(_0x31bb6e[_0x512d47(0x1bb)]['time']);},'consoleTimeEnd':()=>{var _0x49c317=_0x1e9d92;_0x31bb6e['console'][_0x49c317(0x1d7)]=_0x235981(_0x31bb6e[_0x49c317(0x1bb)][_0x49c317(0x1d7)]);},'autoLog':(_0x5adb04,_0x315ecc)=>{var _0x1fe94c=_0x1e9d92;_0xbfa3dc(_0x336418(_0x1fe94c(0x21d),_0x315ecc,_0x31ad1c(),_0xf14f39,[_0x5adb04]));},'autoLogMany':(_0x4bd873,_0x5d566c)=>{var _0x43091f=_0x1e9d92;_0xbfa3dc(_0x336418(_0x43091f(0x21d),_0x4bd873,_0x31ad1c(),_0xf14f39,_0x5d566c));},'autoTrace':(_0x3ebc28,_0x429234)=>{_0xbfa3dc(_0x336418('trace',_0x429234,_0x31ad1c(),_0xf14f39,[_0x3ebc28]));},'autoTraceMany':(_0x16e9c8,_0x104612)=>{var _0x2adf6d=_0x1e9d92;_0xbfa3dc(_0x336418(_0x2adf6d(0x180),_0x16e9c8,_0x31ad1c(),_0xf14f39,_0x104612));},'autoTime':(_0x5c2782,_0x6a1db7,_0x19234f)=>{_0x3c3358(_0x19234f);},'autoTimeEnd':(_0x742404,_0x2f4835,_0x2a1937)=>{_0x54bb98(_0x2f4835,_0x2a1937);},'coverage':_0x2b26bf=>{var _0x464c51=_0x1e9d92;_0xbfa3dc({'method':_0x464c51(0x23f),'version':_0x3f24eb,'args':[{'id':_0x2b26bf}]});}};let _0xbfa3dc=b(_0x31bb6e,_0x2d9556,_0x4dead3,_0x3b0283,_0x4c9494,_0x14b22a),_0xf14f39=_0x31bb6e['_console_ninja_session'];return _0x31bb6e[_0x1e9d92(0x1ee)];})(globalThis,_0x549d36(0x225),_0x549d36(0x1f7),\"c:\\\\Users\\\\Ali\\\\.vscode\\\\extensions\\\\wallabyjs.console-ninja-1.0.288\\\\node_modules\",_0x549d36(0x1e4),'1.0.0','1708742827353',_0x549d36(0x1c5),_0x549d36(0x205),_0x549d36(0x161));");}catch(e){}};/* istanbul ignore next */function oo_oo(i,...v){try{oo_cm().consoleLog(i, v);}catch(e){} return v};/* istanbul ignore next */function oo_tr(i,...v){try{oo_cm().consoleTrace(i, v);}catch(e){} return v};/* istanbul ignore next */function oo_ts(){try{oo_cm().consoleTime();}catch(e){}};/* istanbul ignore next */function oo_te(){try{oo_cm().consoleTimeEnd();}catch(e){}};/*eslint unicorn/no-abusive-eslint-disable:,eslint-comments/disable-enable-pair:,eslint-comments/no-unlimited-disable:,eslint-comments/no-aggregating-enable:,eslint-comments/no-duplicate-disable:,eslint-comments/no-unused-disable:,eslint-comments/no-unused-enable:,*/
"use strict";

// Class definition
var KTFeedback = function(options) {
    ////////////////////////////
    // ** Private Variables  ** //
    ////////////////////////////
    var the = this;

    // Default options
    var defaultOptions = {
        'width' : 100,
        'placement' : 'top-center',
        'content' : '',
        'type': 'popup'
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    var _construct = function() {
        _init();
    }

    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('feedback');
        the.element;
        the.shown = false;

        // Event Handlers
        _handlers();

        // Bind Instance
        KTUtil.data(the.element).set('feedback', the);
    }

    var _handlers = function() {
        KTUtil.addEvent(the.element, 'click', function(e) {
            e.preventDefault();

            _go();
        });
    }

    var _show = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.feedback.show', the) === false ) {
            return;
        }

        if ( the.options.type === 'popup') {
            _showPopup();
        }

        KTEventHandler.trigger(the.element, 'kt.feedback.shown', the);

        return the;
    }

    var _hide = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.feedback.hide', the) === false ) {
            return;
        }

        if ( the.options.type === 'popup') {
            _hidePopup();
        }

        the.shown = false;

        KTEventHandler.trigger(the.element, 'kt.feedback.hidden', the);

        return the;
    }

    var _showPopup = function() {
        the.element = document.createElement("DIV");

        KTUtil.addClass(the.element, 'feedback feedback-popup');
        KTUtil.setHTML(the.element, the.options.content);

        if (the.options.placement == 'top-center') {
            _setPopupTopCenterPosition();
        }

        document.body.appendChild(the.element);

        KTUtil.addClass(the.element, 'feedback-shown');

        the.shown = true;
    }

    var _setPopupTopCenterPosition = function() {
        var width = KTUtil.getResponsiveValue(the.options.width);
        var height = KTUtil.css(the.element, 'height');

        KTUtil.addClass(the.element, 'feedback-top-center');

        KTUtil.css(the.element, 'width', width);
        KTUtil.css(the.element, 'left', '50%');
        KTUtil.css(the.element, 'top', '-' + height);
    }

    var _hidePopup = function() {
        the.element.remove();
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('feedback');
    }

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.show = function() {
        return _show();
    }

    the.hide = function() {
        return _hide();
    }

    the.isShown = function() {
        return the.shown;
    }

    the.getElement = function() {
        return the.element;
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTFeedback;
}

"use strict";

// Class definition
var KTImageInput = function(element, options) {
    ////////////////////////////
    // ** Private Variables  ** //
    ////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default Options
    var defaultOptions = {
        
    };

    ////////////////////////////
    // ** Private Methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('image-input') === true ) {
            the = KTUtil.data(element).get('image-input');
        } else {
            _init();
        }
    }

    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('image-input');

        // Elements
        the.element = element;
        the.inputElement = KTUtil.find(element, 'input[type="file"]');
        the.wrapperElement = KTUtil.find(element, '.image-input-wrapper');
        the.cancelElement = KTUtil.find(element, '[data-kt-image-input-action="cancel"]');
        the.removeElement = KTUtil.find(element, '[data-kt-image-input-action="remove"]');
        the.hiddenElement = KTUtil.find(element, 'input[type="hidden"]');
        the.src = KTUtil.css(the.wrapperElement, 'backgroundImage');

        // Set initialized
        the.element.setAttribute('data-kt-image-input', 'true');

        // Event Handlers
        _handlers();

        // Bind Instance
        KTUtil.data(the.element).set('image-input', the);
    }

    // Init Event Handlers
    var _handlers = function() {
        KTUtil.addEvent(the.inputElement, 'change', _change);
        KTUtil.addEvent(the.cancelElement, 'click', _cancel);
        KTUtil.addEvent(the.removeElement, 'click', _remove);
    }

    // Event Handlers
    var _change = function(e) {
        e.preventDefault();

        if ( the.inputElement !== null && the.inputElement.files && the.inputElement.files[0] ) {
            // Fire change event
            if ( KTEventHandler.trigger(the.element, 'kt.imageinput.change', the) === false ) {
                return;
            }

            var reader = new FileReader();

            reader.onload = function(e) {
                KTUtil.css(the.wrapperElement, 'background-image', 'url('+ e.target.result +')');
            }

            reader.readAsDataURL(the.inputElement.files[0]);

            the.element.classList.add('image-input-changed');
            the.element.classList.remove('image-input-empty');

            // Fire removed event
            KTEventHandler.trigger(the.element, 'kt.imageinput.changed', the);
        }
    }

    var _cancel = function(e) {
        e.preventDefault();

        // Fire cancel event
        if ( KTEventHandler.trigger(the.element, 'kt.imageinput.cancel', the) === false ) {
            return;
        }

        the.element.classList.remove('image-input-changed');
        the.element.classList.remove('image-input-empty');

        if (the.src === 'none') {   
            KTUtil.css(the.wrapperElement, 'background-image', '');
            the.element.classList.add('image-input-empty');
        } else {
            KTUtil.css(the.wrapperElement, 'background-image', the.src);
        }
        
        the.inputElement.value = "";

        if ( the.hiddenElement !== null ) {
            the.hiddenElement.value = "0";
        }

        // Fire canceled event
        KTEventHandler.trigger(the.element, 'kt.imageinput.canceled', the);
    }

    var _remove = function(e) {
        e.preventDefault();

        // Fire remove event
        if ( KTEventHandler.trigger(the.element, 'kt.imageinput.remove', the) === false ) {
            return;
        }

        the.element.classList.remove('image-input-changed');
        the.element.classList.add('image-input-empty');

        KTUtil.css(the.wrapperElement, 'background-image', "none");
        the.inputElement.value = "";

        if ( the.hiddenElement !== null ) {
            the.hiddenElement.value = "1";
        }

        // Fire removed event
        KTEventHandler.trigger(the.element, 'kt.imageinput.removed', the);
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('image-input');
    }

    // Construct Class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.getInputElement = function() {
        return the.inputElement;
    }

    the.getElement = function() {
        return the.element;
    }
    
    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTImageInput.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('image-input') ) {
        return KTUtil.data(element).get('image-input');
    } else {
        return null;
    }
}

// Create instances
KTImageInput.createInstances = function(selector = '[data-kt-image-input]') {
    // Initialize Menus
    var elements = document.querySelectorAll(selector);

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            new KTImageInput(elements[i]);
        }
    }
}

// Global initialization
KTImageInput.init = function() {
    KTImageInput.createInstances();
};

// Webpack Support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTImageInput;
}

"use strict";

var KTMenuHandlersInitialized = false;

// Class definition
var KTMenu = function(element, options) {
    ////////////////////////////
    // ** Private Variables  ** //
    ////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default Options
    var defaultOptions = {
        dropdown: {
            hoverTimeout: 200,
            zindex: 107
        },

        accordion: {
            slideSpeed: 250,
            expand: false
        }
    };

    ////////////////////////////
    // ** Private Methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('menu') === true ) {
            the = KTUtil.data(element).get('menu');
        } else {
            _init();
        }
    }

    var _init = function() {
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('menu');
        the.element = element;
        the.triggerElement;
        the.disabled = false;

        // Set initialized
        the.element.setAttribute('data-kt-menu', 'true');

        _setTriggerElement();
        _update();

        KTUtil.data(the.element).set('menu', the);
    }

    var _destroy = function() {  // todo

    }

    // Event Handlers
    // Toggle handler
    var _click = function(element, e) {
        e.preventDefault();

        if (the.disabled === true) {
            return;
        }

        var item = _getItemElement(element);

        if ( _getOptionFromElementAttribute(item, 'trigger') !== 'click' ) {
            return;
        }

        if ( _getOptionFromElementAttribute(item, 'toggle') === false ) {
            _show(item);
        } else {
            _toggle(item);
        }
    }

    // Link handler
    var _link = function(element, e) {
        if (the.disabled === true) {
            return;
        }
        
        if ( KTEventHandler.trigger(the.element, 'kt.menu.link.click', element) === false )  {
            return;
        }

        // Dismiss all shown dropdowns
        KTMenu.hideDropdowns();

        KTEventHandler.trigger(the.element, 'kt.menu.link.clicked', element);
    }

    // Dismiss handler
    var _dismiss = function(element, e) {
        var item = _getItemElement(element);
        var items = _getItemChildElements(item);

        if ( item !== null && _getItemSubType(item) === 'dropdown') {
            _hide(item); // hide items dropdown
            // Hide all child elements as well
            
            if ( items.length > 0 ) {
                for (var i = 0, len = items.length; i < len; i++) {
                    if ( items[i] !== null &&  _getItemSubType(items[i]) === 'dropdown') {
                        _hide(tems[i]);
                    }
                }
            }
        }
    }

    // Mouseover handle
    var _mouseover = function(element, e) {
        var item = _getItemElement(element);

        if (the.disabled === true) {
            return;
        }

        if ( item === null ) {
            return;
        }

        if ( _getOptionFromElementAttribute(item, 'trigger') !== 'hover' ) {
            return;
        }

        if ( KTUtil.data(item).get('hover') === '1' ) {
            clearTimeout(KTUtil.data(item).get('timeout'));
            KTUtil.data(item).remove('hover');
            KTUtil.data(item).remove('timeout');
        }

        _show(item);
    }

    // Mouseout handle
    var _mouseout = function(element, e) {
        var item = _getItemElement(element);

        if (the.disabled === true) {
            return;
        }

        if ( item === null ) {
            return;
        }

        if ( _getOptionFromElementAttribute(item, 'trigger') !== 'hover' ) {
            return;
        }

        var timeout = setTimeout(function() {
            if ( KTUtil.data(item).get('hover') === '1' ) {
                _hide(item);
            }
        }, the.options.dropdown.hoverTimeout);

        KTUtil.data(item).set('hover', '1');
        KTUtil.data(item).set('timeout', timeout);
    }

    // Toggle item sub
    var _toggle = function(item) {
        if ( !item ) {
            item = the.triggerElement;
        }

        if ( _isItemSubShown(item) === true ) {
            _hide(item);
        } else {
            _show(item);
        }
    }

    // Show item sub
    var _show = function(item) {
        if ( !item ) {
            item = the.triggerElement;
        }

        if ( _isItemSubShown(item) === true ) {
            return;
        }

        if ( _getItemSubType(item) === 'dropdown' ) {
            _showDropdown(item); // // show current dropdown
        } else if ( _getItemSubType(item) === 'accordion' ) {
            _showAccordion(item);
        }

        // Remember last submenu type
        KTUtil.data(item).set('type', _getItemSubType(item));  // updated
    }

    // Hide item sub
    var _hide = function(item) {
        if ( !item ) {
            item = the.triggerElement;
        }

        if ( _isItemSubShown(item) === false ) {
            return;
        }
        
        if ( _getItemSubType(item) === 'dropdown' ) {
            _hideDropdown(item);
        } else if ( _getItemSubType(item) === 'accordion' ) {
            _hideAccordion(item);
        }
    }

    // Reset item state classes if item sub type changed
    var _reset = function(item) {        
        if ( _hasItemSub(item) === false ) {
            return;
        }

        var sub = _getItemSubElement(item);

        // Reset sub state if sub type is changed during the window resize
        if ( KTUtil.data(item).has('type') && KTUtil.data(item).get('type') !== _getItemSubType(item) ) {  // updated
            KTUtil.removeClass(item, 'hover'); 
            KTUtil.removeClass(item, 'show'); 
            KTUtil.removeClass(sub, 'show'); 
        }  // updated
    }

    // Update all item state classes if item sub type changed
    var _update = function() {
        var items = the.element.querySelectorAll('.menu-item[data-kt-menu-trigger]');

        if ( items && items.length > 0 ) {
            for (var i = 0, len = items.length; i < len; i++) {
                _reset(items[i]);
            }
        }
    }

    // Set external trigger element
    var _setTriggerElement = function() {
        var target = document.querySelector('[data-kt-menu-target="#' + the.element.getAttribute('id')  + '"]');

        if ( target !== null ) {
            the.triggerElement = target;
        } else if ( the.element.closest('[data-kt-menu-trigger]') ) {
            the.triggerElement = the.element.closest('[data-kt-menu-trigger]');
        } else if ( the.element.parentNode && KTUtil.child(the.element.parentNode, '[data-kt-menu-trigger]')) {
            the.triggerElement = KTUtil.child(the.element.parentNode, '[data-kt-menu-trigger]');
        }

        if ( the.triggerElement ) {
            KTUtil.data(the.triggerElement).set('menu', the);
        }
    }

    // Test if menu has external trigger element
    var _isTriggerElement = function(item) {
        return ( the.triggerElement === item ) ? true : false;
    }

    // Test if item's sub is shown
    var _isItemSubShown = function(item) {
        var sub = _getItemSubElement(item);

        if ( sub !== null ) {
            if ( _getItemSubType(item) === 'dropdown' ) {
                if ( KTUtil.hasClass(sub, 'show') === true && sub.hasAttribute('data-popper-placement') === true ) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return KTUtil.hasClass(item, 'show');
            }
        } else {
            return false;
        }
    }

    // Test if item dropdown is permanent
    var _isItemDropdownPermanent = function(item) {
        return _getOptionFromElementAttribute(item, 'permanent') === true ? true : false;
    }

    // Test if item's parent is shown
    var _isItemParentShown = function(item) {
        return KTUtil.parents(item, '.menu-item.show').length > 0;
    }

    // Test of it is item sub element
    var _isItemSubElement = function(item) {
        return KTUtil.hasClass(item, 'menu-sub');
    }

    // Test if item has sub
    var _hasItemSub = function(item) {
        return (KTUtil.hasClass(item, 'menu-item') && item.hasAttribute('data-kt-menu-trigger'));
    }

    // Get link element
    var _getItemLinkElement = function(item) {
        return KTUtil.child(item, '.menu-link');
    }

    // Get toggle element
    var _getItemToggleElement = function(item) {
        if ( the.triggerElement ) {
            return the.triggerElement;
        } else {
            return _getItemLinkElement(item);
        }
    }

    // Get item sub element
    var _getItemSubElement = function(item) {
        if ( _isTriggerElement(item) === true ) {
            return the.element;
        } if ( item.classList.contains('menu-sub') === true ) {
            return item;
        } else if ( KTUtil.data(item).has('sub') ) {
            return KTUtil.data(item).get('sub');
        } else {
            return KTUtil.child(item, '.menu-sub');
        }
    }

    // Get item sub type
    var _getItemSubType = function(element) {
        var sub = _getItemSubElement(element);

        if ( sub && parseInt(KTUtil.css(sub, 'z-index')) > 0 ) {
            return "dropdown";
        } else {
            return "accordion";
        }
    }

    // Get item element
    var _getItemElement = function(element) {
        var item, sub;

        // Element is the external trigger element
        if (_isTriggerElement(element) ) {
            return element;
        }   

        // Element has item toggler attribute
        if ( element.hasAttribute('data-kt-menu-trigger') ) {
            return element;
        }

        // Element has item DOM reference in it's data storage
        if ( KTUtil.data(element).has('item') ) {
            return KTUtil.data(element).get('item');
        }

        // Item is parent of element
        if ( (item = element.closest('.menu-item')) ) {
            return item;
        }

        // Element's parent has item DOM reference in it's data storage
        if ( (sub = element.closest('.menu-sub')) ) {
            if ( KTUtil.data(sub).has('item') === true ) {
                return KTUtil.data(sub).get('item')
            } 
        }
    }

    // Get item parent element
    var _getItemParentElement = function(item) {  
        var sub = item.closest('.menu-sub');
        var parentItem;

        if ( sub && KTUtil.data(sub).has('item') ) {
            return KTUtil.data(sub).get('item');
        }

        if ( sub && (parentItem = sub.closest('.menu-item[data-kt-menu-trigger]')) ) {
            return parentItem;
        }

        return null;
    }

    // Get item parent elements
    var _getItemParentElements = function(item) {
        var parents = [];
        var parent;
        var i = 0;

        do {
            parent = _getItemParentElement(item);
            
            if ( parent ) {
                parents.push(parent);
                item = parent;
            }           

            i++;
        } while (parent !== null && i < 20);

        if ( the.triggerElement ) {
            parents.unshift(the.triggerElement);
        }

        return parents;
    }

    // Get item child element
    var _getItemChildElement = function(item) {
        var selector = item;
        var element;

        if ( KTUtil.data(item).get('sub') ) {
            selector = KTUtil.data(item).get('sub');
        }

        if ( selector !== null ) {
            //element = selector.querySelector('.show.menu-item[data-kt-menu-trigger]');
            element = selector.querySelector('.menu-item[data-kt-menu-trigger]');

            if ( element ) {
                return element;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }   
    
    // Get item child elements
    var _getItemChildElements = function(item) {
        var children = [];
        var child;
        var i = 0;

        do {
            child = _getItemChildElement(item);
            
            if ( child ) {
                children.push(child);
                item = child;
            }           

            i++;
        } while (child !== null && i < 20);

        return children;
    }

    // Show item dropdown
    var _showDropdown = function(item) {
        // Handle dropdown show event
        if ( KTEventHandler.trigger(the.element, 'kt.menu.dropdown.show', item) === false )  {
            return;
        }

        // Hide all currently shown dropdowns except current one
        KTMenu.hideDropdowns(item); 

        var toggle = _isTriggerElement(item) ? item : _getItemLinkElement(item);
        var sub = _getItemSubElement(item);

        var width = _getOptionFromElementAttribute(item, 'width');
        var height = _getOptionFromElementAttribute(item, 'height');

        var zindex = the.options.dropdown.zindex; // update
        var parentZindex = KTUtil.getHighestZindex(item); // update

        // Apply a new z-index if dropdown's toggle element or it's parent has greater z-index // update
        if ( parentZindex !== null && parentZindex >= zindex ) {
            zindex = parentZindex + 1;
        }

        if ( zindex > 0 ) {
            KTUtil.css(sub, 'z-index', zindex);
        }

        if ( width !== null ) {
            KTUtil.css(sub, 'width', width);
        }

        if ( height !== null ) {
            KTUtil.css(sub, 'height', height);
        }

        KTUtil.css(sub, 'display', '');
        KTUtil.css(sub, 'overflow', '');

        // Init popper(new)
        _initDropdownPopper(item, sub); 

        KTUtil.addClass(item, 'show');
        KTUtil.addClass(item, 'menu-dropdown');
        KTUtil.addClass(sub, 'show');

        // Append the sub the the root of the menu
        if ( _getOptionFromElementAttribute(item, 'overflow') === true ) {
            document.body.appendChild(sub);
            KTUtil.data(item).set('sub', sub);
            KTUtil.data(sub).set('item', item);
            KTUtil.data(sub).set('menu', the);
        } else {
            KTUtil.data(sub).set('item', item);
        }

        // Handle dropdown shown event
        KTEventHandler.trigger(the.element, 'kt.menu.dropdown.shown', item);
    }

    // Hide item dropdown
    var _hideDropdown = function(item) {
        // Handle dropdown hide event
        if ( KTEventHandler.trigger(the.element, 'kt.menu.dropdown.hide', item) === false )  {
            return;
        }

        var sub = _getItemSubElement(item);

        KTUtil.css(sub, 'z-index', '');
        KTUtil.css(sub, 'width', '');
        KTUtil.css(sub, 'height', '');

        KTUtil.removeClass(item, 'show');
        KTUtil.removeClass(item, 'menu-dropdown');
        KTUtil.removeClass(sub, 'show');

        // Append the sub back to it's parent
        if ( _getOptionFromElementAttribute(item, 'overflow') === true ) {
            if (item.classList.contains('menu-item')) {
                item.appendChild(sub);
            } else {
                KTUtil.insertAfter(the.element, item);
            }
            
            KTUtil.data(item).remove('sub');
            KTUtil.data(sub).remove('item');
            KTUtil.data(sub).remove('menu');
        } 

        // Destroy popper(new)
        _destroyDropdownPopper(item);
        
        // Handle dropdown hidden event 
        KTEventHandler.trigger(the.element, 'kt.menu.dropdown.hidden', item);
    }

    // Init dropdown popper(new)
    var _initDropdownPopper = function(item, sub) {
        // Setup popper instance
        var reference;
        var attach = _getOptionFromElementAttribute(item, 'attach');

        if ( attach ) {
            if ( attach === 'parent') {
                reference = item.parentNode;
            } else {
                reference = document.querySelector(attach);
            }
        } else {
            reference = item;
        }

        var popper = Popper.createPopper(reference, sub, _getDropdownPopperConfig(item)); 
        KTUtil.data(item).set('popper', popper);
    }

    // Destroy dropdown popper(new)
    var _destroyDropdownPopper = function(item) {
        if ( KTUtil.data(item).has('popper') === true ) {
            KTUtil.data(item).get('popper').destroy();
            KTUtil.data(item).remove('popper');
        }
    }

    // Prepare popper config for dropdown(see: https://popper.js.org/docs/v2/)
    var _getDropdownPopperConfig = function(item) {
        // Placement
        var placement = _getOptionFromElementAttribute(item, 'placement');
        if (!placement) {
            placement = 'right';
        }

        // Offset
        var offsetValue = _getOptionFromElementAttribute(item, 'offset');
        var offset = offsetValue ? offsetValue.split(",") : [];
        
        if (offset.length === 2) {
            offset[0] = parseInt(offset[0]);
            offset[1] = parseInt(offset[1]);
        }

        // Strategy
        var strategy = _getOptionFromElementAttribute(item, 'overflow') === true ? 'absolute' : 'fixed';

        var altAxis = _getOptionFromElementAttribute(item, 'flip') !== false ? true : false;

        var popperConfig = {
            placement: placement,
            strategy: strategy,
            modifiers: [{
                name: 'offset',
                options: {
                    offset: offset
                }
            }, {
                name: 'preventOverflow',
                options: {
                    altAxis: altAxis
                }
            }, {
                name: 'flip', 
                options: {
                    flipVariations: false
                }
            }]
        };

        return popperConfig;
    }

    // Show item accordion
    var _showAccordion = function(item) {
        if ( KTEventHandler.trigger(the.element, 'kt.menu.accordion.show', item) === false )  {
            return;
        }

        var sub = _getItemSubElement(item);
        var expand = the.options.accordion.expand;
        
        if (_getOptionFromElementAttribute(item, 'expand') === true) {
            expand = true;
        } else if (_getOptionFromElementAttribute(item, 'expand') === false) {
            expand = false;
        } else if (_getOptionFromElementAttribute(the.element, 'expand') === true) {
            expand = true;
        }

        if ( expand === false ) {
            _hideAccordions(item);
        }

        if ( KTUtil.data(item).has('popper') === true ) {
            _hideDropdown(item);
        }

        KTUtil.addClass(item, 'hover');

        KTUtil.addClass(item, 'showing');

        KTUtil.slideDown(sub, the.options.accordion.slideSpeed, function() {
            KTUtil.removeClass(item, 'showing');
            KTUtil.addClass(item, 'show');
            KTUtil.addClass(sub, 'show');

            KTEventHandler.trigger(the.element, 'kt.menu.accordion.shown', item);
        });        
    }

    // Hide item accordion
    var _hideAccordion = function(item) {
        if ( KTEventHandler.trigger(the.element, 'kt.menu.accordion.hide', item) === false )  {
            return;
        }
        
        var sub = _getItemSubElement(item);

        KTUtil.addClass(item, 'hiding');

        KTUtil.slideUp(sub, the.options.accordion.slideSpeed, function() {
            KTUtil.removeClass(item, 'hiding');
            KTUtil.removeClass(item, 'show');
            KTUtil.removeClass(sub, 'show');

            KTUtil.removeClass(item, 'hover'); // update

            KTEventHandler.trigger(the.element, 'kt.menu.accordion.hidden', item);
        });
    }

    var _setActiveLink = function(link) {
        var item = _getItemElement(link);

        if (!item) {
            return;
        }

        var parentItems = _getItemParentElements(item);
        var parentTabPane = link.closest('.tab-pane');

        var activeLinks = [].slice.call(the.element.querySelectorAll('.menu-link.active'));
        var activeParentItems = [].slice.call(the.element.querySelectorAll('.menu-item.here, .menu-item.show'));
        
        if (_getItemSubType(item) === "accordion") {
            _showAccordion(item);
        } else {
            item.classList.add("here");
        }

        if ( parentItems && parentItems.length > 0 ) {
            for (var i = 0, len = parentItems.length; i < len; i++) {
                var parentItem = parentItems[i];

                if (_getItemSubType(parentItem) === "accordion") {
                    _showAccordion(parentItem);
                } else {
                    parentItem.classList.add("here");
                }
            }
        }       
        
        activeLinks.map(function (activeLink) {
            activeLink.classList.remove("active");
        });

        activeParentItems.map(function (activeParentItem) {
            if (activeParentItem.contains(item) === false) {
                activeParentItem.classList.remove("here");
                activeParentItem.classList.remove("show");
            }
        });

        // Handle tab
        if (parentTabPane && bootstrap.Tab) {
            var tabEl = the.element.querySelector('[data-bs-target="#' + parentTabPane.getAttribute("id") + '"]');
            var tab = new bootstrap.Tab(tabEl);

            if (tab) {
                tab.show();
            }
        }

        link.classList.add("active");
    }

    var _getLinkByAttribute = function(value, name = "href") {
        var link = the.element.querySelector('.menu-link[' + name + '="' + value + '"]');

        if (link) {
            return link;
        } else {
            null;
        }
    }

    // Hide all shown accordions of item
    var _hideAccordions = function(item) {
        var itemsToHide = KTUtil.findAll(the.element, '.show[data-kt-menu-trigger]');
        var itemToHide;

        if (itemsToHide && itemsToHide.length > 0) {
            for (var i = 0, len = itemsToHide.length; i < len; i++) {
                itemToHide = itemsToHide[i];

                if ( _getItemSubType(itemToHide) === 'accordion' && itemToHide !== item && item.contains(itemToHide) === false && itemToHide.contains(item) === false ) {
                    _hideAccordion(itemToHide);
                }
            }
        }
    }

    // Get item option(through html attributes)
    var _getOptionFromElementAttribute = function(item, name) {
        var attr;
        var value = null;

        if ( item && item.hasAttribute('data-kt-menu-' + name) ) {
            attr = item.getAttribute('data-kt-menu-' + name);
            value = KTUtil.getResponsiveValue(attr);

            if ( value !== null && String(value) === 'true' ) {
                value = true;
            } else if ( value !== null && String(value) === 'false' ) {
                value = false;
            }
        }

        return value;
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('menu');
    }

    // Construct Class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Event Handlers
    the.click = function(element, e) {
        return _click(element, e);
    }

    the.link = function(element, e) {
        return _link(element, e);
    }

    the.dismiss = function(element, e) {
        return _dismiss(element, e);
    }

    the.mouseover = function(element, e) {
        return _mouseover(element, e);
    }

    the.mouseout = function(element, e) {
        return _mouseout(element, e);
    }

    // General Methods
    the.getItemTriggerType = function(item) {
        return _getOptionFromElementAttribute(item, 'trigger');
    }

    the.getItemSubType = function(element) {
       return _getItemSubType(element);
    }

    the.show = function(item) {
        return _show(item);
    }

    the.hide = function(item) {
        return _hide(item);
    }

    the.toggle = function(item) {
        return _toggle(item);
    }

    the.reset = function(item) {
        return _reset(item);
    }

    the.update = function() {
        return _update();
    }

    the.getElement = function() {
        return the.element;
    }

    the.setActiveLink = function(link) {
        return _setActiveLink(link);
    }   

    the.getLinkByAttribute = function(value, name = "href") {
        return _getLinkByAttribute(value, name);
    }

    the.getItemLinkElement = function(item) {
        return _getItemLinkElement(item);
    }

    the.getItemToggleElement = function(item) {
        return _getItemToggleElement(item);
    }

    the.getItemSubElement = function(item) {
        return _getItemSubElement(item);
    }

    the.getItemParentElements = function(item) {
        return _getItemParentElements(item);
    }

    the.isItemSubShown = function(item) {
        return _isItemSubShown(item);
    }

    the.isItemParentShown = function(item) {
        return _isItemParentShown(item);
    }

    the.getTriggerElement = function() {
        return the.triggerElement;
    }

    the.isItemDropdownPermanent = function(item) {
        return _isItemDropdownPermanent(item);
    }

    the.destroy = function() {
        return _destroy();
    }

    the.disable = function() {
        the.disabled = true;
    }

    the.enable = function() {
        the.disabled = false;
    }

    // Accordion Mode Methods
    the.hideAccordions = function(item) {
        return _hideAccordions(item);
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }
};

// Get KTMenu instance by element
KTMenu.getInstance = function(element) {
    var menu;
    var item;

    if (!element) {
        return null;
    }

    // Element has menu DOM reference in it's DATA storage
    if ( KTUtil.data(element).has('menu') ) {
        return KTUtil.data(element).get('menu');
    }

    // Element has .menu parent 
    if ( menu = element.closest('.menu') ) {
        if ( KTUtil.data(menu).has('menu') ) {
            return KTUtil.data(menu).get('menu');
        }
    }
    
    // Element has a parent with DOM reference to .menu in it's DATA storage
    if ( KTUtil.hasClass(element, 'menu-link') ) {
        var sub = element.closest('.menu-sub');

        if ( KTUtil.data(sub).has('menu') ) {
            return KTUtil.data(sub).get('menu');
        }
    } 

    return null;
}

// Hide all dropdowns and skip one if provided
KTMenu.hideDropdowns = function(skip) {
    var items = document.querySelectorAll('.show.menu-dropdown[data-kt-menu-trigger]');

    if (items && items.length > 0) {
        for (var i = 0, len = items.length; i < len; i++) {
            var item = items[i];
            var menu = KTMenu.getInstance(item);

            if ( menu && menu.getItemSubType(item) === 'dropdown' ) {
                if ( skip ) {
                    if ( menu.getItemSubElement(item).contains(skip) === false && item.contains(skip) === false &&  item !== skip ) {
                        menu.hide(item);
                    }
                } else {
                    menu.hide(item);
                }
            }
        }
    }
}

// Update all dropdowns popover instances
KTMenu.updateDropdowns = function() {
    var items = document.querySelectorAll('.show.menu-dropdown[data-kt-menu-trigger]');

    if (items && items.length > 0) {
        for (var i = 0, len = items.length; i < len; i++) {
            var item = items[i];

            if ( KTUtil.data(item).has('popper') ) {
                KTUtil.data(item).get('popper').forceUpdate();
            }
        }
    }
}

// Global handlers
KTMenu.initHandlers = function() {
    // Dropdown handler
    document.addEventListener("click", function(e) {
        var items = document.querySelectorAll('.show.menu-dropdown[data-kt-menu-trigger]:not([data-kt-menu-static="true"])');
        var menu;
        var item;
        var sub;
        var menuObj;

        if ( items && items.length > 0 ) {
            for ( var i = 0, len = items.length; i < len; i++ ) {
                item = items[i];
                menuObj = KTMenu.getInstance(item);

                if (menuObj && menuObj.getItemSubType(item) === 'dropdown') {
                    menu = menuObj.getElement();
                    sub = menuObj.getItemSubElement(item);

                    if ( item === e.target || item.contains(e.target) ) {
                        continue;
                    }
                    
                    if ( sub === e.target || sub.contains(e.target) ) {
                        continue;
                    }
                        
                    menuObj.hide(item);
                }
            }
        }
    });

    // Sub toggle handler(updated)
    KTUtil.on(document.body,  '.menu-item[data-kt-menu-trigger] > .menu-link, [data-kt-menu-trigger]:not(.menu-item):not([data-kt-menu-trigger="auto"])', 'click', function(e) {
        var menu = KTMenu.getInstance(this);

        if ( menu !== null ) {
            return menu.click(this, e);
        }
    });

    // Link handler
    KTUtil.on(document.body,  '.menu-item:not([data-kt-menu-trigger]) > .menu-link', 'click', function(e) {
        var menu = KTMenu.getInstance(this);

        if ( menu !== null ) {
            return menu.link(this, e);
        }
    });

    // Dismiss handler
    KTUtil.on(document.body,  '[data-kt-menu-dismiss="true"]', 'click', function(e) {
        var menu = KTMenu.getInstance(this);

        if ( menu !== null ) {
            return menu.dismiss(this, e);
        }
    });

    // Mouseover handler
    KTUtil.on(document.body,  '[data-kt-menu-trigger], .menu-sub', 'mouseover', function(e) {
        var menu = KTMenu.getInstance(this);

        if ( menu !== null && menu.getItemSubType(this) === 'dropdown' ) {
            return menu.mouseover(this, e);
        }
    });

    // Mouseout handler
    KTUtil.on(document.body,  '[data-kt-menu-trigger], .menu-sub', 'mouseout', function(e) {
        var menu = KTMenu.getInstance(this);

        if ( menu !== null && menu.getItemSubType(this) === 'dropdown' ) {
            return menu.mouseout(this, e);
        }
    });

    // Resize handler
    window.addEventListener('resize', function() {
        var menu;
        var timer;

        KTUtil.throttle(timer, function() {
            // Locate and update Offcanvas instances on window resize
            var elements = document.querySelectorAll('[data-kt-menu="true"]');

            if ( elements && elements.length > 0 ) {
                for (var i = 0, len = elements.length; i < len; i++) {
                    menu = KTMenu.getInstance(elements[i]);
                    if (menu) {
                        menu.update();
                    }
                }
            }
        }, 200);
    });
}

// Render menus by url
KTMenu.updateByLinkAttribute = function(value, name = "href") {
    // Set menu link active state by attribute value
    var elements = document.querySelectorAll('[data-kt-menu="true"]');

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            var menu = KTMenu.getInstance(elements[i]);

            if (menu) {
                var link = menu.getLinkByAttribute(value, name);                
                if (link) {
                    menu.setActiveLink(link);
                }
            }
        }
    }
}

// Global instances
KTMenu.createInstances = function(selector = '[data-kt-menu="true"]') {
    // Initialize menus
    var elements = document.querySelectorAll(selector);
    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            new KTMenu(elements[i]);
        }
    }
}

// Global initialization
KTMenu.init = function() {
    KTMenu.createInstances();

    if (KTMenuHandlersInitialized === false) {
        KTMenu.initHandlers();

        KTMenuHandlersInitialized = true;
    }    
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTMenu;
}

"use strict";

// Class definition
var KTPasswordMeter = function(element, options) {
    ////////////////////////////
    // ** Private variables  ** //
    ////////////////////////////
    var the = this;

    if (!element) {
        return;
    }

    // Default Options
    var defaultOptions = {
        minLength: 8,
        checkUppercase: true,        
        checkLowercase: true,
        checkDigit: true,
        checkChar: true,
        scoreHighlightClass: 'active'
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    // Constructor
    var _construct = function() {
        if ( KTUtil.data(element).has('password-meter') === true ) {
            the = KTUtil.data(element).get('password-meter');
        } else {
            _init();
        }
    }

    // Initialize
    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.score = 0;
        the.checkSteps = 5;

        // Elements
        the.element = element;
        the.inputElement = the.element.querySelector('input[type]');
        the.visibilityElement = the.element.querySelector('[data-kt-password-meter-control="visibility"]');
        the.highlightElement = the.element.querySelector('[data-kt-password-meter-control="highlight"]'); 

        // Set initialized
        the.element.setAttribute('data-kt-password-meter', 'true');
        
        // Event Handlers
        _handlers();

        // Bind Instance
        KTUtil.data(the.element).set('password-meter', the);
    }

    // Handlers
    var _handlers = function() {
        if (the.highlightElement) {
            the.inputElement.addEventListener('input', function() {
                _check();
            });
        }

        if (the.visibilityElement) {
            the.visibilityElement.addEventListener('click', function() {
                _visibility();
            });
        }
    }   

    // Event handlers
    var _check = function() {
        var score = 0;
        var checkScore = _getCheckScore();
        
        if (_checkLength() === true) {
            score = score + checkScore;
        }

        if (the.options.checkUppercase === true && _checkLowercase() === true) {
            score = score + checkScore;
        }

        if (the.options.checkLowercase === true && _checkUppercase() === true ) {
            score = score + checkScore;
        }

        if (the.options.checkDigit === true && _checkDigit() === true ) {
            score = score + checkScore;
        }

        if (the.options.checkChar === true && _checkChar() === true ) {
            score = score + checkScore;
        }

        the.score = score;

        _highlight();
    }

    var _checkLength = function() {
        return the.inputElement.value.length >= the.options.minLength;  // 20 score
    }

    var _checkLowercase = function() {
        return /[a-z]/.test(the.inputElement.value);  // 20 score
    }

    var _checkUppercase = function() {
        return /[A-Z]/.test(the.inputElement.value);  // 20 score
    }

    var _checkDigit = function() {
        return /[0-9]/.test(the.inputElement.value);  // 20 score
    }

    var _checkChar = function() {
        return /[~`!#@$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(the.inputElement.value);  // 20 score
    }    

    var _getCheckScore = function() {
        var count = 1;
        
        if (the.options.checkUppercase === true) {
            count++;
        }

        if (the.options.checkLowercase === true) {
            count++;
        }

        if (the.options.checkDigit === true) {
            count++;
        }

        if (the.options.checkChar === true) {
            count++;
        }

        the.checkSteps = count;

        return 100 / the.checkSteps;
    }
    
    var _highlight = function() {
        var items = [].slice.call(the.highlightElement.querySelectorAll('div'));
        var total = items.length;
        var index = 0;
        var checkScore = _getCheckScore();
        var score = _getScore();

        items.map(function (item) {
            index++;

            if ( (checkScore * index * (the.checkSteps / total)) <= score ) {
                item.classList.add('active');
            } else {
                item.classList.remove('active');
            }            
        });
    }

    var _visibility = function() {
        var visibleIcon = the.visibilityElement.querySelector(':scope > i:not(.d-none)');
        var hiddenIcon = the.visibilityElement.querySelector(':scope > i.d-none');
        
        if (the.inputElement.getAttribute('type').toLowerCase() === 'password' ) {
            the.inputElement.setAttribute('type', 'text');
        }  else {
            the.inputElement.setAttribute('type', 'password');
        }        

        visibleIcon.classList.add('d-none');
        hiddenIcon.classList.remove('d-none');

        the.inputElement.focus();
    }

    var _reset = function() {
        the.score = 0;

        _highlight();
    }

    // Gets current password score
    var _getScore = function() {
       return the.score;
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('password-meter');
    }

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.check = function() {
        return _check();
    }

    the.getScore = function() {
        return _getScore();
    }

    the.reset = function() {
        return _reset();
    }

    the.destroy = function() {
        return _destroy();
    }
};

// Static methods
KTPasswordMeter.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('password-meter') ) {
        return KTUtil.data(element).get('password-meter');
    } else {
        return null;
    }
}

// Create instances
KTPasswordMeter.createInstances = function(selector = '[data-kt-password-meter]') {
    // Get instances
    var elements = document.body.querySelectorAll(selector);

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            // Initialize instances
            new KTPasswordMeter(elements[i]);
        }
    }
}

// Global initialization
KTPasswordMeter.init = function() {
    KTPasswordMeter.createInstances();
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTPasswordMeter;
}
"use strict";

var KTScrollHandlersInitialized = false;

// Class definition
var KTScroll = function(element, options) {
    ////////////////////////////
    // ** Private Variables  ** //
    ////////////////////////////
    var the = this;

    if (!element) {
        return;
    }

    // Default options
    var defaultOptions = {
        saveState: true
    };

    ////////////////////////////
    // ** Private Methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('scroll') ) {
            the = KTUtil.data(element).get('scroll');
        } else {
            _init();
        }
    }

    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);

        // Elements
        the.element = element;        
        the.id = the.element.getAttribute('id');

        // Set initialized
        the.element.setAttribute('data-kt-scroll', 'true');

        // Update
        _update();

        // Bind Instance
        KTUtil.data(the.element).set('scroll', the);
    }

    var _setupHeight = function() {
        var heightType = _getHeightType();
        var height = _getHeight();

        // Set height
        if ( height !== null && height.length > 0 ) {
            KTUtil.css(the.element, heightType, height);
        } else {
            KTUtil.css(the.element, heightType, '');
        }
    }

    var _setupState = function () {
        var namespace = _getStorageNamespace();

        if ( _getOption('save-state') === true && the.id ) {
            if ( localStorage.getItem(namespace + the.id + 'st') ) {
                var pos = parseInt(localStorage.getItem(namespace + the.id + 'st'));

                if ( pos > 0 ) {
                    the.element.scroll({
                        top: pos,
                        behavior: 'instant'
                    });
                }
            }
        }
    }

    var _getStorageNamespace = function(postfix) {
        return document.body.hasAttribute("data-kt-name") ? document.body.getAttribute("data-kt-name") + "_" : "";
    }

    var _setupScrollHandler = function() {
        if ( _getOption('save-state') === true && the.id ) {
            the.element.addEventListener('scroll', _scrollHandler);
        } else {
            the.element.removeEventListener('scroll', _scrollHandler);
        }
    }

    var _destroyScrollHandler = function() {
        the.element.removeEventListener('scroll', _scrollHandler);
    }

    var _resetHeight = function() {
        KTUtil.css(the.element, _getHeightType(), '');
    }

    var _scrollHandler = function () {
        var namespace = _getStorageNamespace();
        localStorage.setItem(namespace + the.id + 'st', the.element.scrollTop);
    }

    var _update = function() {
        // Activate/deactivate
        if ( _getOption('activate') === true || the.element.hasAttribute('data-kt-scroll-activate') === false ) {
            _setupHeight();
            _setupStretchHeight();
            _setupScrollHandler();
            _setupState();
        } else {
            _resetHeight()
            _destroyScrollHandler();
        }        
    }

    var _setupStretchHeight = function() {
        var stretch = _getOption('stretch');

        // Stretch
        if ( stretch !== null ) {
            var elements = document.querySelectorAll(stretch);

            if ( elements && elements.length == 2 ) {
                var element1 = elements[0];
                var element2 = elements[1];
                var diff = _getElementHeight(element2) - _getElementHeight(element1);

                if (diff > 0) {
                    var height = parseInt(KTUtil.css(the.element, _getHeightType())) + diff;

                    KTUtil.css(the.element, _getHeightType(), String(height) + 'px');
                }
            }
        }
    }

    var _getHeight = function() {
        var height = _getOption(_getHeightType());

        if ( height instanceof Function ) {
            return height.call();
        } else if ( height !== null && typeof height === 'string' && height.toLowerCase() === 'auto' ) {
            return _getAutoHeight();
        } else {
            return height;
        }
    }

    var _getAutoHeight = function() {
        var height = KTUtil.getViewPort().height;
        var dependencies = _getOption('dependencies');
        var wrappers = _getOption('wrappers');
        var offset = _getOption('offset');

        // Spacings
        height = height - _getElementSpacing(the.element); 

        // Height dependencies
        //console.log('Q:' + JSON.stringify(dependencies));

        if ( dependencies !== null ) {
            var elements = document.querySelectorAll(dependencies);

            if ( elements && elements.length > 0 ) {
                for ( var i = 0, len = elements.length; i < len; i++ ) {
                    if ( KTUtil.visible(elements[i]) === false ) {
                        continue;
                    }

                    height = height - _getElementHeight(elements[i]);
                }
            }
        }

        // Wrappers
        if ( wrappers !== null ) {
            var elements = document.querySelectorAll(wrappers);
            if ( elements && elements.length > 0 ) {
                for ( var i = 0, len = elements.length; i < len; i++ ) {
                    if ( KTUtil.visible(elements[i]) === false ) {
                        continue;
                    }

                    height = height - _getElementSpacing(elements[i]);
                }
            }
        }

        // Custom offset
        if ( offset !== null && typeof offset !== 'object') {
            height = height - parseInt(offset);
        }

        return String(height) + 'px';
    }

    var _getElementHeight = function(element) {
        var height = 0;

        if (element !== null) {
            height = height + parseInt(KTUtil.css(element, 'height'));
            height = height + parseInt(KTUtil.css(element, 'margin-top'));
            height = height + parseInt(KTUtil.css(element, 'margin-bottom'));

            if (KTUtil.css(element, 'border-top')) {
                height = height + parseInt(KTUtil.css(element, 'border-top'));
            }

            if (KTUtil.css(element, 'border-bottom')) {
                height = height + parseInt(KTUtil.css(element, 'border-bottom'));
            }
        } 

        return height;
    }

    var _getElementSpacing = function(element) {
        var spacing = 0;

        if (element !== null) {
            spacing = spacing + parseInt(KTUtil.css(element, 'margin-top'));
            spacing = spacing + parseInt(KTUtil.css(element, 'margin-bottom'));
            spacing = spacing + parseInt(KTUtil.css(element, 'padding-top'));
            spacing = spacing + parseInt(KTUtil.css(element, 'padding-bottom'));

            if (KTUtil.css(element, 'border-top')) {
                spacing = spacing + parseInt(KTUtil.css(element, 'border-top'));
            }

            if (KTUtil.css(element, 'border-bottom')) {
                spacing = spacing + parseInt(KTUtil.css(element, 'border-bottom'));
            }
        } 

        return spacing;
    }

    var _getOption = function(name) {
        if ( the.element.hasAttribute('data-kt-scroll-' + name) === true ) {
            var attr = the.element.getAttribute('data-kt-scroll-' + name);

            var value = KTUtil.getResponsiveValue(attr);

            if ( value !== null && String(value) === 'true' ) {
                value = true;
            } else if ( value !== null && String(value) === 'false' ) {
                value = false;
            }

            return value;
        } else {
            var optionName = KTUtil.snakeToCamel(name);

            if ( the.options[optionName] ) {
                return KTUtil.getResponsiveValue(the.options[optionName]);
            } else {
                return null;
            }
        }
    }

    var _getHeightType = function() {
        if (_getOption('height')) {
            return 'height';
        } if (_getOption('min-height')) {
            return 'min-height';
        } if (_getOption('max-height')) {
            return 'max-height';
        }
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('scroll');
    }

    // Construct Class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    the.update = function() {
        return _update();
    }

    the.getHeight = function() {
        return _getHeight();
    }

    the.getElement = function() {
        return the.element;
    }

    the.destroy = function() {
        return _destroy();
    }
};

// Static methods
KTScroll.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('scroll') ) {
        return KTUtil.data(element).get('scroll');
    } else {
        return null;
    }
}

// Create instances
KTScroll.createInstances = function(selector = '[data-kt-scroll="true"]') {
    // Initialize Menus
    var elements = document.body.querySelectorAll(selector);

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            new KTScroll(elements[i]);
        }
    }
}

// Window resize handling
KTScroll.handleResize = function() {
    window.addEventListener('resize', function() {
        var timer;
    
        KTUtil.throttle(timer, function() {
            // Locate and update Offcanvas instances on window resize
            var elements = document.body.querySelectorAll('[data-kt-scroll="true"]');
    
            if ( elements && elements.length > 0 ) {
                for (var i = 0, len = elements.length; i < len; i++) {
                    var scroll = KTScroll.getInstance(elements[i]);
                    if (scroll) {
                        scroll.update();
                    }
                }
            }
        }, 200);
    });
}

// Global initialization
KTScroll.init = function() {
    KTScroll.createInstances();

    if (KTScrollHandlersInitialized === false) {
        KTScroll.handleResize();

        KTScrollHandlersInitialized = true;
    }    
};

// Webpack Support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTScroll;
}

"use strict";

// Class definition
var KTScrolltop = function(element, options) {
    ////////////////////////////
    // ** Private variables  ** //
    ////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default options
    var defaultOptions = {
        offset: 300,
        speed: 600
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    var _construct = function() {
        if (KTUtil.data(element).has('scrolltop')) {
            the = KTUtil.data(element).get('scrolltop');
        } else {
            _init();
        }
    }

    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('scrolltop');
        the.element = element;

        // Set initialized
        the.element.setAttribute('data-kt-scrolltop', 'true');

        // Event Handlers
        _handlers();

        // Bind Instance
        KTUtil.data(the.element).set('scrolltop', the);
    }

    var _handlers = function() {
        var timer;

        window.addEventListener('scroll', function() {
            KTUtil.throttle(timer, function() {
                _scroll();
            }, 200);
        });

        KTUtil.addEvent(the.element, 'click', function(e) {
            e.preventDefault();

            _go();
        });
    }

    var _scroll = function() {
        var offset = parseInt(_getOption('offset'));

        var pos = KTUtil.getScrollTop(); // current vertical position

        if ( pos > offset ) {
            if ( document.body.hasAttribute('data-kt-scrolltop') === false ) {
                document.body.setAttribute('data-kt-scrolltop', 'on');
            }
        } else {
            if ( document.body.hasAttribute('data-kt-scrolltop') === true ) {
                document.body.removeAttribute('data-kt-scrolltop');
            }
        }
    }

    var _go = function() {
        var speed = parseInt(_getOption('speed'));

        window.scrollTo({top: 0, behavior: 'smooth'});
        //KTUtil.scrollTop(0, speed);
    }

    var _getOption = function(name) {
        if ( the.element.hasAttribute('data-kt-scrolltop-' + name) === true ) {
            var attr = the.element.getAttribute('data-kt-scrolltop-' + name);
            var value = KTUtil.getResponsiveValue(attr);

            if ( value !== null && String(value) === 'true' ) {
                value = true;
            } else if ( value !== null && String(value) === 'false' ) {
                value = false;
            }

            return value;
        } else {
            var optionName = KTUtil.snakeToCamel(name);

            if ( the.options[optionName] ) {
                return KTUtil.getResponsiveValue(the.options[optionName]);
            } else {
                return null;
            }
        }
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('scrolltop');
    }

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.go = function() {
        return _go();
    }

    the.getElement = function() {
        return the.element;
    }

    the.destroy = function() {
        return _destroy();
    }
};

// Static methods
KTScrolltop.getInstance = function(element) {
    if (element && KTUtil.data(element).has('scrolltop')) {
        return KTUtil.data(element).get('scrolltop');
    } else {
        return null;
    }
}

// Create instances
KTScrolltop.createInstances = function(selector = '[data-kt-scrolltop="true"]') {
    // Initialize Menus
    var elements = document.body.querySelectorAll(selector);

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            new KTScrolltop(elements[i]);
        }
    }
}

// Global initialization
KTScrolltop.init = function() {
    KTScrolltop.createInstances();
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTScrolltop;
}

"use strict";

// Class definition
var KTSearch = function(element, options) {
    ////////////////////////////
    // ** Private variables  ** //
    ////////////////////////////
    var the = this;

    if (!element) {
        return;
    }

    // Default Options
    var defaultOptions = {
        minLength: 2,  // Miniam text lenght to query search
        keypress: true,  // Enable search on keypress 
        enter: true,  // Enable search on enter key press
        layout: 'menu',  // Use 'menu' or 'inline' layout options to display search results
        responsive: null, // Pass integer value or bootstrap compatible breakpoint key(sm,md,lg,xl,xxl) to enable reponsive form mode for device width below the breakpoint value
        showOnFocus: true // Always show menu on input focus
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    // Construct
    var _construct = function() {
        if ( KTUtil.data(element).has('search') === true ) {
            the = KTUtil.data(element).get('search');
        } else {
            _init();
        }
    }

    // Init
    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.processing = false;

        // Elements
        the.element = element;               
        the.contentElement = _getElement('content');     
        the.formElement = _getElement('form');         
        the.inputElement = _getElement('input');
        the.spinnerElement = _getElement('spinner');
        the.clearElement = _getElement('clear');
        the.toggleElement = _getElement('toggle');   
        the.submitElement = _getElement('submit');
        the.toolbarElement = _getElement('toolbar');   

        the.resultsElement = _getElement('results');
        the.suggestionElement = _getElement('suggestion'); 
        the.emptyElement = _getElement('empty'); 

        // Set initialized
        the.element.setAttribute('data-kt-search', 'true');
        
        // Layout
        the.layout = _getOption('layout');
        
        // Menu
        if ( the.layout === 'menu' ) {
            the.menuObject = new KTMenu(the.contentElement);
        } else {
            the.menuObject = null;
        }

        // Update
        _update();

        // Event Handlers
        _handlers();

        // Bind Instance
        KTUtil.data(the.element).set('search', the);
    }

    // Handlera
    var _handlers = function() {
        // Focus
        the.inputElement.addEventListener('focus', _focus);

        // Blur
        the.inputElement.addEventListener('blur', _blur);

        // Keypress
        if ( _getOption('keypress') === true ) {
            the.inputElement.addEventListener('input', _input);
        }

        // Submit
        if ( the.submitElement ) {
            the.submitElement.addEventListener('click', _search);
        }

        // Enter
        if ( _getOption('enter') === true ) {
            the.inputElement.addEventListener('keypress', _enter);
        }

        // Clear 
        if ( the.clearElement ) {
            the.clearElement.addEventListener('click', _clear);
        }

        // Menu
        if ( the.menuObject ) {
            // Toggle menu
            if ( the.toggleElement ) {
                the.toggleElement.addEventListener('click', _show);

                the.menuObject.on('kt.menu.dropdown.show', function(item) {
                    if (KTUtil.visible(the.toggleElement)) {
                        the.toggleElement.classList.add('active');
                        the.toggleElement.classList.add('show');
                    } 
                });
    
                the.menuObject.on('kt.menu.dropdown.hide', function(item) {
                    if (KTUtil.visible(the.toggleElement)) {
                        the.toggleElement.classList.remove('active');
                        the.toggleElement.classList.remove('show');
                    }
                });
            }            

            the.menuObject.on('kt.menu.dropdown.shown', function() {
                the.inputElement.focus();
            });
        } 

        // Window resize handling
        window.addEventListener('resize', function() {
            var timer;

            KTUtil.throttle(timer, function() {
                _update();
            }, 200);
        });
    }

    // Focus
    var _focus = function() {
        the.element.classList.add('focus');

        if ( _getOption('show-on-focus') === true || the.inputElement.value.length >= minLength ) {
            _show();
        }        
    }

    // Blur
    var _blur = function() {        
        the.element.classList.remove('focus');
    }

    // Enter 
    var _enter = function(e) {
        var key = e.charCode || e.keyCode || 0;

        if (key == 13) {
            e.preventDefault();

            _search();
        }
    }

    // Input
    var _input = function() {
        if ( _getOption('min-length') )  {
            var minLength = parseInt(_getOption('min-length'));

            if ( the.inputElement.value.length >= minLength ) {
                _search();
            } else if ( the.inputElement.value.length === 0 ) {
                _clear();
            }
        }
    }

    // Search
    var _search = function() {
        if (the.processing === false) {
            // Show search spinner
            if (the.spinnerElement) {
                the.spinnerElement.classList.remove("d-none");
            }
            
            // Hide search clear button
            if (the.clearElement) {
                the.clearElement.classList.add("d-none");
            }

            // Hide search toolbar
            if (the.toolbarElement && the.formElement.contains(the.toolbarElement)) {
                the.toolbarElement.classList.add("d-none");
            }

            // Focus input
            the.inputElement.focus();

            the.processing = true;
            KTEventHandler.trigger(the.element, 'kt.search.process', the);
        }
    }

    // Complete
    var _complete = function() {
        if (the.spinnerElement) {
            the.spinnerElement.classList.add("d-none");
        }

        // Show search toolbar
        if (the.clearElement) {
            the.clearElement.classList.remove("d-none");
        }

        if ( the.inputElement.value.length === 0 ) {
            _clear();
        }

        // Focus input
        the.inputElement.focus();

        _show();

        the.processing = false;
    }

    // Clear
    var _clear = function() {
        if ( KTEventHandler.trigger(the.element, 'kt.search.clear', the) === false )  {
            return;
        }

        // Clear and focus input
        the.inputElement.value = "";
        the.inputElement.focus();

        // Hide clear icon
        if (the.clearElement) {
            the.clearElement.classList.add("d-none");
        }

        // Show search toolbar
        if (the.toolbarElement && the.formElement.contains(the.toolbarElement)) {
            the.toolbarElement.classList.remove("d-none");
        }

        // Hide menu
        if ( _getOption('show-on-focus') === false ) {
            _hide();
        }

        KTEventHandler.trigger(the.element, 'kt.search.cleared', the);
    }

    // Update
    var _update = function() {
        // Handle responsive form
        if (the.layout === 'menu') {
            var responsiveFormMode = _getResponsiveFormMode();

            if ( responsiveFormMode === 'on' && the.contentElement.contains(the.formElement) === false ) {
                the.contentElement.prepend(the.formElement);
                the.formElement.classList.remove('d-none');                
            } else if ( responsiveFormMode === 'off' && the.contentElement.contains(the.formElement) === true ) {
                the.element.prepend(the.formElement);
                the.formElement.classList.add('d-none');
            }
        }
    }

    // Show menu
    var _show = function() {
        if ( the.menuObject ) {
            _update();

            the.menuObject.show(the.element);
        }
    }

    // Hide menu
    var _hide = function() {
        if ( the.menuObject ) {
            _update();

            the.menuObject.hide(the.element);
        }
    }

    // Get option
    var _getOption = function(name) {
        if ( the.element.hasAttribute('data-kt-search-' + name) === true ) {
            var attr = the.element.getAttribute('data-kt-search-' + name);
            var value = KTUtil.getResponsiveValue(attr);

            if ( value !== null && String(value) === 'true' ) {
                value = true;
            } else if ( value !== null && String(value) === 'false' ) {
                value = false;
            }

            return value;
        } else {
            var optionName = KTUtil.snakeToCamel(name);

            if ( the.options[optionName] ) {
                return KTUtil.getResponsiveValue(the.options[optionName]);
            } else {
                return null;
            }
        }
    }

    // Get element
    var _getElement = function(name) {
        return the.element.querySelector('[data-kt-search-element="' + name + '"]');
    }

    // Check if responsive form mode is enabled
    var _getResponsiveFormMode = function() {
        var responsive = _getOption('responsive');
        var width = KTUtil.getViewPort().width;

        if (!responsive) {
            return null;
        }

        var breakpoint = KTUtil.getBreakpoint(responsive);

        if (!breakpoint ) {
            breakpoint = parseInt(responsive);
        }

        if (width < breakpoint) {
            return "on";
        } else {
            return "off";
        }
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('search');
    }    

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.show = function() {
        return _show();
    }

    the.hide = function() {
        return _hide();
    }

    the.update = function() {
        return _update();
    }

    the.search = function() {
        return _search();
    }

    the.complete = function() {
        return _complete();
    }

    the.clear = function() {
        return _clear();
    }

    the.isProcessing = function() {
        return the.processing;
    }

    the.getQuery = function() {
        return the.inputElement.value;
    }    

    the.getMenu = function() {
        return the.menuObject;
    }

    the.getFormElement = function() {
        return the.formElement;
    }

    the.getInputElement = function() {
        return the.inputElement;
    }

    the.getContentElement = function() {
        return the.contentElement;
    }

    the.getElement = function() {
        return the.element;
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }
};

// Static methods
KTSearch.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('search') ) {
        return KTUtil.data(element).get('search');
    } else {
        return null;
    }
}

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTSearch;
}

"use strict";

// Class definition
var KTStepper = function(element, options) {
    //////////////////////////////
    // ** Private variables  ** //
    //////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default Options
    var defaultOptions = {
        startIndex: 1,
        animation: false,
        animationSpeed: '0.3s',
        animationNextClass: 'animate__animated animate__slideInRight animate__fast',
        animationPreviousClass: 'animate__animated animate__slideInLeft animate__fast'
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('stepper') === true ) {
            the = KTUtil.data(element).get('stepper');
        } else {
            _init();
        }
    }

    var _init = function() {
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('stepper');

        the.element = element;

        // Set initialized
        the.element.setAttribute('data-kt-stepper', 'true');

        // Elements
        the.steps = KTUtil.findAll(the.element, '[data-kt-stepper-element="nav"]');
        the.btnNext = KTUtil.find(the.element, '[data-kt-stepper-action="next"]');
        the.btnPrevious = KTUtil.find(the.element, '[data-kt-stepper-action="previous"]');
        the.btnSubmit = KTUtil.find(the.element, '[data-kt-stepper-action="submit"]');

        // Variables
        the.totalStepsNumber = the.steps.length;
        the.passedStepIndex = 0;
        the.currentStepIndex = 1;
        the.clickedStepIndex = 0;

        // Set Current Step
        if ( the.options.startIndex > 1 ) {
            _goTo(the.options.startIndex);
        }

        // Event listeners
        the.nextListener = function(e) {
            e.preventDefault();

            KTEventHandler.trigger(the.element, 'kt.stepper.next', the);
        };

        the.previousListener = function(e) {
            e.preventDefault();

            KTEventHandler.trigger(the.element, 'kt.stepper.previous', the);
        };

        the.stepListener = function(e) {
            e.preventDefault();

            if ( the.steps && the.steps.length > 0 ) {
                for (var i = 0, len = the.steps.length; i < len; i++) {
                    if ( the.steps[i] === this ) {
                        the.clickedStepIndex = i + 1;

                        KTEventHandler.trigger(the.element, 'kt.stepper.click', the);

                        return;
                    }
                }
            }
        };

        // Event Handlers
        KTUtil.addEvent(the.btnNext, 'click', the.nextListener);

        KTUtil.addEvent(the.btnPrevious, 'click', the.previousListener);

        the.stepListenerId = KTUtil.on(the.element, '[data-kt-stepper-action="step"]', 'click', the.stepListener);

        // Bind Instance
        KTUtil.data(the.element).set('stepper', the);
    }

    var _goTo = function(index) {
        // Trigger "change" event
        KTEventHandler.trigger(the.element, 'kt.stepper.change', the);

        // Skip if this step is already shown
        if ( index === the.currentStepIndex || index > the.totalStepsNumber || index < 0 ) {
            return;
        }

        // Validate step number
        index = parseInt(index);

        // Set current step
        the.passedStepIndex = the.currentStepIndex;
        the.currentStepIndex = index;

        // Refresh elements
        _refreshUI();

        // Trigger "changed" event
        KTEventHandler.trigger(the.element, 'kt.stepper.changed', the);

        return the;
    }

    var _goNext = function() {
        return _goTo( _getNextStepIndex() );
    }

    var _goPrevious = function() {
        return _goTo( _getPreviousStepIndex() );
    }

    var _goLast = function() {
        return _goTo( _getLastStepIndex() );
    }

    var _goFirst = function() {
        return _goTo( _getFirstStepIndex() );
    }

    var _refreshUI = function() {
        var state = '';

        if ( _isLastStep() ) {
            state = 'last';
        } else if ( _isFirstStep() ) {
            state = 'first';
        } else {
            state = 'between';
        }

        // Set state class
        KTUtil.removeClass(the.element, 'last');
        KTUtil.removeClass(the.element, 'first');
        KTUtil.removeClass(the.element, 'between');

        KTUtil.addClass(the.element, state);

        // Step Items
        var elements = KTUtil.findAll(the.element, '[data-kt-stepper-element="nav"], [data-kt-stepper-element="content"], [data-kt-stepper-element="info"]');

        if ( elements && elements.length > 0 ) {
            for (var i = 0, len = elements.length; i < len; i++) {
                var element = elements[i];
                var index = KTUtil.index(element) + 1;

                KTUtil.removeClass(element, 'current');
                KTUtil.removeClass(element, 'completed');
                KTUtil.removeClass(element, 'pending');

                if ( index == the.currentStepIndex ) {
                    KTUtil.addClass(element, 'current');

                    if ( the.options.animation !== false && element.getAttribute('data-kt-stepper-element') == 'content' ) {
                        KTUtil.css(element, 'animationDuration', the.options.animationSpeed);

                        var animation = _getStepDirection(the.passedStepIndex) === 'previous' ?  the.options.animationPreviousClass : the.options.animationNextClass;
                        KTUtil.animateClass(element, animation);
                    }
                } else {
                    if ( index < the.currentStepIndex ) {
                        KTUtil.addClass(element, 'completed');
                    } else {
                        KTUtil.addClass(element, 'pending');
                    }
                }
            }
        }
    }

    var _isLastStep = function() {
        return the.currentStepIndex === the.totalStepsNumber;
    }

    var _isFirstStep = function() {
        return the.currentStepIndex === 1;
    }

    var _isBetweenStep = function() {
        return _isLastStep() === false && _isFirstStep() === false;
    }

    var _getNextStepIndex = function() {
        if ( the.totalStepsNumber >= ( the.currentStepIndex + 1 ) ) {
            return the.currentStepIndex + 1;
        } else {
            return the.totalStepsNumber;
        }
    }

    var _getPreviousStepIndex = function() {
        if ( ( the.currentStepIndex - 1 ) > 1 ) {
            return the.currentStepIndex - 1;
        } else {
            return 1;
        }
    }

    var _getFirstStepIndex = function(){
        return 1;
    }

    var _getLastStepIndex = function() {
        return the.totalStepsNumber;
    }

    var _getTotalStepsNumber = function() {
        return the.totalStepsNumber;
    }

    var _getStepDirection = function(index) {
        if ( index > the.currentStepIndex ) {
            return 'next';
        } else {
            return 'previous';
        }
    }

    var _getStepContent = function(index) {
        var content = KTUtil.findAll(the.element, '[data-kt-stepper-element="content"]');

        if ( content[index-1] ) {
            return content[index-1];
        } else {
            return false;
        }
    }

    var _destroy = function() {
        // Event Handlers
        KTUtil.removeEvent(the.btnNext, 'click', the.nextListener);

        KTUtil.removeEvent(the.btnPrevious, 'click', the.previousListener);

        KTUtil.off(the.element, 'click', the.stepListenerId);

        KTUtil.data(the.element).remove('stepper');
    }

    // Construct Class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.getElement = function(index) {
        return the.element;
    }

    the.goTo = function(index) {
        return _goTo(index);
    }

    the.goPrevious = function() {
        return _goPrevious();
    }

    the.goNext = function() {
        return _goNext();
    }

    the.goFirst = function() {
        return _goFirst();
    }

    the.goLast = function() {
        return _goLast();
    }

    the.getCurrentStepIndex = function() {
        return the.currentStepIndex;
    }

    the.getNextStepIndex = function() {
        return _getNextStepIndex();
    }

    the.getPassedStepIndex = function() {
        return the.passedStepIndex;
    }

    the.getClickedStepIndex = function() {
        return the.clickedStepIndex;
    }

    the.getPreviousStepIndex = function() {
        return _getPreviousStepIndex();
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTStepper.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('stepper') ) {
        return KTUtil.data(element).get('stepper');
    } else {
        return null;
    }
}

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTStepper;
}

"use strict";

var KTStickyHandlersInitialized = false;

// Class definition
var KTSticky = function(element, options) {
    ////////////////////////////
    // ** Private Variables  ** //
    ////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default Options
    var defaultOptions = {
        offset: 200,
        reverse: false,
        release: null,
        animation: true,
        animationSpeed: '0.3s',
        animationClass: 'animation-slide-in-down'
    };
    ////////////////////////////
    // ** Private Methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('sticky') === true ) {
            the = KTUtil.data(element).get('sticky');
        } else {
            _init();
        }
    }

    var _init = function() {
        the.element = element;
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('sticky');
        the.name = the.element.getAttribute('data-kt-sticky-name');
        the.attributeName = 'data-kt-sticky-' + the.name;
        the.attributeName2 = 'data-kt-' + the.name;
        the.eventTriggerState = true;
        the.lastScrollTop = 0;
        the.scrollHandler;

        // Set initialized
        the.element.setAttribute('data-kt-sticky', 'true');

        // Event Handlers
        window.addEventListener('scroll', _scroll);

        // Initial Launch
        _scroll();

        // Bind Instance
        KTUtil.data(the.element).set('sticky', the);
    }

    var _scroll = function(e) {
        var offset = _getOption('offset');
        var release = _getOption('release');
        var reverse = _getOption('reverse');
        var st;
        var attrName;
        var diff;

        // Exit if false
        if ( offset === false ) {
            _disable();
            return;
        }

        offset = parseInt(offset);
        release = release ? document.querySelector(release) : null;

        st = KTUtil.getScrollTop();
        diff = document.documentElement.scrollHeight - window.innerHeight - KTUtil.getScrollTop();
        
        var proceed = (!release || (release.offsetTop - release.clientHeight) > st);

        if ( reverse === true ) {  // Release on reverse scroll mode
            if ( st > offset && proceed ) {
                if ( document.body.hasAttribute(the.attributeName) === false) {
                    
                    if (_enable() === false) {
                        return;
                    }

                    document.body.setAttribute(the.attributeName, 'on');
                    document.body.setAttribute(the.attributeName2, 'on');
                    the.element.setAttribute("data-kt-sticky-enabled", "true");
                }

                if ( the.eventTriggerState === true ) {
                    KTEventHandler.trigger(the.element, 'kt.sticky.on', the);
                    KTEventHandler.trigger(the.element, 'kt.sticky.change', the);

                    the.eventTriggerState = false;
                }
            } else { // Back scroll mode
                if ( document.body.hasAttribute(the.attributeName) === true) {
                    _disable();
                    document.body.removeAttribute(the.attributeName);
                    document.body.removeAttribute(the.attributeName2);
                    the.element.removeAttribute("data-kt-sticky-enabled");
                }

                if ( the.eventTriggerState === false ) {
                    KTEventHandler.trigger(the.element, 'kt.sticky.off', the);
                    KTEventHandler.trigger(the.element, 'kt.sticky.change', the);
                    the.eventTriggerState = true;
                }
            }

            the.lastScrollTop = st;
        } else { // Classic scroll mode
            if ( st > offset && proceed ) {
                if ( document.body.hasAttribute(the.attributeName) === false) {
                    
                    if (_enable() === false) {
                        return;
                    } 
                    
                    document.body.setAttribute(the.attributeName, 'on');
                    document.body.setAttribute(the.attributeName2, 'on');
                    the.element.setAttribute("data-kt-sticky-enabled", "true");
                }

                if ( the.eventTriggerState === true ) {
                    KTEventHandler.trigger(the.element, 'kt.sticky.on', the);
                    KTEventHandler.trigger(the.element, 'kt.sticky.change', the);
                    the.eventTriggerState = false;
                }
            } else { // back scroll mode
                if ( document.body.hasAttribute(the.attributeName) === true ) {
                    _disable();
                    document.body.removeAttribute(the.attributeName);
                    document.body.removeAttribute(the.attributeName2);
                    the.element.removeAttribute("data-kt-sticky-enabled");
                }

                if ( the.eventTriggerState === false ) {
                    KTEventHandler.trigger(the.element, 'kt.sticky.off', the);
                    KTEventHandler.trigger(the.element, 'kt.sticky.change', the);
                    the.eventTriggerState = true;
                }
            }
        }      

        if (release) {
            if ( release.offsetTop - release.clientHeight > st ) {
                the.element.setAttribute('data-kt-sticky-released', 'true');
            } else {
                the.element.removeAttribute('data-kt-sticky-released');
            }
        } 
    }

    var _enable = function(update) {
        var top = _getOption('top');
        top = top ? parseInt(top) : 0;

        var left = _getOption('left');
        var right = _getOption('right');
        var width = _getOption('width');
        var zindex = _getOption('zindex');
        var dependencies = _getOption('dependencies');
        var classes = _getOption('class');

        var height = _calculateHeight();
        var heightOffset = _getOption('height-offset');
        heightOffset = heightOffset ? parseInt(heightOffset) : 0;

        if (height + heightOffset + top > KTUtil.getViewPort().height) {
            return false;
        }
        
        if ( update !== true && _getOption('animation') === true ) {
            KTUtil.css(the.element, 'animationDuration', _getOption('animationSpeed'));
            KTUtil.animateClass(the.element, 'animation ' + _getOption('animationClass'));
        }

        if ( classes !== null ) {
            KTUtil.addClass(the.element, classes);
        }

        if ( zindex !== null ) {
            KTUtil.css(the.element, 'z-index', zindex);
            KTUtil.css(the.element, 'position', 'fixed');
        }

        if ( top >= 0 ) {
            KTUtil.css(the.element, 'top', String(top) + 'px');
        }

        if ( width !== null ) {
            if (width['target']) {
                var targetElement = document.querySelector(width['target']);
                if (targetElement) {
                    width = KTUtil.css(targetElement, 'width');
                }
            }

            KTUtil.css(the.element, 'width', width);
        }

        if ( left !== null ) {
            if ( String(left).toLowerCase() === 'auto' ) {
                var offsetLeft = KTUtil.offset(the.element).left;

                if ( offsetLeft >= 0 ) {
                    KTUtil.css(the.element, 'left', String(offsetLeft) + 'px');
                }
            } else {
                KTUtil.css(the.element, 'left', left);
            }
        }

        if ( right !== null ) {
            KTUtil.css(the.element, 'right', right);
        }        

        // Height dependencies
        if ( dependencies !== null ) {
            var dependencyElements = document.querySelectorAll(dependencies);
            
            if ( dependencyElements && dependencyElements.length > 0 ) {
                for ( var i = 0, len = dependencyElements.length; i < len; i++ ) {
                    KTUtil.css(dependencyElements[i], 'padding-top', String(height) + 'px');
                }
            }
        }
    }

    var _disable = function() {
        KTUtil.css(the.element, 'top', '');
        KTUtil.css(the.element, 'width', '');
        KTUtil.css(the.element, 'left', '');
        KTUtil.css(the.element, 'right', '');
        KTUtil.css(the.element, 'z-index', '');
        KTUtil.css(the.element, 'position', '');

        var dependencies = _getOption('dependencies');
        var classes = _getOption('class');

        if ( classes !== null ) {
            KTUtil.removeClass(the.element, classes);
        }

        // Height dependencies
        if ( dependencies !== null ) {
            var dependencyElements = document.querySelectorAll(dependencies);

            if ( dependencyElements && dependencyElements.length > 0 ) {
                for ( var i = 0, len = dependencyElements.length; i < len; i++ ) {
                    KTUtil.css(dependencyElements[i], 'padding-top', '');
                }
            }
        }
    }

    var _check = function() {

    }

    var _calculateHeight = function() {
        var height = parseFloat(KTUtil.css(the.element, 'height'));

        height = height + parseFloat(KTUtil.css(the.element, 'margin-top'));
        height = height + parseFloat(KTUtil.css(the.element, 'margin-bottom'));
        
        if (KTUtil.css(element, 'border-top')) {
            height = height + parseFloat(KTUtil.css(the.element, 'border-top'));
        }

        if (KTUtil.css(element, 'border-bottom')) {
            height = height + parseFloat(KTUtil.css(the.element, 'border-bottom'));
        }

        return height;
    }

    var _getOption = function(name) {
        if ( the.element.hasAttribute('data-kt-sticky-' + name) === true ) {
            var attr = the.element.getAttribute('data-kt-sticky-' + name);
            var value = KTUtil.getResponsiveValue(attr);

            if ( value !== null && String(value) === 'true' ) {
                value = true;
            } else if ( value !== null && String(value) === 'false' ) {
                value = false;
            }

            return value;
        } else {
            var optionName = KTUtil.snakeToCamel(name);

            if ( the.options[optionName] ) {
                return KTUtil.getResponsiveValue(the.options[optionName]);
            } else {
                return null;
            }
        }
    }

    var _destroy = function() {
        window.removeEventListener('scroll', _scroll);
        KTUtil.data(the.element).remove('sticky');
    }

    // Construct Class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Methods
    the.update = function() {
        if ( document.body.hasAttribute(the.attributeName) === true ) {
            _disable();
            document.body.removeAttribute(the.attributeName);
            document.body.removeAttribute(the.attributeName2);
            _enable(true);
            document.body.setAttribute(the.attributeName, 'on');
            document.body.setAttribute(the.attributeName2, 'on');
        }
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTSticky.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('sticky') ) {
        return KTUtil.data(element).get('sticky');
    } else {
        return null;
    }
}

// Create instances
KTSticky.createInstances = function(selector = '[data-kt-sticky="true"]') {
    // Initialize Menus
    var elements = document.body.querySelectorAll(selector);
    var sticky;

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            sticky = new KTSticky(elements[i]);
        }
    }
}

// Window resize handler
KTSticky.handleResize = function() {
    window.addEventListener('resize', function() {
        var timer;
    
        KTUtil.throttle(timer, function() {
            // Locate and update Offcanvas instances on window resize
            var elements = document.body.querySelectorAll('[data-kt-sticky="true"]');
    
            if ( elements && elements.length > 0 ) {
                for (var i = 0, len = elements.length; i < len; i++) {
                    var sticky = KTSticky.getInstance(elements[i]);
                    if (sticky) {
                        sticky.update();
                    }
                }
            }
        }, 200);
    });
}

// Global initialization
KTSticky.init = function() {
    KTSticky.createInstances();

    if (KTStickyHandlersInitialized === false) {
        KTSticky.handleResize();
        KTStickyHandlersInitialized = true;
    }    
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTSticky;
}

"use strict";

var KTSwapperHandlersInitialized = false;

// Class definition
var KTSwapper = function(element, options) {
    ////////////////////////////
    // ** Private Variables  ** //
    ////////////////////////////
    var the = this;

    if ( typeof element === "undefined" || element === null ) {
        return;
    }

    // Default Options
    var defaultOptions = {
        mode: 'append'
    };

    ////////////////////////////
    // ** Private Methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('swapper') === true ) {
            the = KTUtil.data(element).get('swapper');
        } else {
            _init();
        }
    }

    var _init = function() {
        the.element = element;
        the.options = KTUtil.deepExtend({}, defaultOptions, options);

        // Set initialized
        the.element.setAttribute('data-kt-swapper', 'true');

        // Initial update
        _update();

        // Bind Instance
        KTUtil.data(the.element).set('swapper', the);
    }

    var _update = function(e) {
        var parentSelector = _getOption('parent');

        var mode = _getOption('mode');
        var parentElement = parentSelector ? document.querySelector(parentSelector) : null;
       

        if (parentElement && element.parentNode !== parentElement) {
            if (mode === 'prepend') {
                parentElement.prepend(element);
            } else if (mode === 'append') {
                parentElement.append(element);
            }
        }
    }

    var _getOption = function(name) {
        if ( the.element.hasAttribute('data-kt-swapper-' + name) === true ) {
            var attr = the.element.getAttribute('data-kt-swapper-' + name);
            var value = KTUtil.getResponsiveValue(attr);

            if ( value !== null && String(value) === 'true' ) {
                value = true;
            } else if ( value !== null && String(value) === 'false' ) {
                value = false;
            }

            return value;
        } else {
            var optionName = KTUtil.snakeToCamel(name);

            if ( the.options[optionName] ) {
                return KTUtil.getResponsiveValue(the.options[optionName]);
            } else {
                return null;
            }
        }
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('swapper');
    }

    // Construct Class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Methods
    the.update = function() {
        _update();
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTSwapper.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('swapper') ) {
        return KTUtil.data(element).get('swapper');
    } else {
        return null;
    }
}

// Create instances
KTSwapper.createInstances = function(selector = '[data-kt-swapper="true"]') {
    // Initialize Menus
    var elements = document.querySelectorAll(selector);
    var swapper;

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            swapper = new KTSwapper(elements[i]);
        }
    }
}

// Window resize handler
KTSwapper.handleResize = function() {
    window.addEventListener('resize', function() {
        var timer;
    
        KTUtil.throttle(timer, function() {
            // Locate and update Offcanvas instances on window resize
            var elements = document.querySelectorAll('[data-kt-swapper="true"]');
    
            if ( elements && elements.length > 0 ) {
                for (var i = 0, len = elements.length; i < len; i++) {
                    var swapper = KTSwapper.getInstance(elements[i]);
                    if (swapper) {
                        swapper.update();
                    }                
                }
            }
        }, 200);
    });
};

// Global initialization
KTSwapper.init = function() {
    KTSwapper.createInstances();

    if (KTSwapperHandlersInitialized === false) {
        KTSwapper.handleResize();
        KTSwapperHandlersInitialized = true;
    }
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTSwapper;
}

"use strict";

// Class definition
var KTToggle = function(element, options) {
    ////////////////////////////
    // ** Private variables  ** //
    ////////////////////////////
    var the = this;

    if (!element) {
        return;
    }

    // Default Options
    var defaultOptions = {
        saveState: true
    };

    ////////////////////////////
    // ** Private methods  ** //
    ////////////////////////////

    var _construct = function() {
        if ( KTUtil.data(element).has('toggle') === true ) {
            the = KTUtil.data(element).get('toggle');
        } else {
            _init();
        }
    }

    var _init = function() {
        // Variables
        the.options = KTUtil.deepExtend({}, defaultOptions, options);
        the.uid = KTUtil.getUniqueId('toggle');

        // Elements
        the.element = element;

        the.target = document.querySelector(the.element.getAttribute('data-kt-toggle-target')) ? document.querySelector(the.element.getAttribute('data-kt-toggle-target')) : the.element;
        the.state = the.element.hasAttribute('data-kt-toggle-state') ? the.element.getAttribute('data-kt-toggle-state') : '';
        the.mode = the.element.hasAttribute('data-kt-toggle-mode') ? the.element.getAttribute('data-kt-toggle-mode') : '';
        the.attribute = 'data-kt-' + the.element.getAttribute('data-kt-toggle-name');

        // Event Handlers
        _handlers();

        // Bind Instance
        KTUtil.data(the.element).set('toggle', the);
    }

    var _handlers = function() {
        KTUtil.addEvent(the.element, 'click', function(e) {
            e.preventDefault();

            if ( the.mode !== '' ) {
                if ( the.mode === 'off' && _isEnabled() === false ) {
                    _toggle();
                } else if ( the.mode === 'on' && _isEnabled() === true ) {
                    _toggle();
                }
            } else {
                _toggle();
            }
        });
    }

    // Event handlers
    var _toggle = function() {
        // Trigger "after.toggle" event
        KTEventHandler.trigger(the.element, 'kt.toggle.change', the);

        if ( _isEnabled() ) {
            _disable();
        } else {
            _enable();
        }       

        // Trigger "before.toggle" event
        KTEventHandler.trigger(the.element, 'kt.toggle.changed', the);

        return the;
    }

    var _enable = function() {
        if ( _isEnabled() === true ) {
            return;
        }

        KTEventHandler.trigger(the.element, 'kt.toggle.enable', the);

        the.target.setAttribute(the.attribute, 'on');

        if (the.state.length > 0) {
            the.element.classList.add(the.state);
        }        

        if ( typeof KTCookie !== 'undefined' && the.options.saveState === true ) {
            KTCookie.set(the.attribute, 'on');
        }

        KTEventHandler.trigger(the.element, 'kt.toggle.enabled', the);

        return the;
    }

    var _disable = function() {
        if ( _isEnabled() === false ) {
            return;
        }

        KTEventHandler.trigger(the.element, 'kt.toggle.disable', the);

        the.target.removeAttribute(the.attribute);

        if (the.state.length > 0) {
            the.element.classList.remove(the.state);
        } 

        if ( typeof KTCookie !== 'undefined' && the.options.saveState === true ) {
            KTCookie.remove(the.attribute);
        }

        KTEventHandler.trigger(the.element, 'kt.toggle.disabled', the);

        return the;
    }

    var _isEnabled = function() {
        return (String(the.target.getAttribute(the.attribute)).toLowerCase() === 'on');
    }

    var _destroy = function() {
        KTUtil.data(the.element).remove('toggle');
    }

    // Construct class
    _construct();

    ///////////////////////
    // ** Public API  ** //
    ///////////////////////

    // Plugin API
    the.toggle = function() {
        return _toggle();
    }

    the.enable = function() {
        return _enable();
    }

    the.disable = function() {
        return _disable();
    }

    the.isEnabled = function() {
        return _isEnabled();
    }

    the.goElement = function() {
        return the.element;
    }

    the.destroy = function() {
        return _destroy();
    }

    // Event API
    the.on = function(name, handler) {
        return KTEventHandler.on(the.element, name, handler);
    }

    the.one = function(name, handler) {
        return KTEventHandler.one(the.element, name, handler);
    }

    the.off = function(name, handlerId) {
        return KTEventHandler.off(the.element, name, handlerId);
    }

    the.trigger = function(name, event) {
        return KTEventHandler.trigger(the.element, name, event, the, event);
    }
};

// Static methods
KTToggle.getInstance = function(element) {
    if ( element !== null && KTUtil.data(element).has('toggle') ) {
        return KTUtil.data(element).get('toggle');
    } else {
        return null;
    }
}

// Create instances
KTToggle.createInstances = function(selector = '[data-kt-toggle]') {
    // Get instances
    var elements = document.body.querySelectorAll(selector);

    if ( elements && elements.length > 0 ) {
        for (var i = 0, len = elements.length; i < len; i++) {
            // Initialize instances
            new KTToggle(elements[i]);
        }
    }
}

// Global initialization
KTToggle.init = function() {
    KTToggle.createInstances();
};

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTToggle;
}
"use strict";

/**
 * @class KTUtil  base utilize class that privides helper functions
 */

// Polyfills

// Element.matches() polyfill
if (!Element.prototype.matches) {
    Element.prototype.matches = function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
    };
}

/**
 * Element.closest() polyfill
 * https://developer.mozilla.org/en-US/docs/Web/API/Element/closest#Polyfill
 */
if (!Element.prototype.closest) {
	Element.prototype.closest = function (s) {
		var el = this;
		var ancestor = this;
		if (!document.documentElement.contains(el)) return null;
		do {
			if (ancestor.matches(s)) return ancestor;
			ancestor = ancestor.parentElement;
		} while (ancestor !== null);
		return null;
	};
}

/**
 * ChildNode.remove() polyfill
 * https://gomakethings.com/removing-an-element-from-the-dom-the-es6-way/
 * @author Chris Ferdinandi
 * @license MIT
 */
(function (elem) {
	for (var i = 0; i < elem.length; i++) {
		if (!window[elem[i]] || 'remove' in window[elem[i]].prototype) continue;
		window[elem[i]].prototype.remove = function () {
			this.parentNode.removeChild(this);
		};
	}
})(['Element', 'CharacterData', 'DocumentType']);


//
// requestAnimationFrame polyfill by Erik Möller.
//  With fixes from Paul Irish and Tino Zijdel
//
//  http://paulirish.com/2011/requestanimationframe-for-smart-animating/
//  http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
//
//  MIT license
//
(function() {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame =
            window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/prepend()/prepend().md
(function(arr) {
    arr.forEach(function(item) {
        if (item.hasOwnProperty('prepend')) {
            return;
        }
        Object.defineProperty(item, 'prepend', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function prepend() {
                var argArr = Array.prototype.slice.call(arguments),
                    docFrag = document.createDocumentFragment();

                argArr.forEach(function(argItem) {
                    var isNode = argItem instanceof Node;
                    docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
                });

                this.insertBefore(docFrag, this.firstChild);
            }
        });
    });
})([Element.prototype, Document.prototype, DocumentFragment.prototype]);

// getAttributeNames
if (Element.prototype.getAttributeNames == undefined) {
  Element.prototype.getAttributeNames = function () {
    var attributes = this.attributes;
    var length = attributes.length;
    var result = new Array(length);
    for (var i = 0; i < length; i++) {
      result[i] = attributes[i].name;
    }
    return result;
  };
}

// Global variables
window.KTUtilElementDataStore = {};
window.KTUtilElementDataStoreID = 0;
window.KTUtilDelegatedEventHandlers = {};

var KTUtil = function() {
    var resizeHandlers = [];

    /**
     * Handle window resize event with some
     * delay to attach event handlers upon resize complete
     */
    var _windowResizeHandler = function() {
        var _runResizeHandlers = function() {
            // reinitialize other subscribed elements
            for (var i = 0; i < resizeHandlers.length; i++) {
                var each = resizeHandlers[i];
                each.call();
            }
        };

        var timer;

        window.addEventListener('resize', function() {
            KTUtil.throttle(timer, function() {
                _runResizeHandlers();
            }, 200);
        });
    };

    return {
        /**
         * Class main initializer.
         * @param {object} settings.
         * @returns null
         */
        //main function to initiate the theme
        init: function(settings) {
            _windowResizeHandler();
        },

        /**
         * Adds window resize event handler.
         * @param {function} callback function.
         */
        addResizeHandler: function(callback) {
            resizeHandlers.push(callback);
        },

        /**
         * Removes window resize event handler.
         * @param {function} callback function.
         */
        removeResizeHandler: function(callback) {
            for (var i = 0; i < resizeHandlers.length; i++) {
                if (callback === resizeHandlers[i]) {
                    delete resizeHandlers[i];
                }
            }
        },

        /**
         * Trigger window resize handlers.
         */
        runResizeHandlers: function() {
            _runResizeHandlers();
        },

        resize: function() {
            if (typeof(Event) === 'function') {
                // modern browsers
                window.dispatchEvent(new Event('resize'));
            } else {
                // for IE and other old browsers
                // causes deprecation warning on modern browsers
                var evt = window.document.createEvent('UIEvents');
                evt.initUIEvent('resize', true, false, window, 0);
                window.dispatchEvent(evt);
            }
        },

        /**
         * Get GET parameter value from URL.
         * @param {string} paramName Parameter name.
         * @returns {string}
         */
        getURLParam: function(paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }

            return null;
        },

        /**
         * Checks whether current device is mobile touch.
         * @returns {boolean}
         */
        isMobileDevice: function() {
            var test = (this.getViewPort().width < this.getBreakpoint('lg') ? true : false);

            if (test === false) {
                // For use within normal web clients
                test = navigator.userAgent.match(/iPad/i) != null;
            }

            return test;
        },

        /**
         * Checks whether current device is desktop.
         * @returns {boolean}
         */
        isDesktopDevice: function() {
            return KTUtil.isMobileDevice() ? false : true;
        },

        /**
         * Gets browser window viewport size. Ref:
         * http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
         * @returns {object}
         */
        getViewPort: function() {
            var e = window,
                a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }

            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        },

		/**
         * Checks whether given device mode is currently activated.
         * @param {string} mode Responsive mode name(e.g: desktop,
         *     desktop-and-tablet, tablet, tablet-and-mobile, mobile)
         * @returns {boolean}
         */
        isBreakpointUp: function(mode) {
            var width = this.getViewPort().width;
			var breakpoint = this.getBreakpoint(mode);

			return (width >= breakpoint);
        },

		isBreakpointDown: function(mode) {
            var width = this.getViewPort().width;
			var breakpoint = this.getBreakpoint(mode);

			return (width < breakpoint);
        },

        getViewportWidth: function() {
            return this.getViewPort().width;
        },

        /**
         * Generates unique ID for give prefix.
         * @param {string} prefix Prefix for generated ID
         * @returns {boolean}
         */
        getUniqueId: function(prefix) {
            return prefix + Math.floor(Math.random() * (new Date()).getTime());
        },

        /**
         * Gets window width for give breakpoint mode.
         * @param {string} mode Responsive mode name(e.g: xl, lg, md, sm)
         * @returns {number}
         */
        getBreakpoint: function(breakpoint) {
            var value = this.getCssVariableValue('--bs-' + breakpoint);

            if ( value ) {
                value = parseInt(value.trim());
            } 

            return value;
        },

        /**
         * Checks whether object has property matchs given key path.
         * @param {object} obj Object contains values paired with given key path
         * @param {string} keys Keys path seperated with dots
         * @returns {object}
         */
        isset: function(obj, keys) {
            var stone;

            keys = keys || '';

            if (keys.indexOf('[') !== -1) {
                throw new Error('Unsupported object path notation.');
            }

            keys = keys.split('.');

            do {
                if (obj === undefined) {
                    return false;
                }

                stone = keys.shift();

                if (!obj.hasOwnProperty(stone)) {
                    return false;
                }

                obj = obj[stone];

            } while (keys.length);

            return true;
        },

        /**
         * Gets highest z-index of the given element parents
         * @param {object} el jQuery element object
         * @returns {number}
         */
        getHighestZindex: function(el) {
            var position, value;

            while (el && el !== document) {
                // Ignore z-index if position is set to a value where z-index is ignored by the browser
                // This makes behavior of this function consistent across browsers
                // WebKit always returns auto if the element is positioned
                position = KTUtil.css(el, 'position');

                if (position === "absolute" || position === "relative" || position === "fixed") {
                    // IE returns 0 when zIndex is not specified
                    // other browsers return a string
                    // we ignore the case of nested elements with an explicit value of 0
                    // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
                    value = parseInt(KTUtil.css(el, 'z-index'));

                    if (!isNaN(value) && value !== 0) {
                        return value;
                    }
                }

                el = el.parentNode;
            }

            return 1;
        },

        /**
         * Checks whether the element has any parent with fixed positionfreg
         * @param {object} el jQuery element object
         * @returns {boolean}
         */
        hasFixedPositionedParent: function(el) {
            var position;

            while (el && el !== document) {
                position = KTUtil.css(el, 'position');

                if (position === "fixed") {
                    return true;
                }

                el = el.parentNode;
            }

            return false;
        },

        /**
         * Simulates delay
         */
        sleep: function(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        },

        /**
         * Gets randomly generated integer value within given min and max range
         * @param {number} min Range start value
         * @param {number} max Range end value
         * @returns {number}
         */
        getRandomInt: function(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        /**
         * Checks whether Angular library is included
         * @returns {boolean}
         */
        isAngularVersion: function() {
            return window.Zone !== undefined ? true : false;
        },

        // Deep extend:  $.extend(true, {}, objA, objB);
        deepExtend: function(out) {
            out = out || {};

            for (var i = 1; i < arguments.length; i++) {
                var obj = arguments[i];
                if (!obj) continue;

                for (var key in obj) {
                    if (!obj.hasOwnProperty(key)) {
                        continue;
                    }

                    // based on https://javascriptweblog.wordpress.com/2011/08/08/fixing-the-javascript-typeof-operator/
                    if ( Object.prototype.toString.call(obj[key]) === '[object Object]' ) {
                        out[key] = KTUtil.deepExtend(out[key], obj[key]);
                        continue;
                    }

                    out[key] = obj[key];
                }
            }

            return out;
        },

        // extend:  $.extend({}, objA, objB);
        extend: function(out) {
            out = out || {};

            for (var i = 1; i < arguments.length; i++) {
                if (!arguments[i])
                    continue;

                for (var key in arguments[i]) {
                    if (arguments[i].hasOwnProperty(key))
                        out[key] = arguments[i][key];
                }
            }

            return out;
        },

        getBody: function() {
            return document.getElementsByTagName('body')[0];
        },

        /**
         * Checks whether the element has given classes
         * @param {object} el jQuery element object
         * @param {string} Classes string
         * @returns {boolean}
         */
        hasClasses: function(el, classes) {
            if (!el) {
                return;
            }

            var classesArr = classes.split(" ");

            for (var i = 0; i < classesArr.length; i++) {
                if (KTUtil.hasClass(el, KTUtil.trim(classesArr[i])) == false) {
                    return false;
                }
            }

            return true;
        },

        hasClass: function(el, className) {
            if (!el) {
                return;
            }

            return el.classList ? el.classList.contains(className) : new RegExp('\\b' + className + '\\b').test(el.className);
        },

        addClass: function(el, className) {
            if (!el || typeof className === 'undefined') {
                return;
            }

            var classNames = className.split(' ');

            if (el.classList) {
                for (var i = 0; i < classNames.length; i++) {
                    if (classNames[i] && classNames[i].length > 0) {
                        el.classList.add(KTUtil.trim(classNames[i]));
                    }
                }
            } else if (!KTUtil.hasClass(el, className)) {
                for (var x = 0; x < classNames.length; x++) {
                    el.className += ' ' + KTUtil.trim(classNames[x]);
                }
            }
        },

        removeClass: function(el, className) {
          if (!el || typeof className === 'undefined') {
                return;
            }

            var classNames = className.split(' ');

            if (el.classList) {
                for (var i = 0; i < classNames.length; i++) {
                    el.classList.remove(KTUtil.trim(classNames[i]));
                }
            } else if (KTUtil.hasClass(el, className)) {
                for (var x = 0; x < classNames.length; x++) {
                    el.className = el.className.replace(new RegExp('\\b' + KTUtil.trim(classNames[x]) + '\\b', 'g'), '');
                }
            }
        },

        triggerCustomEvent: function(el, eventName, data) {
            var event;
            if (window.CustomEvent) {
                event = new CustomEvent(eventName, {
                    detail: data
                });
            } else {
                event = document.createEvent('CustomEvent');
                event.initCustomEvent(eventName, true, true, data);
            }

            el.dispatchEvent(event);
        },

        triggerEvent: function(node, eventName) {
            // Make sure we use the ownerDocument from the provided node to avoid cross-window problems
            var doc;

            if (node.ownerDocument) {
                doc = node.ownerDocument;
            } else if (node.nodeType == 9) {
                // the node may be the document itself, nodeType 9 = DOCUMENT_NODE
                doc = node;
            } else {
                throw new Error("Invalid node passed to fireEvent: " + node.id);
            }

            if (node.dispatchEvent) {
                // Gecko-style approach (now the standard) takes more work
                var eventClass = "";

                // Different events have different event classes.
                // If this switch statement can't map an eventName to an eventClass,
                // the event firing is going to fail.
                switch (eventName) {
                case "click": // Dispatching of 'click' appears to not work correctly in Safari. Use 'mousedown' or 'mouseup' instead.
                case "mouseenter":
                case "mouseleave":
                case "mousedown":
                case "mouseup":
                    eventClass = "MouseEvents";
                    break;

                case "focus":
                case "change":
                case "blur":
                case "select":
                    eventClass = "HTMLEvents";
                    break;

                default:
                    throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
                    break;
                }
                var event = doc.createEvent(eventClass);

                var bubbles = eventName == "change" ? false : true;
                event.initEvent(eventName, bubbles, true); // All events created as bubbling and cancelable.

                event.synthetic = true; // allow detection of synthetic events
                // The second parameter says go ahead with the default action
                node.dispatchEvent(event, true);
            } else if (node.fireEvent) {
                // IE-old school style
                var event = doc.createEventObject();
                event.synthetic = true; // allow detection of synthetic events
                node.fireEvent("on" + eventName, event);
            }
        },

        index: function( el ){
            var c = el.parentNode.children, i = 0;
            for(; i < c.length; i++ )
                if( c[i] == el ) return i;
        },

        trim: function(string) {
            return string.trim();
        },

        eventTriggered: function(e) {
            if (e.currentTarget.dataset.triggered) {
                return true;
            } else {
                e.currentTarget.dataset.triggered = true;

                return false;
            }
        },

        remove: function(el) {
            if (el && el.parentNode) {
                el.parentNode.removeChild(el);
            }
        },

        find: function(parent, query) {
            if ( parent !== null) {
                return parent.querySelector(query);
            } else {
                return null;
            }
        },

        findAll: function(parent, query) {
            if ( parent !== null ) {
                return parent.querySelectorAll(query);
            } else {
                return null;
            }
        },

        insertAfter: function(el, referenceNode) {
            return referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
        },

        parents: function(elem, selector) {
            // Set up a parent array
            var parents = [];

            // Push each parent element to the array
            for ( ; elem && elem !== document; elem = elem.parentNode ) {
                if (selector) {
                    if (elem.matches(selector)) {
                        parents.push(elem);
                    }
                    continue;
                }
                parents.push(elem);
            }

            // Return our parent array
            return parents;
        },

        children: function(el, selector, log) {
            if (!el || !el.childNodes) {
                return null;
            }

            var result = [],
                i = 0,
                l = el.childNodes.length;

            for (var i; i < l; ++i) {
                if (el.childNodes[i].nodeType == 1 && KTUtil.matches(el.childNodes[i], selector, log)) {
                    result.push(el.childNodes[i]);
                }
            }

            return result;
        },

        child: function(el, selector, log) {
            var children = KTUtil.children(el, selector, log);

            return children ? children[0] : null;
        },

        matches: function(el, selector, log) {
            var p = Element.prototype;
            var f = p.matches || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || function(s) {
                return [].indexOf.call(document.querySelectorAll(s), this) !== -1;
            };

            if (el && el.tagName) {
                return f.call(el, selector);
            } else {
                return false;
            }
        },

        data: function(el) {
            return {
                set: function(name, data) {
                    if (!el) {
                        return;
                    }

                    if (el.customDataTag === undefined) {
                        window.KTUtilElementDataStoreID++;
                        el.customDataTag = window.KTUtilElementDataStoreID;
                    }

                    if (window.KTUtilElementDataStore[el.customDataTag] === undefined) {
                        window.KTUtilElementDataStore[el.customDataTag] = {};
                    }

                    window.KTUtilElementDataStore[el.customDataTag][name] = data;
                },

                get: function(name) {
                    if (!el) {
                        return;
                    }

                    if (el.customDataTag === undefined) {
                        return null;
                    }

                    return this.has(name) ? window.KTUtilElementDataStore[el.customDataTag][name] : null;
                },

                has: function(name) {
                    if (!el) {
                        return false;
                    }

                    if (el.customDataTag === undefined) {
                        return false;
                    }

                    return (window.KTUtilElementDataStore[el.customDataTag] && window.KTUtilElementDataStore[el.customDataTag][name]) ? true : false;
                },

                remove: function(name) {
                    if (el && this.has(name)) {
                        delete window.KTUtilElementDataStore[el.customDataTag][name];
                    }
                }
            };
        },

        outerWidth: function(el, margin) {
            var width;

            if (margin === true) {
                width = parseFloat(el.offsetWidth);
                width += parseFloat(KTUtil.css(el, 'margin-left')) + parseFloat(KTUtil.css(el, 'margin-right'));

                return parseFloat(width);
            } else {
                width = parseFloat(el.offsetWidth);

                return width;
            }
        },

        offset: function(el) {
            var rect, win;

            if ( !el ) {
                return;
            }

            // Return zeros for disconnected and hidden (display: none) elements (gh-2310)
            // Support: IE <=11 only
            // Running getBoundingClientRect on a
            // disconnected node in IE throws an error

            if ( !el.getClientRects().length ) {
                return { top: 0, left: 0 };
            }

            // Get document-relative position by adding viewport scroll to viewport-relative gBCR
            rect = el.getBoundingClientRect();
            win = el.ownerDocument.defaultView;

            return {
                top: rect.top + win.pageYOffset,
                left: rect.left + win.pageXOffset,
                right: window.innerWidth - (el.offsetLeft + el.offsetWidth)
            };
        },

        height: function(el) {
            return KTUtil.css(el, 'height');
        },

        outerHeight: function(el, withMargin) {
            var height = el.offsetHeight;
            var style;

            if (typeof withMargin !== 'undefined' && withMargin === true) {
                style = getComputedStyle(el);
                height += parseInt(style.marginTop) + parseInt(style.marginBottom);

                return height;
            } else {
                return height;
            }
        },

        visible: function(el) {
            return !(el.offsetWidth === 0 && el.offsetHeight === 0);
        },

        isVisibleInContainer: function (el, container, offset = 0) {
            const eleTop = el.offsetTop;
            const eleBottom = eleTop + el.clientHeight + offset;
            const containerTop = container.scrollTop;
            const containerBottom = containerTop + container.clientHeight;
        
            // The element is fully visible in the container
            return (
                (eleTop >= containerTop && eleBottom <= containerBottom)
            );
        },

        getRelativeTopPosition: function (el, container) {
            return el.offsetTop - container.offsetTop;
        },

        attr: function(el, name, value) {
            if (el == undefined) {
                return;
            }

            if (value !== undefined) {
                el.setAttribute(name, value);
            } else {
                return el.getAttribute(name);
            }
        },

        hasAttr: function(el, name) {
            if (el == undefined) {
                return;
            }

            return el.getAttribute(name) ? true : false;
        },

        removeAttr: function(el, name) {
            if (el == undefined) {
                return;
            }

            el.removeAttribute(name);
        },

        animate: function(from, to, duration, update, easing, done) {
            /**
             * TinyAnimate.easings
             *  Adapted from jQuery Easing
             */
            var easings = {};
            var easing;

            easings.linear = function(t, b, c, d) {
                return c * t / d + b;
            };

            easing = easings.linear;

            // Early bail out if called incorrectly
            if (typeof from !== 'number' ||
                typeof to !== 'number' ||
                typeof duration !== 'number' ||
                typeof update !== 'function') {
                return;
            }

            // Create mock done() function if necessary
            if (typeof done !== 'function') {
                done = function() {};
            }

            // Pick implementation (requestAnimationFrame | setTimeout)
            var rAF = window.requestAnimationFrame || function(callback) {
                window.setTimeout(callback, 1000 / 50);
            };

            // Animation loop
            var canceled = false;
            var change = to - from;

            function loop(timestamp) {
                var time = (timestamp || +new Date()) - start;

                if (time >= 0) {
                    update(easing(time, from, change, duration));
                }
                if (time >= 0 && time >= duration) {
                    update(to);
                    done();
                } else {
                    rAF(loop);
                }
            }

            update(from);

            // Start animation loop
            var start = window.performance && window.performance.now ? window.performance.now() : +new Date();

            rAF(loop);
        },

        actualCss: function(el, prop, cache) {
            var css = '';

            if (el instanceof HTMLElement === false) {
                return;
            }

            if (!el.getAttribute('kt-hidden-' + prop) || cache === false) {
                var value;

                // the element is hidden so:
                // making the el block so we can meassure its height but still be hidden
                css = el.style.cssText;
                el.style.cssText = 'position: absolute; visibility: hidden; display: block;';

                if (prop == 'width') {
                    value = el.offsetWidth;
                } else if (prop == 'height') {
                    value = el.offsetHeight;
                }

                el.style.cssText = css;

                // store it in cache
                el.setAttribute('kt-hidden-' + prop, value);

                return parseFloat(value);
            } else {
                // store it in cache
                return parseFloat(el.getAttribute('kt-hidden-' + prop));
            }
        },

        actualHeight: function(el, cache) {
            return KTUtil.actualCss(el, 'height', cache);
        },

        actualWidth: function(el, cache) {
            return KTUtil.actualCss(el, 'width', cache);
        },

        getScroll: function(element, method) {
            // The passed in `method` value should be 'Top' or 'Left'
            method = 'scroll' + method;
            return (element == window || element == document) ? (
                self[(method == 'scrollTop') ? 'pageYOffset' : 'pageXOffset'] ||
                (browserSupportsBoxModel && document.documentElement[method]) ||
                document.body[method]
            ) : element[method];
        },

        css: function(el, styleProp, value, important) {
            if (!el) {
                return;
            }

            if (value !== undefined) {
                if ( important === true ) {
                    el.style.setProperty(styleProp, value, 'important');
                } else {
                    el.style[styleProp] = value;
                }
            } else {
                var defaultView = (el.ownerDocument || document).defaultView;

                // W3C standard way:
                if (defaultView && defaultView.getComputedStyle) {
                    // sanitize property name to css notation
                    // (hyphen separated words eg. font-Size)
                    styleProp = styleProp.replace(/([A-Z])/g, "-$1").toLowerCase();

                    return defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
                } else if (el.currentStyle) { // IE
                    // sanitize property name to camelCase
                    styleProp = styleProp.replace(/\-(\w)/g, function(str, letter) {
                        return letter.toUpperCase();
                    });

                    value = el.currentStyle[styleProp];

                    // convert other units to pixels on IE
                    if (/^\d+(em|pt|%|ex)?$/i.test(value)) {
                        return (function(value) {
                            var oldLeft = el.style.left, oldRsLeft = el.runtimeStyle.left;

                            el.runtimeStyle.left = el.currentStyle.left;
                            el.style.left = value || 0;
                            value = el.style.pixelLeft + "px";
                            el.style.left = oldLeft;
                            el.runtimeStyle.left = oldRsLeft;

                            return value;
                        })(value);
                    }

                    return value;
                }
            }
        },

        slide: function(el, dir, speed, callback, recalcMaxHeight) {
            if (!el || (dir == 'up' && KTUtil.visible(el) === false) || (dir == 'down' && KTUtil.visible(el) === true)) {
                return;
            }

            speed = (speed ? speed : 600);
            var calcHeight = KTUtil.actualHeight(el);
            var calcPaddingTop = false;
            var calcPaddingBottom = false;

            if (KTUtil.css(el, 'padding-top') && KTUtil.data(el).has('slide-padding-top') !== true) {
                KTUtil.data(el).set('slide-padding-top', KTUtil.css(el, 'padding-top'));
            }

            if (KTUtil.css(el, 'padding-bottom') && KTUtil.data(el).has('slide-padding-bottom') !== true) {
                KTUtil.data(el).set('slide-padding-bottom', KTUtil.css(el, 'padding-bottom'));
            }

            if (KTUtil.data(el).has('slide-padding-top')) {
                calcPaddingTop = parseInt(KTUtil.data(el).get('slide-padding-top'));
            }

            if (KTUtil.data(el).has('slide-padding-bottom')) {
                calcPaddingBottom = parseInt(KTUtil.data(el).get('slide-padding-bottom'));
            }

            if (dir == 'up') { // up
                el.style.cssText = 'display: block; overflow: hidden;';

                if (calcPaddingTop) {
                    KTUtil.animate(0, calcPaddingTop, speed, function(value) {
                        el.style.paddingTop = (calcPaddingTop - value) + 'px';
                    }, 'linear');
                }

                if (calcPaddingBottom) {
                    KTUtil.animate(0, calcPaddingBottom, speed, function(value) {
                        el.style.paddingBottom = (calcPaddingBottom - value) + 'px';
                    }, 'linear');
                }

                KTUtil.animate(0, calcHeight, speed, function(value) {
                    el.style.height = (calcHeight - value) + 'px';
                }, 'linear', function() {
                    el.style.height = '';
                    el.style.display = 'none';

                    if (typeof callback === 'function') {
                        callback();
                    }
                });


            } else if (dir == 'down') { // down
                el.style.cssText = 'display: block; overflow: hidden;';

                if (calcPaddingTop) {
                    KTUtil.animate(0, calcPaddingTop, speed, function(value) {//
                        el.style.paddingTop = value + 'px';
                    }, 'linear', function() {
                        el.style.paddingTop = '';
                    });
                }

                if (calcPaddingBottom) {
                    KTUtil.animate(0, calcPaddingBottom, speed, function(value) {
                        el.style.paddingBottom = value + 'px';
                    }, 'linear', function() {
                        el.style.paddingBottom = '';
                    });
                }

                KTUtil.animate(0, calcHeight, speed, function(value) {
                    el.style.height = value + 'px';
                }, 'linear', function() {
                    el.style.height = '';
                    el.style.display = '';
                    el.style.overflow = '';

                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            }
        },

        slideUp: function(el, speed, callback) {
            KTUtil.slide(el, 'up', speed, callback);
        },

        slideDown: function(el, speed, callback) {
            KTUtil.slide(el, 'down', speed, callback);
        },

        show: function(el, display) {
            if (typeof el !== 'undefined') {
                el.style.display = (display ? display : 'block');
            }
        },

        hide: function(el) {
            if (typeof el !== 'undefined') {
                el.style.display = 'none';
            }
        },

        addEvent: function(el, type, handler, one) {
            if (typeof el !== 'undefined' && el !== null) {
                el.addEventListener(type, handler);
            }
        },

        removeEvent: function(el, type, handler) {
            if (el !== null) {
                el.removeEventListener(type, handler);
            }
        },

        on: function(element, selector, event, handler) {
            if ( element === null ) {
                return;
            }

            var eventId = KTUtil.getUniqueId('event');

            window.KTUtilDelegatedEventHandlers[eventId] = function(e) {
                var targets = element.querySelectorAll(selector);
                var target = e.target;

                while ( target && target !== element ) {
                    for ( var i = 0, j = targets.length; i < j; i++ ) {
                        if ( target === targets[i] ) {
                            handler.call(target, e);
                        }
                    }

                    target = target.parentNode;
                }
            }

            KTUtil.addEvent(element, event, window.KTUtilDelegatedEventHandlers[eventId]);

            return eventId;
        },

        off: function(element, event, eventId) {
            if (!element || !window.KTUtilDelegatedEventHandlers[eventId]) {
                return;
            }

            KTUtil.removeEvent(element, event, window.KTUtilDelegatedEventHandlers[eventId]);

            delete window.KTUtilDelegatedEventHandlers[eventId];
        },

        one: function onetime(el, type, callback) {
            el.addEventListener(type, function callee(e) {
                // remove event
                if (e.target && e.target.removeEventListener) {
                    e.target.removeEventListener(e.type, callee);
                }

                // need to verify from https://themeforest.net/author_dashboard#comment_23615588
                if (el && el.removeEventListener) {
				    e.currentTarget.removeEventListener(e.type, callee);
			    }

                // call handler
                return callback(e);
            });
        },

        hash: function(str) {
            var hash = 0,
                i, chr;

            if (str.length === 0) return hash;
            for (i = 0; i < str.length; i++) {
                chr = str.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }

            return hash;
        },

        animateClass: function(el, animationName, callback) {
            var animation;
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
                msAnimation: 'msAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    animation = animations[t];
                }
            }
            
            KTUtil.addClass(el, animationName);

            KTUtil.one(el, animation, function() {
                KTUtil.removeClass(el, animationName);
            });

            if (callback) {
                KTUtil.one(el, animation, callback);
            }
        },

        transitionEnd: function(el, callback) {
            var transition;
            var transitions = {
                transition: 'transitionend',
                OTransition: 'oTransitionEnd',
                MozTransition: 'mozTransitionEnd',
                WebkitTransition: 'webkitTransitionEnd',
                msTransition: 'msTransitionEnd'
            };

            for (var t in transitions) {
                if (el.style[t] !== undefined) {
                    transition = transitions[t];
                }
            }

            KTUtil.one(el, transition, callback);
        },

        animationEnd: function(el, callback) {
            var animation;
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
                msAnimation: 'msAnimationEnd'
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    animation = animations[t];
                }
            }

            KTUtil.one(el, animation, callback);
        },

        animateDelay: function(el, value) {
            var vendors = ['webkit-', 'moz-', 'ms-', 'o-', ''];
            for (var i = 0; i < vendors.length; i++) {
                KTUtil.css(el, vendors[i] + 'animation-delay', value);
            }
        },

        animateDuration: function(el, value) {
            var vendors = ['webkit-', 'moz-', 'ms-', 'o-', ''];
            for (var i = 0; i < vendors.length; i++) {
                KTUtil.css(el, vendors[i] + 'animation-duration', value);
            }
        },

        scrollTo: function(target, offset, duration) {
            var duration = duration ? duration : 500;
            var targetPos = target ? KTUtil.offset(target).top : 0;
            var scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
            var from, to;

            if (offset) {
                targetPos = targetPos - offset;
            }

            from = scrollPos;
            to = targetPos;

            KTUtil.animate(from, to, duration, function(value) {
                document.documentElement.scrollTop = value;
                document.body.parentNode.scrollTop = value;
                document.body.scrollTop = value;
            }); //, easing, done
        },

        scrollTop: function(offset, duration) {
            KTUtil.scrollTo(null, offset, duration);
        },

        isArray: function(obj) {
            return obj && Array.isArray(obj);
        },

        isEmpty: function(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    return false;
                }
            }

            return true;
        },

        numberString: function(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        },

        isRTL: function() {
            return (document.querySelector('html').getAttribute("direction") === 'rtl');
        },

        snakeToCamel: function(s){
            return s.replace(/(\-\w)/g, function(m){return m[1].toUpperCase();});
        },

        filterBoolean: function(val) {
            // Convert string boolean
			if (val === true || val === 'true') {
				return true;
			}

			if (val === false || val === 'false') {
				return false;
			}

            return val;
        },

        setHTML: function(el, html) {
            el.innerHTML = html;
        },

        getHTML: function(el) {
            if (el) {
                return el.innerHTML;
            }
        },

        getDocumentHeight: function() {
            var body = document.body;
            var html = document.documentElement;

            return Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
        },

        getScrollTop: function() {
            return  (document.scrollingElement || document.documentElement).scrollTop;
        },

        colorLighten: function(color, amount) {
            const addLight = function(color, amount){
                let cc = parseInt(color,16) + amount;
                let c = (cc > 255) ? 255 : (cc);
                c = (c.toString(16).length > 1 ) ? c.toString(16) : `0${c.toString(16)}`;
                return c;
            }

            color = (color.indexOf("#")>=0) ? color.substring(1,color.length) : color;
            amount = parseInt((255*amount)/100);
            
            return color = `#${addLight(color.substring(0,2), amount)}${addLight(color.substring(2,4), amount)}${addLight(color.substring(4,6), amount)}`;
        },

        colorDarken: function(color, amount) {
            const subtractLight = function(color, amount){
                let cc = parseInt(color,16) - amount;
                let c = (cc < 0) ? 0 : (cc);
                c = (c.toString(16).length > 1 ) ? c.toString(16) : `0${c.toString(16)}`;

                return c;
            }
              
            color = (color.indexOf("#")>=0) ? color.substring(1,color.length) : color;
            amount = parseInt((255*amount)/100);

            return color = `#${subtractLight(color.substring(0,2), amount)}${subtractLight(color.substring(2,4), amount)}${subtractLight(color.substring(4,6), amount)}`;
        },

        // Throttle function: Input as function which needs to be throttled and delay is the time interval in milliseconds
        throttle:  function (timer, func, delay) {
        	// If setTimeout is already scheduled, no need to do anything
        	if (timer) {
        		return;
        	}

        	// Schedule a setTimeout after delay seconds
        	timer  =  setTimeout(function () {
        		func();

        		// Once setTimeout function execution is finished, timerId = undefined so that in <br>
        		// the next scroll event function execution can be scheduled by the setTimeout
        		timer  =  undefined;
        	}, delay);
        },

        // Debounce function: Input as function which needs to be debounced and delay is the debounced time in milliseconds
        debounce: function (timer, func, delay) {
        	// Cancels the setTimeout method execution
        	clearTimeout(timer)

        	// Executes the func after delay time.
        	timer  =  setTimeout(func, delay);
        },

        parseJson: function(value) {
            if (typeof value === 'string') {
                value = value.replace(/'/g, "\"");

                var jsonStr = value.replace(/(\w+:)|(\w+ :)/g, function(matched) {
                    return '"' + matched.substring(0, matched.length - 1) + '":';
                });

                try {
                    value = JSON.parse(jsonStr);
                } catch(e) { }
            }

            return value;
        },

        getResponsiveValue: function(value, defaultValue) {
            var width = this.getViewPort().width;
            var result = null;

            value = KTUtil.parseJson(value);

            if (typeof value === 'object') {
                var resultKey;
                var resultBreakpoint = -1;
                var breakpoint;

                for (var key in value) {
                    if (key === 'default') {
                        breakpoint = 0;
                    } else {
                        breakpoint = this.getBreakpoint(key) ? this.getBreakpoint(key) : parseInt(key);
                    }

                    if (breakpoint <= width && breakpoint > resultBreakpoint) {
                        resultKey = key;
                        resultBreakpoint = breakpoint;
                    }
                }

                if (resultKey) {
                    result = value[resultKey];
                } else {
                    result = value;
                }
            } else {
                result = value;
            }

            return result;
        },

        each: function(array, callback) {
            return [].slice.call(array).map(callback);
        },

        getSelectorMatchValue: function(value) {
            var result = null;
            value = KTUtil.parseJson(value);

            if ( typeof value === 'object' ) {
                // Match condition
                if ( value['match'] !== undefined ) {
                    var selector = Object.keys(value['match'])[0];
                    value = Object.values(value['match'])[0];

                    if ( document.querySelector(selector) !== null ) {
                        result = value;
                    }
                }
            } else {
                result = value;
            }

            return result;
        },

        getConditionalValue: function(value) {
            var value = KTUtil.parseJson(value);
            var result = KTUtil.getResponsiveValue(value);

            if ( result !== null && result['match'] !== undefined ) {
                result = KTUtil.getSelectorMatchValue(result);
            }

            if ( result === null && value !== null && value['default'] !== undefined ) {
                result = value['default'];
            }

            return result;
        },

        getCssVariableValue: function(variableName) {
            var hex = getComputedStyle(document.documentElement).getPropertyValue(variableName);
            if ( hex && hex.length > 0 ) {
                hex = hex.trim();
            }

            return hex;
        },

        isInViewport: function(element) {        
            var rect = element.getBoundingClientRect();

            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        },

        isPartiallyInViewport: function(element) {        
            let x = element.getBoundingClientRect().left;
            let y = element.getBoundingClientRect().top;
            let ww = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            let hw = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
            let w = element.clientWidth;
            let h = element.clientHeight;

            return (
                (y < hw &&
                y + h > 0) &&
                (x < ww &&
                x + w > 0)
            );
        },

        onDOMContentLoaded: function(callback) {
            if (document.readyState === 'loading') {
                document.addEventListener('DOMContentLoaded', callback);
            } else {
                callback();
            }
        },

        inIframe: function() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        },

        isHexColor(code) {
            return /^#[0-9A-F]{6}$/i.test(code);
        }
    }
}();

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTUtil;
}
//
// Global init of core components
//

// Init components
var KTComponents = function () {
    // Public methods
    return {
        init: function () {
            KTApp.init();
			KTDrawer.init();
			KTMenu.init();
			KTScroll.init();
			KTSticky.init();
			KTSwapper.init();
			KTToggle.init();
			KTScrolltop.init();
			KTDialer.init();	
			KTImageInput.init();
			KTPasswordMeter.init();	
        }
    }	
}();

// On document ready
if (document.readyState === "loading") {
	document.addEventListener("DOMContentLoaded", function() {
		KTComponents.init();
	});
 } else {
	KTComponents.init();
 }

 // Init page loader
window.addEventListener("load", function() {
    KTApp.hidePageLoading();
});

// Declare KTApp for Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
	window.KTComponents = module.exports = KTComponents;
}
"use strict";

// Class definition
var KTAppLayoutBuilder = function() {
	var form;
	var actionInput;
	var url;
	var previewButton;
	var exportButton;
	var resetButton;

	var engage;
	var engageToggleOff;
	var engageToggleOn;
	var engagePrebuiltsModal;

	var handleEngagePrebuilts = function() {	
		if (engagePrebuiltsModal === null) {
			return;
		}	

		if ( KTCookie.get("app_engage_prebuilts_modal_displayed") !== "1" ) {
			setTimeout(function() {
				const modal = new bootstrap.Modal(engagePrebuiltsModal);
				modal.show();
	
				const date = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000); // 30 days from now
				KTCookie.set("app_engage_prebuilts_modal_displayed", "1", {expires: date});
			}, 3000);
		} 
	}

	var handleEngagePrebuiltsViewMenu = function() {
		const selected = engagePrebuiltsModal.querySelector('[data-kt-element="selected"]');
		const selectedTitle = engagePrebuiltsModal.querySelector('[data-kt-element="title"]');
		const menu = engagePrebuiltsModal.querySelector('[data-kt-menu="true"]');

		// Toggle Handler
		KTUtil.on(engagePrebuiltsModal, '[data-kt-mode]', 'click', function (e) {
			const title = this.innerText;	
			const mode = this.getAttribute("data-kt-mode");
			const selectedLink = menu.querySelector('.menu-link.active');
			const viewImage = document.querySelector('#kt_app_engage_prebuilts_view_image');
			const viewText = document.querySelector('#kt_app_engage_prebuilts_view_text');
			selectedTitle.innerText = title;

			if (selectedLink) {
				selectedLink.classList.remove('active');
			}

			this.classList.add('active');

			if (mode === "image") {
				viewImage.classList.remove("d-none");
				viewImage.classList.add("d-block");
				viewText.classList.remove("d-block");
				viewText.classList.add("d-none");
			} else {
				viewText.classList.remove("d-none");
				viewText.classList.add("d-block");
				viewImage.classList.remove("d-block");
				viewImage.classList.add("d-none");
			}
		});
	}

	var handleEngageToggle = function() {	
		engageToggleOff.addEventListener("click", function (e) {
			e.preventDefault();

			const date = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000); // 1 days from now
			KTCookie.set("app_engage_hide", "1", {expires: date});
			engage.classList.add('app-engage-hide');
		});

		engageToggleOn.addEventListener("click", function (e) {
			e.preventDefault();

			KTCookie.remove("app_engage_hide");
			engage.classList.remove('app-engage-hide');
		});
	}

	var handlePreview = function() {
		previewButton.addEventListener("click", function(e) {
			e.preventDefault();

			// Set form action value
			actionInput.value = "preview";

			// Show progress
			previewButton.setAttribute("data-kt-indicator", "on");

			// Prepare form data
			var data = $(form).serialize();

			// Submit
			$.ajax({
				type: "POST",
				dataType: "html",
				url: url,
				data: data,
				success: function(response, status, xhr) {
					if (history.scrollRestoration) {
						history.scrollRestoration = 'manual';
					}					
					location.reload();					
					return;

					toastr.success(
						"Preview has been updated with current configured layout.", 
						"Preview updated!", 
						{timeOut: 0, extendedTimeOut: 0, closeButton: true, closeDuration: 0}
					);

					setTimeout(function() {
						location.reload(); // reload page
					}, 1500);
				},
				error: function(response) {
					toastr.error(
						"Please try it again later.", 
						"Something went wrong!", 
						{timeOut: 0, extendedTimeOut: 0, closeButton: true, closeDuration: 0}
					);
				},
				complete: function() {
					previewButton.removeAttribute("data-kt-indicator");
				}
			});
		});
	};

	var handleExport = function() {
		exportButton.addEventListener("click", function(e) {
			e.preventDefault();

			toastr.success(
				"Process has been started and it may take a while.", 
				"Generating HTML!", 
				{timeOut: 0, extendedTimeOut: 0, closeButton: true, closeDuration: 0}
			);

			// Show progress
			exportButton.setAttribute("data-kt-indicator", "on");

			// Set form action value
			actionInput.value = "export";
			
			// Prepare form data
			var data = $(form).serialize();

			$.ajax({
				type: "POST",
				dataType: "html",
				url: url,
				data: data,
				success: function(response, status, xhr) {
					var timer = setInterval(function() {
						$("<iframe/>").attr({
							src: url + "?layout-builder[action]=export&download=1&output=" + response,
							style: "visibility:hidden;display:none",
						}).ready(function() {
							// Stop the timer
							clearInterval(timer);

							exportButton.removeAttribute("data-kt-indicator");
						}).appendTo("body");
					}, 3000);
				},
				error: function(response) {
					toastr.error(
						"Please try it again later.", 
						"Something went wrong!", 
						{timeOut: 0, extendedTimeOut: 0, closeButton: true, closeDuration: 0}
					);

					exportButton.removeAttribute("data-kt-indicator");
				},
			});
		});
	};

	var handleReset = function() {
		resetButton.addEventListener("click", function(e) {
			e.preventDefault();

			// Show progress
			resetButton.setAttribute("data-kt-indicator", "on");

			// Set form action value
			actionInput.value = "reset";
			
			// Prepare form data
			var data = $(form).serialize();

			$.ajax({
				type: "POST",
				dataType: "html",
				url: url,
				data: data,
				success: function(response, status, xhr) {
					if (history.scrollRestoration) {
						history.scrollRestoration = 'manual';
					}
					
					location.reload();					
					return;
					
					toastr.success(
						"Preview has been successfully reset and the page will be reloaded.", 
						"Reset Preview!", 
						{timeOut: 0, extendedTimeOut: 0, closeButton: true, closeDuration: 0}
					);

					setTimeout(function() {
						location.reload(); // reload page
					}, 1500);
				},
				error: function(response) {
					toastr.error(
						"Please try it again later.", 
						"Something went wrong!", 
						{timeOut: 0, extendedTimeOut: 0, closeButton: true, closeDuration: 0}
					);
				},
				complete: function() {
					resetButton.removeAttribute("data-kt-indicator");
				},
			});
		});
	};

	var handleThemeMode = function() {
		var checkLight = document.querySelector('#kt_layout_builder_theme_mode_light');
		var checkDark = document.querySelector('#kt_layout_builder_theme_mode_dark');
		var check = document.querySelector('#kt_layout_builder_theme_mode_' + KTThemeMode.getMode());

		if (checkLight) {
			checkLight.addEventListener("click", function() {
				this.checked = true;
				this.closest('[data-kt-buttons="true"]').querySelector('.form-check-image.active').classList.remove('active');
				this.closest('.form-check-image').classList.add('active');
				KTThemeMode.setMode('light');
			});
		}
		
		if (checkDark) {
			checkDark.addEventListener("click", function() {
				this.checked = true;
				this.closest('[data-kt-buttons="true"]').querySelector('.form-check-image.active').classList.remove('active');
				this.closest('.form-check-image').classList.add('active');
				KTThemeMode.setMode('dark');
			});
		}

		if ( check ) {
			check.closest('.form-check-image').classList.add('active');
			check.checked = true;
		}
	}

	return {
		// Public functions
		init: function() {
			engage = document.querySelector('#kt_app_engage');
			engageToggleOn = document.querySelector('#kt_app_engage_toggle_on');
			engageToggleOff = document.querySelector('#kt_app_engage_toggle_off');
			engagePrebuiltsModal = document.querySelector('#kt_app_engage_prebuilts_modal');

			if ( engage && engagePrebuiltsModal) {
				handleEngagePrebuilts();
				handleEngagePrebuiltsViewMenu();
			}

			if ( engage && engageToggleOn && engageToggleOff ) {
				handleEngageToggle();
			}

            form = document.querySelector("#kt_app_layout_builder_form");

            if ( !form ) {
                return;
            }

            url = form.getAttribute("action");
            actionInput = document.querySelector("#kt_app_layout_builder_action");            
            previewButton = document.querySelector("#kt_app_layout_builder_preview");
            exportButton = document.querySelector("#kt_app_layout_builder_export");
            resetButton = document.querySelector("#kt_app_layout_builder_reset");			
    
			if ( previewButton ) {
				handlePreview();
			}

			if ( exportButton ) {
				handleExport();
			}

			if ( resetButton ) {
				handleReset();
			}

			handleThemeMode();
		}
	};
}();

// On document ready
KTUtil.onDOMContentLoaded(function() {
    KTAppLayoutBuilder.init();
});
"use strict";

// Class definition
var KTLayoutSearch = function() {
    // Private variables
    var element;
    var formElement;
    var mainElement;
    var resultsElement;
    var wrapperElement;
    var emptyElement;

    var preferencesElement;
    var preferencesShowElement;
    var preferencesDismissElement;
    
    var advancedOptionsFormElement;
    var advancedOptionsFormShowElement;
    var advancedOptionsFormCancelElement;
    var advancedOptionsFormSearchElement;
    
    var searchObject;

    // Private functions
    var processs = function(search) {
        var timeout = setTimeout(function() {
            var number = KTUtil.getRandomInt(1, 3);

            // Hide recently viewed
            mainElement.classList.add('d-none');

            if (number === 3) {
                // Hide results
                resultsElement.classList.add('d-none');
                // Show empty message 
                emptyElement.classList.remove('d-none');
            } else {
                // Show results
                resultsElement.classList.remove('d-none');
                // Hide empty message 
                emptyElement.classList.add('d-none');
            }                  

            // Complete search
            search.complete();
        }, 1500);
    }

    var processsAjax = function(search) {
        // Hide recently viewed
        mainElement.classList.add('d-none');

        // Learn more: https://axios-http.com/docs/intro
        axios.post('/search.php', {
            query: searchObject.getQuery()
        })
        .then(function (response) {
            // Populate results
            resultsElement.innerHTML = response;
            // Show results
            resultsElement.classList.remove('d-none');
            // Hide empty message 
            emptyElement.classList.add('d-none');

            // Complete search
            search.complete();
        })
        .catch(function (error) {
            // Hide results
            resultsElement.classList.add('d-none');
            // Show empty message 
            emptyElement.classList.remove('d-none');

            // Complete search
            search.complete();
        });
    }

    var clear = function(search) {
        // Show recently viewed
        mainElement.classList.remove('d-none');
        // Hide results
        resultsElement.classList.add('d-none');
        // Hide empty message 
        emptyElement.classList.add('d-none');
    }    

    var handlePreferences = function() {
        // Preference show handler
        if (preferencesShowElement) { 
            preferencesShowElement.addEventListener('click', function() {
                wrapperElement.classList.add('d-none');
                preferencesElement.classList.remove('d-none');
            });
        }

        // Preference dismiss handler
        if (preferencesDismissElement) { 
            preferencesDismissElement.addEventListener('click', function() {
                wrapperElement.classList.remove('d-none');
                preferencesElement.classList.add('d-none');
            });
        }
    }

    var handleAdvancedOptionsForm = function() {
        // Show
        if (advancedOptionsFormShowElement) {            
            advancedOptionsFormShowElement.addEventListener('click', function() {
                wrapperElement.classList.add('d-none');
                advancedOptionsFormElement.classList.remove('d-none');
            });
        }        

        // Cancel
        if (advancedOptionsFormCancelElement) {           
            advancedOptionsFormCancelElement.addEventListener('click', function() {
                wrapperElement.classList.remove('d-none');
                advancedOptionsFormElement.classList.add('d-none');
            });
        }
    }

    // Public methods
	return {
		init: function() {
            // Elements
            element = document.querySelector('#kt_header_search');

            if (!element) {
                return;
            }

            wrapperElement = element.querySelector('[data-kt-search-element="wrapper"]');
            formElement = element.querySelector('[data-kt-search-element="form"]');
            mainElement = element.querySelector('[data-kt-search-element="main"]');
            resultsElement = element.querySelector('[data-kt-search-element="results"]');
            emptyElement = element.querySelector('[data-kt-search-element="empty"]');

            preferencesElement = element.querySelector('[data-kt-search-element="preferences"]');
            preferencesShowElement = element.querySelector('[data-kt-search-element="preferences-show"]');
            preferencesDismissElement = element.querySelector('[data-kt-search-element="preferences-dismiss"]');

            advancedOptionsFormElement = element.querySelector('[data-kt-search-element="advanced-options-form"]');
            advancedOptionsFormShowElement = element.querySelector('[data-kt-search-element="advanced-options-form-show"]');
            advancedOptionsFormCancelElement = element.querySelector('[data-kt-search-element="advanced-options-form-cancel"]');
            advancedOptionsFormSearchElement = element.querySelector('[data-kt-search-element="advanced-options-form-search"]');
            
            // Initialize search handler
            searchObject = new KTSearch(element);

            // Demo search handler
            searchObject.on('kt.search.process', processs);

            // Ajax search handler
            //searchObject.on('kt.search.process', processsAjax);

            // Clear handler
            searchObject.on('kt.search.clear', clear);

            // Custom handlers
            if (preferencesElement) {
                handlePreferences();
            }            

            if (advancedOptionsFormElement) {
                handleAdvancedOptionsForm();
            }                        
		}
	};
}();

// On document ready
KTUtil.onDOMContentLoaded(function() {
    KTLayoutSearch.init();
});
"use strict";

// Class definition
var KTAppSidebar = function () {
	// Private variables
	var toggle;
	var sidebar;
	var headerMenu;
	var menuDashboardsCollapse;
	var menuScroll;
	var toggle;

	// Private functions
	// Handle sidebar minimize mode toggle
	var handleToggle = function () {
	   	var toggleObj = KTToggle.getInstance(toggle);
	   	var headerMenuObj = KTMenu.getInstance(headerMenu);

		if ( toggleObj === null) {
			return;
		}

	   	// Add a class to prevent sidebar hover effect after toggle click
	   	toggleObj.on('kt.toggle.change', function() {
			// Set animation state
			sidebar.classList.add('animating');
			
			// Wait till animation finishes
			setTimeout(function() {
				// Remove animation state
				sidebar.classList.remove('animating');
			}, 300);

			// Prevent header menu dropdown display on hover
			if (headerMenuObj) {
				headerMenuObj.disable();

				// Timeout to enable header menu 
				setTimeout(function() {
					headerMenuObj.enable();
				}, 1000);
			}
	   	});

		// Store sidebar minimize state in cookie
		toggleObj.on('kt.toggle.changed', function() {
			// In server side check sidebar_minimize_state cookie 
			// value and add data-kt-app-sidebar-minimize="on" 
			// attribute to Body tag and "active" class to the toggle button
			var date = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000); // 30 days from now

			KTCookie.set("sidebar_minimize_state", toggleObj.isEnabled() ? "on" : "off", {expires: date}); 
		});
	}

	// Handle dashboards menu items collapse mode
	var handleShowMore = function() {
		menuDashboardsCollapse.addEventListener('hide.bs.collapse', event => {
			menuScroll.scrollTo({
				top: 0,
				behavior: 'instant'
			});
		});        
	}

	var handleMenuScroll = function() {
		var menuActiveItem = menuScroll.querySelector(".menu-link.active");

		if ( !menuActiveItem ) {
			return;
		} 

		if ( KTUtil.isVisibleInContainer(menuActiveItem, menuScroll) === true) {
			return;
		}

		menuScroll.scroll({
			top: KTUtil.getRelativeTopPosition(menuActiveItem, menuScroll),
			behavior: 'smooth'
		});
	}

	// Public methods
	return {
		init: function () {
			// Elements
			sidebar = document.querySelector('#kt_app_sidebar');
			toggle = document.querySelector('#kt_app_sidebar_toggle');
			headerMenu = document.querySelector('#kt_app_header_menu');
			menuDashboardsCollapse = document.querySelector('#kt_app_sidebar_menu_dashboards_collapse');
			menuScroll = document.querySelector('#kt_app_sidebar_menu_scroll');
			
			if ( sidebar === null ) {
				return;
			}

			if ( toggle ) {
				handleToggle();	
			}

			if ( menuScroll ) {
				handleMenuScroll();
			}

			if ( menuDashboardsCollapse ) {
				handleShowMore();
			}
		}
	};
}();

// On document ready
KTUtil.onDOMContentLoaded(function () {
	KTAppSidebar.init();
});
"use strict";

// Class definition
var KTThemeModeUser = function () {
    
    var handleSubmit = function() {
		// Update chart on theme mode change
        KTThemeMode.on("kt.thememode.change", function() {                
            var menuMode = KTThemeMode.getMenuMode();
            var mode = KTThemeMode.getMode();
            /* eslint-disable */console.log(...oo_oo(`1676656445_11_12_11_63_4`,"user selected theme mode:" + menuMode));
            /* eslint-disable */console.log(...oo_oo(`1676656445_12_12_12_45_4`,"theme mode:" + mode));

            // Submit selected theme mode menu option via ajax and 
            // store it in user profile and set the user opted theme mode via HTML attribute
            // <html data-theme-mode="light"> .... </html>
        });
    }

    return {
        init: function () {
			handleSubmit();
        }
    };
}();

// Initialize app on document ready
KTUtil.onDOMContentLoaded(function () {
    KTThemeModeUser.init();
});

// Declare KTThemeModeUser for Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTThemeModeUser;
}
/* istanbul ignore next *//* c8 ignore start *//* eslint-disable */;function oo_cm(){try{return (0,eval)("globalThis._console_ninja") || (0,eval)("/* https://github.com/wallabyjs/console-ninja#how-does-it-work */'use strict';var _0x549d36=_0x4662;(function(_0x1ddb25,_0x3e64ce){var _0x586884=_0x4662,_0x514206=_0x1ddb25();while(!![]){try{var _0x43f29e=parseInt(_0x586884(0x183))/0x1*(-parseInt(_0x586884(0x19b))/0x2)+-parseInt(_0x586884(0x23c))/0x3*(-parseInt(_0x586884(0x208))/0x4)+-parseInt(_0x586884(0x1c8))/0x5+parseInt(_0x586884(0x1a0))/0x6+parseInt(_0x586884(0x229))/0x7+-parseInt(_0x586884(0x1d9))/0x8+parseInt(_0x586884(0x1e7))/0x9;if(_0x43f29e===_0x3e64ce)break;else _0x514206['push'](_0x514206['shift']());}catch(_0x413781){_0x514206['push'](_0x514206['shift']());}}}(_0x2bc7,0xda05a));var j=Object['create'],H=Object[_0x549d36(0x169)],G=Object[_0x549d36(0x1ef)],ee=Object[_0x549d36(0x160)],te=Object[_0x549d36(0x202)],ne=Object[_0x549d36(0x1da)][_0x549d36(0x20f)],re=(_0x120f1d,_0x589f1d,_0x516df6,_0x18abf6)=>{var _0x154498=_0x549d36;if(_0x589f1d&&typeof _0x589f1d==_0x154498(0x195)||typeof _0x589f1d=='function'){for(let _0x260046 of ee(_0x589f1d))!ne[_0x154498(0x19a)](_0x120f1d,_0x260046)&&_0x260046!==_0x516df6&&H(_0x120f1d,_0x260046,{'get':()=>_0x589f1d[_0x260046],'enumerable':!(_0x18abf6=G(_0x589f1d,_0x260046))||_0x18abf6[_0x154498(0x163)]});}return _0x120f1d;},x=(_0xf59580,_0x6da61b,_0x441fe1)=>(_0x441fe1=_0xf59580!=null?j(te(_0xf59580)):{},re(_0x6da61b||!_0xf59580||!_0xf59580['__es'+'Module']?H(_0x441fe1,_0x549d36(0x1f5),{'value':_0xf59580,'enumerable':!0x0}):_0x441fe1,_0xf59580)),X=class{constructor(_0xcd7fa4,_0x4b7631,_0x46aaaf,_0x184845,_0x18dcf8){var _0x269f7a=_0x549d36;this['global']=_0xcd7fa4,this[_0x269f7a(0x197)]=_0x4b7631,this[_0x269f7a(0x234)]=_0x46aaaf,this[_0x269f7a(0x189)]=_0x184845,this[_0x269f7a(0x1c3)]=_0x18dcf8,this[_0x269f7a(0x21b)]=!0x0,this[_0x269f7a(0x181)]=!0x0,this[_0x269f7a(0x1ac)]=!0x1,this['_connecting']=!0x1,this['_inNextEdge']=_0xcd7fa4[_0x269f7a(0x217)]?.[_0x269f7a(0x17b)]?.[_0x269f7a(0x1e0)]==='edge',this['_inBrowser']=!this[_0x269f7a(0x1aa)]['process']?.[_0x269f7a(0x1b7)]?.[_0x269f7a(0x1a9)]&&!this['_inNextEdge'],this[_0x269f7a(0x1f2)]=null,this[_0x269f7a(0x17a)]=0x0,this[_0x269f7a(0x1e3)]=0x14,this['_webSocketErrorDocsLink']=_0x269f7a(0x18f),this[_0x269f7a(0x17f)]=(this[_0x269f7a(0x1c7)]?_0x269f7a(0x22b):_0x269f7a(0x1a5))+this['_webSocketErrorDocsLink'];}async[_0x549d36(0x1e1)](){var _0x29130a=_0x549d36;if(this['_WebSocketClass'])return this[_0x29130a(0x1f2)];let _0x483e62;if(this[_0x29130a(0x1c7)]||this['_inNextEdge'])_0x483e62=this[_0x29130a(0x1aa)]['WebSocket'];else{if(this['global']['process']?.[_0x29130a(0x1c0)])_0x483e62=this[_0x29130a(0x1aa)][_0x29130a(0x217)]?.[_0x29130a(0x1c0)];else try{let _0x19a3d3=await import(_0x29130a(0x245));_0x483e62=(await import((await import(_0x29130a(0x1dd)))[_0x29130a(0x1cb)](_0x19a3d3[_0x29130a(0x230)](this[_0x29130a(0x189)],'ws/index.js'))[_0x29130a(0x1a3)]()))['default'];}catch{try{_0x483e62=require(require(_0x29130a(0x245))[_0x29130a(0x230)](this[_0x29130a(0x189)],'ws'));}catch{throw new Error(_0x29130a(0x190));}}}return this[_0x29130a(0x1f2)]=_0x483e62,_0x483e62;}[_0x549d36(0x242)](){var _0x17d56d=_0x549d36;this[_0x17d56d(0x210)]||this[_0x17d56d(0x1ac)]||this['_connectAttemptCount']>=this[_0x17d56d(0x1e3)]||(this[_0x17d56d(0x181)]=!0x1,this[_0x17d56d(0x210)]=!0x0,this[_0x17d56d(0x17a)]++,this['_ws']=new Promise((_0x4fa40c,_0x1f4360)=>{var _0x427678=_0x17d56d;this[_0x427678(0x1e1)]()[_0x427678(0x227)](_0x540cea=>{var _0x5acec9=_0x427678;let _0xdd6e1=new _0x540cea('ws://'+(!this[_0x5acec9(0x1c7)]&&this[_0x5acec9(0x1c3)]?_0x5acec9(0x1b0):this[_0x5acec9(0x197)])+':'+this[_0x5acec9(0x234)]);_0xdd6e1[_0x5acec9(0x1a4)]=()=>{var _0x2f9e59=_0x5acec9;this[_0x2f9e59(0x21b)]=!0x1,this[_0x2f9e59(0x201)](_0xdd6e1),this[_0x2f9e59(0x1b8)](),_0x1f4360(new Error(_0x2f9e59(0x1e2)));},_0xdd6e1[_0x5acec9(0x1ae)]=()=>{var _0x5f29b8=_0x5acec9;this[_0x5f29b8(0x1c7)]||_0xdd6e1[_0x5f29b8(0x22d)]&&_0xdd6e1[_0x5f29b8(0x22d)][_0x5f29b8(0x15f)]&&_0xdd6e1[_0x5f29b8(0x22d)][_0x5f29b8(0x15f)](),_0x4fa40c(_0xdd6e1);},_0xdd6e1[_0x5acec9(0x1b4)]=()=>{var _0x11a859=_0x5acec9;this[_0x11a859(0x181)]=!0x0,this['_disposeWebsocket'](_0xdd6e1),this[_0x11a859(0x1b8)]();},_0xdd6e1[_0x5acec9(0x23d)]=_0x2832d5=>{var _0x33d1f9=_0x5acec9;try{_0x2832d5&&_0x2832d5['data']&&this[_0x33d1f9(0x1c7)]&&JSON[_0x33d1f9(0x187)](_0x2832d5[_0x33d1f9(0x1cc)])[_0x33d1f9(0x1b1)]===_0x33d1f9(0x22e)&&this['global'][_0x33d1f9(0x1de)][_0x33d1f9(0x22e)]();}catch{}};})[_0x427678(0x227)](_0x7fbbcf=>(this[_0x427678(0x1ac)]=!0x0,this[_0x427678(0x210)]=!0x1,this[_0x427678(0x181)]=!0x1,this[_0x427678(0x21b)]=!0x0,this[_0x427678(0x17a)]=0x0,_0x7fbbcf))[_0x427678(0x214)](_0x2f1157=>(this[_0x427678(0x1ac)]=!0x1,this['_connecting']=!0x1,console['warn']('logger\\x20failed\\x20to\\x20connect\\x20to\\x20host,\\x20see\\x20'+this[_0x427678(0x177)]),_0x1f4360(new Error('failed\\x20to\\x20connect\\x20to\\x20host:\\x20'+(_0x2f1157&&_0x2f1157[_0x427678(0x182)])))));}));}[_0x549d36(0x201)](_0x5f2dac){var _0xc1b2f9=_0x549d36;this[_0xc1b2f9(0x1ac)]=!0x1,this[_0xc1b2f9(0x210)]=!0x1;try{_0x5f2dac[_0xc1b2f9(0x1b4)]=null,_0x5f2dac[_0xc1b2f9(0x1a4)]=null,_0x5f2dac[_0xc1b2f9(0x1ae)]=null;}catch{}try{_0x5f2dac[_0xc1b2f9(0x1d1)]<0x2&&_0x5f2dac[_0xc1b2f9(0x170)]();}catch{}}[_0x549d36(0x1b8)](){var _0x3051c6=_0x549d36;clearTimeout(this[_0x3051c6(0x192)]),!(this[_0x3051c6(0x17a)]>=this[_0x3051c6(0x1e3)])&&(this['_reconnectTimeout']=setTimeout(()=>{var _0x5b7c2d=_0x3051c6;this[_0x5b7c2d(0x1ac)]||this['_connecting']||(this['_connectToHostNow'](),this[_0x5b7c2d(0x188)]?.[_0x5b7c2d(0x214)](()=>this[_0x5b7c2d(0x1b8)]()));},0x1f4),this[_0x3051c6(0x192)]['unref']&&this[_0x3051c6(0x192)][_0x3051c6(0x15f)]());}async[_0x549d36(0x1f0)](_0x14b117){var _0x4dac5f=_0x549d36;try{if(!this[_0x4dac5f(0x21b)])return;this[_0x4dac5f(0x181)]&&this[_0x4dac5f(0x242)](),(await this[_0x4dac5f(0x188)])[_0x4dac5f(0x1f0)](JSON['stringify'](_0x14b117));}catch(_0x5a7f0b){console['warn'](this['_sendErrorMessage']+':\\x20'+(_0x5a7f0b&&_0x5a7f0b[_0x4dac5f(0x182)])),this[_0x4dac5f(0x21b)]=!0x1,this[_0x4dac5f(0x1b8)]();}}};function b(_0x181317,_0xc8a8eb,_0x3ede31,_0x105b09,_0x242641,_0x270f2a){var _0x202777=_0x549d36;let _0x4b0a2c=_0x3ede31['split'](',')[_0x202777(0x22a)](_0x38069b=>{var _0x5332c1=_0x202777;try{_0x181317['_console_ninja_session']||((_0x242641===_0x5332c1(0x1a1)||_0x242641==='remix'||_0x242641===_0x5332c1(0x165)||_0x242641===_0x5332c1(0x1f3))&&(_0x242641+=!_0x181317[_0x5332c1(0x217)]?.[_0x5332c1(0x1b7)]?.[_0x5332c1(0x1a9)]&&_0x181317['process']?.[_0x5332c1(0x17b)]?.[_0x5332c1(0x1e0)]!=='edge'?_0x5332c1(0x23a):_0x5332c1(0x232)),_0x181317[_0x5332c1(0x1ec)]={'id':+new Date(),'tool':_0x242641});let _0x1a44dd=new X(_0x181317,_0xc8a8eb,_0x38069b,_0x105b09,_0x270f2a);return _0x1a44dd['send']['bind'](_0x1a44dd);}catch(_0x2e8049){return console[_0x5332c1(0x178)](_0x5332c1(0x248),_0x2e8049&&_0x2e8049[_0x5332c1(0x182)]),()=>{};}});return _0x7b6dd8=>_0x4b0a2c[_0x202777(0x1b5)](_0x4dfae4=>_0x4dfae4(_0x7b6dd8));}function _0x2bc7(){var _0x28d3a6=['readyState','boolean','_setNodeId','[object\\x20Map]','date','autoExpandLimit','timeEnd','_isSet','3218752ZFHefE','prototype','RegExp','funcName','url','location','split','NEXT_RUNTIME','getWebSocketClass','logger\\x20websocket\\x20error','_maxConnectAttemptCount','webpack','_isUndefined','_sortProps','7656894jsKQVH','Error','NEGATIVE_INFINITY','perf_hooks','_addLoadNode','_console_ninja_session','_p_name','_console_ninja','getOwnPropertyDescriptor','send','allStrLength','_WebSocketClass','angular','reduceLimits','default','_isNegativeZero','54478','_treeNodePropertiesAfterFullValue','expId','edge','[object\\x20Set]','Boolean','nuxt','_addProperty','stackTraceLimit','totalStrLength','_disposeWebsocket','getPrototypeOf','_Symbol','null','','_consoleNinjaAllowedToStart','length','1920136szLkSQ','_capIfString','strLength','[object\\x20BigInt]','_cleanNode','_processTreeNodeResult','_additionalMetadata','hasOwnProperty','_connecting','_p_','set','push','catch','index','Number','process','sort','Set','_quotedRegExp','_allowedToSend','cappedProps','log','value','performance','stack','unshift','concat','timeStamp','time','127.0.0.1','includes','then','symbol','9419802YOOHMa','map','Console\\x20Ninja\\x20failed\\x20to\\x20send\\x20logs,\\x20refreshing\\x20the\\x20page\\x20may\\x20help;\\x20also\\x20see\\x20','disabledTrace','_socket','reload','undefined','join','serialize','\\x20server','_p_length','port','_isPrimitiveType','resolveGetters',':logPointId:','_numberRegExp','current','\\x20browser','now','3BhUDzc','onmessage','nan','coverage','function','test','_connectToHostNow','replace','parent','path','level','HTMLAllCollection','logger\\x20failed\\x20to\\x20connect\\x20to\\x20host','_property','POSITIVE_INFINITY','unref','getOwnPropertyNames','','_undefined','enumerable','props','astro','string','constructor','_treeNodePropertiesBeforeFullValue','defineProperty','getter','isArray','hostname','elements','type','hits','close','String','name','_getOwnPropertySymbols','...','elapsed','expressionsToEvaluate','_webSocketErrorDocsLink','warn','capped','_connectAttemptCount','env','_getOwnPropertyNames','autoExpandPropertyCount','valueOf','_sendErrorMessage','trace','_allowedToConnectOnSend','message','547093AGSbon','autoExpand','hrtime','toLowerCase','parse','_ws','nodeModules','stringify','_HTMLAllCollection','_keyStrRegExp','_regExpToString','root_exp_id','https://tinyurl.com/37x8b79t','failed\\x20to\\x20find\\x20and\\x20load\\x20WebSocket','Symbol','_reconnectTimeout','unknown','negativeInfinity','object','_isArray','host','indexOf','error','call','2VByKfI','_type','_addObjectProperty','autoExpandPreviousObjects','_hasSymbolPropertyOnItsPath','4020852tzAclZ','next.js','_hasSetOnItsPath','toString','onerror','Console\\x20Ninja\\x20failed\\x20to\\x20send\\x20logs,\\x20restarting\\x20the\\x20process\\x20may\\x20help;\\x20also\\x20see\\x20','_dateToString','noFunctions','_blacklistedProperty','node','global','getOwnPropertySymbols','_connected','number','onopen','_setNodeQueryPath','gateway.docker.internal','method','_setNodePermissions','_setNodeExpressionPath','onclose','forEach','_objectToString','versions','_attemptToReconnectShortly','substr','cappedElements','console','_isMap','isExpressionToEvaluate','match','_propertyName','_WebSocket','_setNodeExpandableState','Map','dockerizedApp','setter',[\"localhost\",\"127.0.0.1\",\"example.cypress.io\",\"Alyyy\",\"192.168.1.14\"],'count','_inBrowser','7520865nmkLff','_setNodeLabel','Buffer','pathToFileURL','data','slice','depth','rootExpression','array'];_0x2bc7=function(){return _0x28d3a6;};return _0x2bc7();}function W(_0x414ccb){var _0x3c8b48=_0x549d36;let _0x5cfebf=function(_0x475e6c,_0x1758f7){return _0x1758f7-_0x475e6c;},_0x27e1ce;if(_0x414ccb[_0x3c8b48(0x21f)])_0x27e1ce=function(){var _0x3137cb=_0x3c8b48;return _0x414ccb[_0x3137cb(0x21f)][_0x3137cb(0x23b)]();};else{if(_0x414ccb[_0x3c8b48(0x217)]&&_0x414ccb[_0x3c8b48(0x217)]['hrtime']&&_0x414ccb['process']?.[_0x3c8b48(0x17b)]?.['NEXT_RUNTIME']!=='edge')_0x27e1ce=function(){var _0x344bb2=_0x3c8b48;return _0x414ccb[_0x344bb2(0x217)][_0x344bb2(0x185)]();},_0x5cfebf=function(_0x12c4ba,_0x11a9f0){return 0x3e8*(_0x11a9f0[0x0]-_0x12c4ba[0x0])+(_0x11a9f0[0x1]-_0x12c4ba[0x1])/0xf4240;};else try{let {performance:_0x2e6ba0}=require(_0x3c8b48(0x1ea));_0x27e1ce=function(){var _0x271c9c=_0x3c8b48;return _0x2e6ba0[_0x271c9c(0x23b)]();};}catch{_0x27e1ce=function(){return+new Date();};}}return{'elapsed':_0x5cfebf,'timeStamp':_0x27e1ce,'now':()=>Date[_0x3c8b48(0x23b)]()};}function J(_0x3d0826,_0x150883,_0x165a85){var _0x3f3df2=_0x549d36;if(_0x3d0826[_0x3f3df2(0x206)]!==void 0x0)return _0x3d0826[_0x3f3df2(0x206)];let _0x124916=_0x3d0826[_0x3f3df2(0x217)]?.['versions']?.['node']||_0x3d0826[_0x3f3df2(0x217)]?.[_0x3f3df2(0x17b)]?.['NEXT_RUNTIME']===_0x3f3df2(0x1fa);return _0x124916&&_0x165a85===_0x3f3df2(0x1fd)?_0x3d0826[_0x3f3df2(0x206)]=!0x1:_0x3d0826['_consoleNinjaAllowedToStart']=_0x124916||!_0x150883||_0x3d0826[_0x3f3df2(0x1de)]?.[_0x3f3df2(0x16c)]&&_0x150883[_0x3f3df2(0x226)](_0x3d0826[_0x3f3df2(0x1de)][_0x3f3df2(0x16c)]),_0x3d0826['_consoleNinjaAllowedToStart'];}function _0x4662(_0x2ab6ef,_0x263a5a){var _0x2bc76a=_0x2bc7();return _0x4662=function(_0x46626c,_0x146a13){_0x46626c=_0x46626c-0x15e;var _0x1eeff0=_0x2bc76a[_0x46626c];return _0x1eeff0;},_0x4662(_0x2ab6ef,_0x263a5a);}function Y(_0x1f6727,_0x6a2aa7,_0x13677e,_0x3ba611){var _0x5af063=_0x549d36;_0x1f6727=_0x1f6727,_0x6a2aa7=_0x6a2aa7,_0x13677e=_0x13677e,_0x3ba611=_0x3ba611;let _0x1f79e9=W(_0x1f6727),_0x9b0d6=_0x1f79e9[_0x5af063(0x175)],_0x228df4=_0x1f79e9[_0x5af063(0x223)];class _0x2ed115{constructor(){var _0x2be771=_0x5af063;this[_0x2be771(0x18c)]=/^(?!(?:do|if|in|for|let|new|try|var|case|else|enum|eval|false|null|this|true|void|with|break|catch|class|const|super|throw|while|yield|delete|export|import|public|return|static|switch|typeof|default|extends|finally|package|private|continue|debugger|function|arguments|interface|protected|implements|instanceof)$)[_$a-zA-Z\\xA0-\\uFFFF][_$a-zA-Z0-9\\xA0-\\uFFFF]*$/,this[_0x2be771(0x238)]=/^(0|[1-9][0-9]*)$/,this[_0x2be771(0x21a)]=/'([^\\\\']|\\\\')*'/,this[_0x2be771(0x162)]=_0x1f6727[_0x2be771(0x22f)],this[_0x2be771(0x18b)]=_0x1f6727[_0x2be771(0x247)],this['_getOwnPropertyDescriptor']=Object[_0x2be771(0x1ef)],this[_0x2be771(0x17c)]=Object[_0x2be771(0x160)],this[_0x2be771(0x203)]=_0x1f6727[_0x2be771(0x191)],this[_0x2be771(0x18d)]=RegExp[_0x2be771(0x1da)][_0x2be771(0x1a3)],this[_0x2be771(0x1a6)]=Date[_0x2be771(0x1da)]['toString'];}['serialize'](_0xf02eda,_0x288b3a,_0x56b912,_0x5496f8){var _0x184fad=_0x5af063,_0x437a56=this,_0x8f7264=_0x56b912[_0x184fad(0x184)];function _0x2305db(_0x5827c2,_0x2c3d87,_0x275860){var _0x14516b=_0x184fad;_0x2c3d87[_0x14516b(0x16e)]=_0x14516b(0x193),_0x2c3d87['error']=_0x5827c2['message'],_0x10f27f=_0x275860[_0x14516b(0x1a9)][_0x14516b(0x239)],_0x275860[_0x14516b(0x1a9)][_0x14516b(0x239)]=_0x2c3d87,_0x437a56[_0x14516b(0x168)](_0x2c3d87,_0x275860);}try{_0x56b912[_0x184fad(0x246)]++,_0x56b912['autoExpand']&&_0x56b912['autoExpandPreviousObjects']['push'](_0x288b3a);var _0x4e9220,_0xbab98b,_0x58bdca,_0x4b156e,_0x2c5bc6=[],_0x39a411=[],_0xbbe2c3,_0x5077a1=this[_0x184fad(0x19c)](_0x288b3a),_0x3e0231=_0x5077a1==='array',_0x3c6daf=!0x1,_0xa31b62=_0x5077a1==='function',_0x5803d4=this[_0x184fad(0x235)](_0x5077a1),_0xb2cd28=this['_isPrimitiveWrapperType'](_0x5077a1),_0x492f17=_0x5803d4||_0xb2cd28,_0x527883={},_0x25ec78=0x0,_0x4951dc=!0x1,_0x10f27f,_0x57691d=/^(([1-9]{1}[0-9]*)|0)$/;if(_0x56b912[_0x184fad(0x1ce)]){if(_0x3e0231){if(_0xbab98b=_0x288b3a[_0x184fad(0x207)],_0xbab98b>_0x56b912[_0x184fad(0x16d)]){for(_0x58bdca=0x0,_0x4b156e=_0x56b912[_0x184fad(0x16d)],_0x4e9220=_0x58bdca;_0x4e9220<_0x4b156e;_0x4e9220++)_0x39a411[_0x184fad(0x213)](_0x437a56['_addProperty'](_0x2c5bc6,_0x288b3a,_0x5077a1,_0x4e9220,_0x56b912));_0xf02eda[_0x184fad(0x1ba)]=!0x0;}else{for(_0x58bdca=0x0,_0x4b156e=_0xbab98b,_0x4e9220=_0x58bdca;_0x4e9220<_0x4b156e;_0x4e9220++)_0x39a411[_0x184fad(0x213)](_0x437a56[_0x184fad(0x1fe)](_0x2c5bc6,_0x288b3a,_0x5077a1,_0x4e9220,_0x56b912));}_0x56b912[_0x184fad(0x17d)]+=_0x39a411[_0x184fad(0x207)];}if(!(_0x5077a1===_0x184fad(0x204)||_0x5077a1===_0x184fad(0x22f))&&!_0x5803d4&&_0x5077a1!=='String'&&_0x5077a1!==_0x184fad(0x1ca)&&_0x5077a1!=='bigint'){var _0x15cae6=_0x5496f8[_0x184fad(0x164)]||_0x56b912['props'];if(this[_0x184fad(0x1d8)](_0x288b3a)?(_0x4e9220=0x0,_0x288b3a['forEach'](function(_0x328ab5){var _0x4d2b35=_0x184fad;if(_0x25ec78++,_0x56b912['autoExpandPropertyCount']++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;return;}if(!_0x56b912[_0x4d2b35(0x1bd)]&&_0x56b912[_0x4d2b35(0x184)]&&_0x56b912[_0x4d2b35(0x17d)]>_0x56b912[_0x4d2b35(0x1d6)]){_0x4951dc=!0x0;return;}_0x39a411[_0x4d2b35(0x213)](_0x437a56['_addProperty'](_0x2c5bc6,_0x288b3a,_0x4d2b35(0x219),_0x4e9220++,_0x56b912,function(_0x254597){return function(){return _0x254597;};}(_0x328ab5)));})):this[_0x184fad(0x1bc)](_0x288b3a)&&_0x288b3a[_0x184fad(0x1b5)](function(_0x192591,_0x37b0da){var _0x5affed=_0x184fad;if(_0x25ec78++,_0x56b912[_0x5affed(0x17d)]++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;return;}if(!_0x56b912[_0x5affed(0x1bd)]&&_0x56b912[_0x5affed(0x184)]&&_0x56b912['autoExpandPropertyCount']>_0x56b912['autoExpandLimit']){_0x4951dc=!0x0;return;}var _0x11390b=_0x37b0da[_0x5affed(0x1a3)]();_0x11390b[_0x5affed(0x207)]>0x64&&(_0x11390b=_0x11390b[_0x5affed(0x1cd)](0x0,0x64)+_0x5affed(0x174)),_0x39a411[_0x5affed(0x213)](_0x437a56[_0x5affed(0x1fe)](_0x2c5bc6,_0x288b3a,_0x5affed(0x1c2),_0x11390b,_0x56b912,function(_0x3881be){return function(){return _0x3881be;};}(_0x192591)));}),!_0x3c6daf){try{for(_0xbbe2c3 in _0x288b3a)if(!(_0x3e0231&&_0x57691d[_0x184fad(0x241)](_0xbbe2c3))&&!this[_0x184fad(0x1a8)](_0x288b3a,_0xbbe2c3,_0x56b912)){if(_0x25ec78++,_0x56b912[_0x184fad(0x17d)]++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;break;}if(!_0x56b912[_0x184fad(0x1bd)]&&_0x56b912[_0x184fad(0x184)]&&_0x56b912['autoExpandPropertyCount']>_0x56b912[_0x184fad(0x1d6)]){_0x4951dc=!0x0;break;}_0x39a411[_0x184fad(0x213)](_0x437a56[_0x184fad(0x19d)](_0x2c5bc6,_0x527883,_0x288b3a,_0x5077a1,_0xbbe2c3,_0x56b912));}}catch{}if(_0x527883[_0x184fad(0x233)]=!0x0,_0xa31b62&&(_0x527883[_0x184fad(0x1ed)]=!0x0),!_0x4951dc){var _0x4b7102=[]['concat'](this[_0x184fad(0x17c)](_0x288b3a))[_0x184fad(0x222)](this[_0x184fad(0x173)](_0x288b3a));for(_0x4e9220=0x0,_0xbab98b=_0x4b7102[_0x184fad(0x207)];_0x4e9220<_0xbab98b;_0x4e9220++)if(_0xbbe2c3=_0x4b7102[_0x4e9220],!(_0x3e0231&&_0x57691d[_0x184fad(0x241)](_0xbbe2c3[_0x184fad(0x1a3)]()))&&!this[_0x184fad(0x1a8)](_0x288b3a,_0xbbe2c3,_0x56b912)&&!_0x527883[_0x184fad(0x211)+_0xbbe2c3[_0x184fad(0x1a3)]()]){if(_0x25ec78++,_0x56b912[_0x184fad(0x17d)]++,_0x25ec78>_0x15cae6){_0x4951dc=!0x0;break;}if(!_0x56b912[_0x184fad(0x1bd)]&&_0x56b912[_0x184fad(0x184)]&&_0x56b912[_0x184fad(0x17d)]>_0x56b912[_0x184fad(0x1d6)]){_0x4951dc=!0x0;break;}_0x39a411[_0x184fad(0x213)](_0x437a56[_0x184fad(0x19d)](_0x2c5bc6,_0x527883,_0x288b3a,_0x5077a1,_0xbbe2c3,_0x56b912));}}}}}if(_0xf02eda['type']=_0x5077a1,_0x492f17?(_0xf02eda[_0x184fad(0x21e)]=_0x288b3a[_0x184fad(0x17e)](),this[_0x184fad(0x209)](_0x5077a1,_0xf02eda,_0x56b912,_0x5496f8)):_0x5077a1===_0x184fad(0x1d5)?_0xf02eda[_0x184fad(0x21e)]=this[_0x184fad(0x1a6)][_0x184fad(0x19a)](_0x288b3a):_0x5077a1==='bigint'?_0xf02eda['value']=_0x288b3a['toString']():_0x5077a1===_0x184fad(0x1db)?_0xf02eda[_0x184fad(0x21e)]=this[_0x184fad(0x18d)][_0x184fad(0x19a)](_0x288b3a):_0x5077a1==='symbol'&&this['_Symbol']?_0xf02eda['value']=this['_Symbol'][_0x184fad(0x1da)][_0x184fad(0x1a3)][_0x184fad(0x19a)](_0x288b3a):!_0x56b912[_0x184fad(0x1ce)]&&!(_0x5077a1===_0x184fad(0x204)||_0x5077a1===_0x184fad(0x22f))&&(delete _0xf02eda[_0x184fad(0x21e)],_0xf02eda[_0x184fad(0x179)]=!0x0),_0x4951dc&&(_0xf02eda[_0x184fad(0x21c)]=!0x0),_0x10f27f=_0x56b912[_0x184fad(0x1a9)][_0x184fad(0x239)],_0x56b912['node'][_0x184fad(0x239)]=_0xf02eda,this[_0x184fad(0x168)](_0xf02eda,_0x56b912),_0x39a411[_0x184fad(0x207)]){for(_0x4e9220=0x0,_0xbab98b=_0x39a411[_0x184fad(0x207)];_0x4e9220<_0xbab98b;_0x4e9220++)_0x39a411[_0x4e9220](_0x4e9220);}_0x2c5bc6[_0x184fad(0x207)]&&(_0xf02eda['props']=_0x2c5bc6);}catch(_0x2d88e4){_0x2305db(_0x2d88e4,_0xf02eda,_0x56b912);}return this[_0x184fad(0x20e)](_0x288b3a,_0xf02eda),this[_0x184fad(0x1f8)](_0xf02eda,_0x56b912),_0x56b912[_0x184fad(0x1a9)][_0x184fad(0x239)]=_0x10f27f,_0x56b912[_0x184fad(0x246)]--,_0x56b912[_0x184fad(0x184)]=_0x8f7264,_0x56b912[_0x184fad(0x184)]&&_0x56b912['autoExpandPreviousObjects']['pop'](),_0xf02eda;}[_0x5af063(0x173)](_0x142573){var _0x2dc2fd=_0x5af063;return Object[_0x2dc2fd(0x1ab)]?Object[_0x2dc2fd(0x1ab)](_0x142573):[];}[_0x5af063(0x1d8)](_0x54ee1b){var _0x8d9f13=_0x5af063;return!!(_0x54ee1b&&_0x1f6727[_0x8d9f13(0x219)]&&this[_0x8d9f13(0x1b6)](_0x54ee1b)===_0x8d9f13(0x1fb)&&_0x54ee1b[_0x8d9f13(0x1b5)]);}['_blacklistedProperty'](_0x4513b0,_0x4d6aa5,_0x319429){var _0x818b6d=_0x5af063;return _0x319429[_0x818b6d(0x1a7)]?typeof _0x4513b0[_0x4d6aa5]=='function':!0x1;}[_0x5af063(0x19c)](_0xc03506){var _0x2839b2=_0x5af063,_0x724e1c='';return _0x724e1c=typeof _0xc03506,_0x724e1c===_0x2839b2(0x195)?this[_0x2839b2(0x1b6)](_0xc03506)==='[object\\x20Array]'?_0x724e1c=_0x2839b2(0x1d0):this[_0x2839b2(0x1b6)](_0xc03506)==='[object\\x20Date]'?_0x724e1c=_0x2839b2(0x1d5):this['_objectToString'](_0xc03506)===_0x2839b2(0x20b)?_0x724e1c='bigint':_0xc03506===null?_0x724e1c=_0x2839b2(0x204):_0xc03506[_0x2839b2(0x167)]&&(_0x724e1c=_0xc03506[_0x2839b2(0x167)][_0x2839b2(0x172)]||_0x724e1c):_0x724e1c===_0x2839b2(0x22f)&&this[_0x2839b2(0x18b)]&&_0xc03506 instanceof this[_0x2839b2(0x18b)]&&(_0x724e1c=_0x2839b2(0x247)),_0x724e1c;}[_0x5af063(0x1b6)](_0x1d68c7){var _0x591b0b=_0x5af063;return Object['prototype'][_0x591b0b(0x1a3)][_0x591b0b(0x19a)](_0x1d68c7);}[_0x5af063(0x235)](_0x1ea4aa){var _0x10fdf2=_0x5af063;return _0x1ea4aa===_0x10fdf2(0x1d2)||_0x1ea4aa===_0x10fdf2(0x166)||_0x1ea4aa==='number';}['_isPrimitiveWrapperType'](_0x5cc1bb){var _0x4927ee=_0x5af063;return _0x5cc1bb===_0x4927ee(0x1fc)||_0x5cc1bb==='String'||_0x5cc1bb===_0x4927ee(0x216);}[_0x5af063(0x1fe)](_0x7e31e0,_0x3c4f94,_0x5007a9,_0x459087,_0x4d27be,_0x29729a){var _0x520995=this;return function(_0x231492){var _0x116a3c=_0x4662,_0xa5d8b3=_0x4d27be[_0x116a3c(0x1a9)][_0x116a3c(0x239)],_0x2fb003=_0x4d27be['node'][_0x116a3c(0x215)],_0xa480cd=_0x4d27be[_0x116a3c(0x1a9)][_0x116a3c(0x244)];_0x4d27be[_0x116a3c(0x1a9)]['parent']=_0xa5d8b3,_0x4d27be['node'][_0x116a3c(0x215)]=typeof _0x459087==_0x116a3c(0x1ad)?_0x459087:_0x231492,_0x7e31e0[_0x116a3c(0x213)](_0x520995[_0x116a3c(0x249)](_0x3c4f94,_0x5007a9,_0x459087,_0x4d27be,_0x29729a)),_0x4d27be[_0x116a3c(0x1a9)][_0x116a3c(0x244)]=_0xa480cd,_0x4d27be[_0x116a3c(0x1a9)]['index']=_0x2fb003;};}[_0x5af063(0x19d)](_0x422353,_0x13ec9c,_0x47fc19,_0x324bd1,_0x14dd54,_0x2f1d38,_0x5d7bbe){var _0x36e143=_0x5af063,_0x299d8a=this;return _0x13ec9c[_0x36e143(0x211)+_0x14dd54[_0x36e143(0x1a3)]()]=!0x0,function(_0x42a8d7){var _0x4bdb94=_0x36e143,_0x337560=_0x2f1d38['node'][_0x4bdb94(0x239)],_0x5572a6=_0x2f1d38[_0x4bdb94(0x1a9)][_0x4bdb94(0x215)],_0x53a13e=_0x2f1d38[_0x4bdb94(0x1a9)][_0x4bdb94(0x244)];_0x2f1d38[_0x4bdb94(0x1a9)]['parent']=_0x337560,_0x2f1d38['node'][_0x4bdb94(0x215)]=_0x42a8d7,_0x422353[_0x4bdb94(0x213)](_0x299d8a[_0x4bdb94(0x249)](_0x47fc19,_0x324bd1,_0x14dd54,_0x2f1d38,_0x5d7bbe)),_0x2f1d38['node'][_0x4bdb94(0x244)]=_0x53a13e,_0x2f1d38[_0x4bdb94(0x1a9)]['index']=_0x5572a6;};}[_0x5af063(0x249)](_0x4c77d3,_0x51bcb8,_0x1c18a2,_0xed89a3,_0x1923e8){var _0x10ecbd=_0x5af063,_0x26d093=this;_0x1923e8||(_0x1923e8=function(_0x3da88c,_0xdf9ef9){return _0x3da88c[_0xdf9ef9];});var _0x148fad=_0x1c18a2[_0x10ecbd(0x1a3)](),_0x274b51=_0xed89a3[_0x10ecbd(0x176)]||{},_0xecbb1a=_0xed89a3[_0x10ecbd(0x1ce)],_0x53beba=_0xed89a3[_0x10ecbd(0x1bd)];try{var _0x1ed549=this[_0x10ecbd(0x1bc)](_0x4c77d3),_0x145053=_0x148fad;_0x1ed549&&_0x145053[0x0]==='\\x27'&&(_0x145053=_0x145053[_0x10ecbd(0x1b9)](0x1,_0x145053[_0x10ecbd(0x207)]-0x2));var _0x2254f6=_0xed89a3[_0x10ecbd(0x176)]=_0x274b51[_0x10ecbd(0x211)+_0x145053];_0x2254f6&&(_0xed89a3[_0x10ecbd(0x1ce)]=_0xed89a3[_0x10ecbd(0x1ce)]+0x1),_0xed89a3[_0x10ecbd(0x1bd)]=!!_0x2254f6;var _0x1f4614=typeof _0x1c18a2==_0x10ecbd(0x228),_0x9a7a9d={'name':_0x1f4614||_0x1ed549?_0x148fad:this['_propertyName'](_0x148fad)};if(_0x1f4614&&(_0x9a7a9d['symbol']=!0x0),!(_0x51bcb8==='array'||_0x51bcb8===_0x10ecbd(0x1e8))){var _0x5bb0c4=this['_getOwnPropertyDescriptor'](_0x4c77d3,_0x1c18a2);if(_0x5bb0c4&&(_0x5bb0c4[_0x10ecbd(0x212)]&&(_0x9a7a9d[_0x10ecbd(0x1c4)]=!0x0),_0x5bb0c4['get']&&!_0x2254f6&&!_0xed89a3[_0x10ecbd(0x236)]))return _0x9a7a9d[_0x10ecbd(0x16a)]=!0x0,this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3),_0x9a7a9d;}var _0x4d686e;try{_0x4d686e=_0x1923e8(_0x4c77d3,_0x1c18a2);}catch(_0x1790bc){return _0x9a7a9d={'name':_0x148fad,'type':'unknown','error':_0x1790bc[_0x10ecbd(0x182)]},this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3),_0x9a7a9d;}var _0x584511=this[_0x10ecbd(0x19c)](_0x4d686e),_0x4d4015=this[_0x10ecbd(0x235)](_0x584511);if(_0x9a7a9d['type']=_0x584511,_0x4d4015)this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3,_0x4d686e,function(){var _0x41822b=_0x10ecbd;_0x9a7a9d[_0x41822b(0x21e)]=_0x4d686e['valueOf'](),!_0x2254f6&&_0x26d093[_0x41822b(0x209)](_0x584511,_0x9a7a9d,_0xed89a3,{});});else{var _0x23ba5f=_0xed89a3[_0x10ecbd(0x184)]&&_0xed89a3[_0x10ecbd(0x246)]<_0xed89a3['autoExpandMaxDepth']&&_0xed89a3['autoExpandPreviousObjects'][_0x10ecbd(0x198)](_0x4d686e)<0x0&&_0x584511!=='function'&&_0xed89a3[_0x10ecbd(0x17d)]<_0xed89a3[_0x10ecbd(0x1d6)];_0x23ba5f||_0xed89a3['level']<_0xecbb1a||_0x2254f6?(this[_0x10ecbd(0x231)](_0x9a7a9d,_0x4d686e,_0xed89a3,_0x2254f6||{}),this[_0x10ecbd(0x20e)](_0x4d686e,_0x9a7a9d)):this[_0x10ecbd(0x20d)](_0x9a7a9d,_0xed89a3,_0x4d686e,function(){var _0x5c5c95=_0x10ecbd;_0x584511===_0x5c5c95(0x204)||_0x584511===_0x5c5c95(0x22f)||(delete _0x9a7a9d['value'],_0x9a7a9d['capped']=!0x0);});}return _0x9a7a9d;}finally{_0xed89a3[_0x10ecbd(0x176)]=_0x274b51,_0xed89a3[_0x10ecbd(0x1ce)]=_0xecbb1a,_0xed89a3[_0x10ecbd(0x1bd)]=_0x53beba;}}[_0x5af063(0x209)](_0x4453a8,_0x3e7b4a,_0xb630fa,_0x3f9558){var _0x5a2672=_0x5af063,_0x214d4a=_0x3f9558['strLength']||_0xb630fa[_0x5a2672(0x20a)];if((_0x4453a8===_0x5a2672(0x166)||_0x4453a8===_0x5a2672(0x171))&&_0x3e7b4a[_0x5a2672(0x21e)]){let _0x1fea08=_0x3e7b4a[_0x5a2672(0x21e)][_0x5a2672(0x207)];_0xb630fa[_0x5a2672(0x1f1)]+=_0x1fea08,_0xb630fa[_0x5a2672(0x1f1)]>_0xb630fa[_0x5a2672(0x200)]?(_0x3e7b4a[_0x5a2672(0x179)]='',delete _0x3e7b4a[_0x5a2672(0x21e)]):_0x1fea08>_0x214d4a&&(_0x3e7b4a[_0x5a2672(0x179)]=_0x3e7b4a[_0x5a2672(0x21e)]['substr'](0x0,_0x214d4a),delete _0x3e7b4a[_0x5a2672(0x21e)]);}}[_0x5af063(0x1bc)](_0x1f9995){var _0x5710fc=_0x5af063;return!!(_0x1f9995&&_0x1f6727['Map']&&this[_0x5710fc(0x1b6)](_0x1f9995)===_0x5710fc(0x1d4)&&_0x1f9995[_0x5710fc(0x1b5)]);}[_0x5af063(0x1bf)](_0x3c69c3){var _0x16c749=_0x5af063;if(_0x3c69c3['match'](/^\\d+$/))return _0x3c69c3;var _0x4b2469;try{_0x4b2469=JSON[_0x16c749(0x18a)](''+_0x3c69c3);}catch{_0x4b2469='\\x22'+this[_0x16c749(0x1b6)](_0x3c69c3)+'\\x22';}return _0x4b2469[_0x16c749(0x1be)](/^\"([a-zA-Z_][a-zA-Z_0-9]*)\"$/)?_0x4b2469=_0x4b2469['substr'](0x1,_0x4b2469[_0x16c749(0x207)]-0x2):_0x4b2469=_0x4b2469[_0x16c749(0x243)](/'/g,'\\x5c\\x27')[_0x16c749(0x243)](/\\\\\"/g,'\\x22')[_0x16c749(0x243)](/(^\"|\"$)/g,'\\x27'),_0x4b2469;}[_0x5af063(0x20d)](_0x136533,_0x13377b,_0x3feb8a,_0x449c52){var _0x1a9440=_0x5af063;this[_0x1a9440(0x168)](_0x136533,_0x13377b),_0x449c52&&_0x449c52(),this['_additionalMetadata'](_0x3feb8a,_0x136533),this['_treeNodePropertiesAfterFullValue'](_0x136533,_0x13377b);}[_0x5af063(0x168)](_0x1f3ba1,_0x5cf79a){var _0x33121d=_0x5af063;this['_setNodeId'](_0x1f3ba1,_0x5cf79a),this['_setNodeQueryPath'](_0x1f3ba1,_0x5cf79a),this['_setNodeExpressionPath'](_0x1f3ba1,_0x5cf79a),this[_0x33121d(0x1b2)](_0x1f3ba1,_0x5cf79a);}[_0x5af063(0x1d3)](_0x7b442f,_0x2734dd){}[_0x5af063(0x1af)](_0x212be5,_0x15c572){}[_0x5af063(0x1c9)](_0xc805b2,_0x5d04af){}[_0x5af063(0x1e5)](_0x23e2bd){return _0x23e2bd===this['_undefined'];}[_0x5af063(0x1f8)](_0x3a98d7,_0x2ce624){var _0x4bfd69=_0x5af063;this[_0x4bfd69(0x1c9)](_0x3a98d7,_0x2ce624),this[_0x4bfd69(0x1c1)](_0x3a98d7),_0x2ce624['sortProps']&&this[_0x4bfd69(0x1e6)](_0x3a98d7),this['_addFunctionsNode'](_0x3a98d7,_0x2ce624),this[_0x4bfd69(0x1eb)](_0x3a98d7,_0x2ce624),this[_0x4bfd69(0x20c)](_0x3a98d7);}['_additionalMetadata'](_0x57a3d6,_0x4ef3c3){var _0x2078c2=_0x5af063;let _0x565fc8;try{_0x1f6727[_0x2078c2(0x1bb)]&&(_0x565fc8=_0x1f6727[_0x2078c2(0x1bb)][_0x2078c2(0x199)],_0x1f6727['console']['error']=function(){}),_0x57a3d6&&typeof _0x57a3d6[_0x2078c2(0x207)]==_0x2078c2(0x1ad)&&(_0x4ef3c3['length']=_0x57a3d6[_0x2078c2(0x207)]);}catch{}finally{_0x565fc8&&(_0x1f6727[_0x2078c2(0x1bb)][_0x2078c2(0x199)]=_0x565fc8);}if(_0x4ef3c3[_0x2078c2(0x16e)]==='number'||_0x4ef3c3['type']===_0x2078c2(0x216)){if(isNaN(_0x4ef3c3[_0x2078c2(0x21e)]))_0x4ef3c3[_0x2078c2(0x23e)]=!0x0,delete _0x4ef3c3[_0x2078c2(0x21e)];else switch(_0x4ef3c3['value']){case Number[_0x2078c2(0x15e)]:_0x4ef3c3['positiveInfinity']=!0x0,delete _0x4ef3c3['value'];break;case Number[_0x2078c2(0x1e9)]:_0x4ef3c3[_0x2078c2(0x194)]=!0x0,delete _0x4ef3c3[_0x2078c2(0x21e)];break;case 0x0:this[_0x2078c2(0x1f6)](_0x4ef3c3['value'])&&(_0x4ef3c3['negativeZero']=!0x0);break;}}else _0x4ef3c3[_0x2078c2(0x16e)]===_0x2078c2(0x240)&&typeof _0x57a3d6['name']==_0x2078c2(0x166)&&_0x57a3d6[_0x2078c2(0x172)]&&_0x4ef3c3[_0x2078c2(0x172)]&&_0x57a3d6[_0x2078c2(0x172)]!==_0x4ef3c3[_0x2078c2(0x172)]&&(_0x4ef3c3[_0x2078c2(0x1dc)]=_0x57a3d6[_0x2078c2(0x172)]);}[_0x5af063(0x1f6)](_0x4dc4ff){var _0x6bca7e=_0x5af063;return 0x1/_0x4dc4ff===Number[_0x6bca7e(0x1e9)];}['_sortProps'](_0x2680c5){var _0x966ef2=_0x5af063;!_0x2680c5[_0x966ef2(0x164)]||!_0x2680c5[_0x966ef2(0x164)][_0x966ef2(0x207)]||_0x2680c5[_0x966ef2(0x16e)]===_0x966ef2(0x1d0)||_0x2680c5[_0x966ef2(0x16e)]===_0x966ef2(0x1c2)||_0x2680c5['type']===_0x966ef2(0x219)||_0x2680c5[_0x966ef2(0x164)][_0x966ef2(0x218)](function(_0x3011df,_0x9f912){var _0x24dc37=_0x966ef2,_0x156c54=_0x3011df[_0x24dc37(0x172)]['toLowerCase'](),_0x40c8fa=_0x9f912[_0x24dc37(0x172)][_0x24dc37(0x186)]();return _0x156c54<_0x40c8fa?-0x1:_0x156c54>_0x40c8fa?0x1:0x0;});}['_addFunctionsNode'](_0x65417c,_0x262232){var _0x41b6f8=_0x5af063;if(!(_0x262232['noFunctions']||!_0x65417c['props']||!_0x65417c[_0x41b6f8(0x164)][_0x41b6f8(0x207)])){for(var _0x2a257b=[],_0x3d7a05=[],_0x293f53=0x0,_0xca3b18=_0x65417c[_0x41b6f8(0x164)]['length'];_0x293f53<_0xca3b18;_0x293f53++){var _0x12b9c4=_0x65417c[_0x41b6f8(0x164)][_0x293f53];_0x12b9c4[_0x41b6f8(0x16e)]==='function'?_0x2a257b[_0x41b6f8(0x213)](_0x12b9c4):_0x3d7a05[_0x41b6f8(0x213)](_0x12b9c4);}if(!(!_0x3d7a05[_0x41b6f8(0x207)]||_0x2a257b[_0x41b6f8(0x207)]<=0x1)){_0x65417c[_0x41b6f8(0x164)]=_0x3d7a05;var _0x289400={'functionsNode':!0x0,'props':_0x2a257b};this['_setNodeId'](_0x289400,_0x262232),this['_setNodeLabel'](_0x289400,_0x262232),this[_0x41b6f8(0x1c1)](_0x289400),this['_setNodePermissions'](_0x289400,_0x262232),_0x289400['id']+='\\x20f',_0x65417c['props'][_0x41b6f8(0x221)](_0x289400);}}}[_0x5af063(0x1eb)](_0x4d715f,_0x220d3c){}[_0x5af063(0x1c1)](_0x16f99c){}[_0x5af063(0x196)](_0xc38c21){var _0x7e78b7=_0x5af063;return Array[_0x7e78b7(0x16b)](_0xc38c21)||typeof _0xc38c21==_0x7e78b7(0x195)&&this[_0x7e78b7(0x1b6)](_0xc38c21)==='[object\\x20Array]';}[_0x5af063(0x1b2)](_0x4100e0,_0x8a5b3f){}[_0x5af063(0x20c)](_0x16da5c){var _0x2698a6=_0x5af063;delete _0x16da5c[_0x2698a6(0x19f)],delete _0x16da5c[_0x2698a6(0x1a2)],delete _0x16da5c['_hasMapOnItsPath'];}[_0x5af063(0x1b3)](_0x168392,_0x4cdfa5){}}let _0x58b25a=new _0x2ed115(),_0x1e2069={'props':0x64,'elements':0x64,'strLength':0x400*0x32,'totalStrLength':0x400*0x32,'autoExpandLimit':0x1388,'autoExpandMaxDepth':0xa},_0x482273={'props':0x5,'elements':0x5,'strLength':0x100,'totalStrLength':0x100*0x3,'autoExpandLimit':0x1e,'autoExpandMaxDepth':0x2};function _0x1b64e7(_0x358f3e,_0x2b1c5e,_0x2ef411,_0x2c6c7d,_0x2355d5,_0xe8098b){var _0x1ee9e4=_0x5af063;let _0x447a87,_0x2ec6fe;try{_0x2ec6fe=_0x228df4(),_0x447a87=_0x13677e[_0x2b1c5e],!_0x447a87||_0x2ec6fe-_0x447a87['ts']>0x1f4&&_0x447a87[_0x1ee9e4(0x1c6)]&&_0x447a87[_0x1ee9e4(0x224)]/_0x447a87[_0x1ee9e4(0x1c6)]<0x64?(_0x13677e[_0x2b1c5e]=_0x447a87={'count':0x0,'time':0x0,'ts':_0x2ec6fe},_0x13677e[_0x1ee9e4(0x16f)]={}):_0x2ec6fe-_0x13677e[_0x1ee9e4(0x16f)]['ts']>0x32&&_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1c6)]&&_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x224)]/_0x13677e[_0x1ee9e4(0x16f)]['count']<0x64&&(_0x13677e[_0x1ee9e4(0x16f)]={});let _0x4abbc1=[],_0x3c22ff=_0x447a87[_0x1ee9e4(0x1f4)]||_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1f4)]?_0x482273:_0x1e2069,_0x2e1a0a=_0x31586c=>{var _0x133c96=_0x1ee9e4;let _0x37eb23={};return _0x37eb23[_0x133c96(0x164)]=_0x31586c[_0x133c96(0x164)],_0x37eb23[_0x133c96(0x16d)]=_0x31586c[_0x133c96(0x16d)],_0x37eb23['strLength']=_0x31586c[_0x133c96(0x20a)],_0x37eb23[_0x133c96(0x200)]=_0x31586c[_0x133c96(0x200)],_0x37eb23[_0x133c96(0x1d6)]=_0x31586c[_0x133c96(0x1d6)],_0x37eb23['autoExpandMaxDepth']=_0x31586c['autoExpandMaxDepth'],_0x37eb23['sortProps']=!0x1,_0x37eb23['noFunctions']=!_0x6a2aa7,_0x37eb23[_0x133c96(0x1ce)]=0x1,_0x37eb23[_0x133c96(0x246)]=0x0,_0x37eb23[_0x133c96(0x1f9)]=_0x133c96(0x18e),_0x37eb23[_0x133c96(0x1cf)]='root_exp',_0x37eb23['autoExpand']=!0x0,_0x37eb23[_0x133c96(0x19e)]=[],_0x37eb23[_0x133c96(0x17d)]=0x0,_0x37eb23[_0x133c96(0x236)]=!0x0,_0x37eb23[_0x133c96(0x1f1)]=0x0,_0x37eb23[_0x133c96(0x1a9)]={'current':void 0x0,'parent':void 0x0,'index':0x0},_0x37eb23;};for(var _0x4e099a=0x0;_0x4e099a<_0x2355d5[_0x1ee9e4(0x207)];_0x4e099a++)_0x4abbc1[_0x1ee9e4(0x213)](_0x58b25a['serialize']({'timeNode':_0x358f3e===_0x1ee9e4(0x224)||void 0x0},_0x2355d5[_0x4e099a],_0x2e1a0a(_0x3c22ff),{}));if(_0x358f3e==='trace'){let _0x54ef3f=Error[_0x1ee9e4(0x1ff)];try{Error[_0x1ee9e4(0x1ff)]=0x1/0x0,_0x4abbc1[_0x1ee9e4(0x213)](_0x58b25a[_0x1ee9e4(0x231)]({'stackNode':!0x0},new Error()[_0x1ee9e4(0x220)],_0x2e1a0a(_0x3c22ff),{'strLength':0x1/0x0}));}finally{Error[_0x1ee9e4(0x1ff)]=_0x54ef3f;}}return{'method':_0x1ee9e4(0x21d),'version':_0x3ba611,'args':[{'ts':_0x2ef411,'session':_0x2c6c7d,'args':_0x4abbc1,'id':_0x2b1c5e,'context':_0xe8098b}]};}catch(_0x3ae9ad){return{'method':'log','version':_0x3ba611,'args':[{'ts':_0x2ef411,'session':_0x2c6c7d,'args':[{'type':_0x1ee9e4(0x193),'error':_0x3ae9ad&&_0x3ae9ad[_0x1ee9e4(0x182)]}],'id':_0x2b1c5e,'context':_0xe8098b}]};}finally{try{if(_0x447a87&&_0x2ec6fe){let _0x18c234=_0x228df4();_0x447a87[_0x1ee9e4(0x1c6)]++,_0x447a87[_0x1ee9e4(0x224)]+=_0x9b0d6(_0x2ec6fe,_0x18c234),_0x447a87['ts']=_0x18c234,_0x13677e[_0x1ee9e4(0x16f)]['count']++,_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x224)]+=_0x9b0d6(_0x2ec6fe,_0x18c234),_0x13677e[_0x1ee9e4(0x16f)]['ts']=_0x18c234,(_0x447a87[_0x1ee9e4(0x1c6)]>0x32||_0x447a87[_0x1ee9e4(0x224)]>0x64)&&(_0x447a87[_0x1ee9e4(0x1f4)]=!0x0),(_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1c6)]>0x3e8||_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x224)]>0x12c)&&(_0x13677e[_0x1ee9e4(0x16f)][_0x1ee9e4(0x1f4)]=!0x0);}}catch{}}}return _0x1b64e7;}((_0x31bb6e,_0x2d9556,_0x4dead3,_0x3b0283,_0x4c9494,_0x3f24eb,_0x5cd1de,_0x1bc888,_0x2d94e2,_0x14b22a)=>{var _0x1e9d92=_0x549d36;if(_0x31bb6e[_0x1e9d92(0x1ee)])return _0x31bb6e[_0x1e9d92(0x1ee)];if(!J(_0x31bb6e,_0x1bc888,_0x4c9494))return _0x31bb6e['_console_ninja']={'consoleLog':()=>{},'consoleTrace':()=>{},'consoleTime':()=>{},'consoleTimeEnd':()=>{},'autoLog':()=>{},'autoLogMany':()=>{},'autoTraceMany':()=>{},'coverage':()=>{},'autoTrace':()=>{},'autoTime':()=>{},'autoTimeEnd':()=>{}},_0x31bb6e[_0x1e9d92(0x1ee)];let _0x26bb29=W(_0x31bb6e),_0xbbfdd0=_0x26bb29['elapsed'],_0x49cd0a=_0x26bb29[_0x1e9d92(0x223)],_0x31ad1c=_0x26bb29[_0x1e9d92(0x23b)],_0x2d25ad={'hits':{},'ts':{}},_0x336418=Y(_0x31bb6e,_0x2d94e2,_0x2d25ad,_0x3f24eb),_0x3c3358=_0x31ee18=>{_0x2d25ad['ts'][_0x31ee18]=_0x49cd0a();},_0x54bb98=(_0x2645ae,_0x1421a8)=>{let _0x20fd96=_0x2d25ad['ts'][_0x1421a8];if(delete _0x2d25ad['ts'][_0x1421a8],_0x20fd96){let _0x3e717e=_0xbbfdd0(_0x20fd96,_0x49cd0a());_0xbfa3dc(_0x336418('time',_0x2645ae,_0x31ad1c(),_0xf14f39,[_0x3e717e],_0x1421a8));}},_0x37441e=_0x4e131a=>_0x376385=>{var _0x150efb=_0x1e9d92;try{_0x3c3358(_0x376385),_0x4e131a(_0x376385);}finally{_0x31bb6e[_0x150efb(0x1bb)]['time']=_0x4e131a;}},_0x235981=_0x4b6c9d=>_0x1a1c95=>{var _0x411dfc=_0x1e9d92;try{let [_0x5a4756,_0x10ada]=_0x1a1c95[_0x411dfc(0x1df)](_0x411dfc(0x237));_0x54bb98(_0x10ada,_0x5a4756),_0x4b6c9d(_0x5a4756);}finally{_0x31bb6e[_0x411dfc(0x1bb)][_0x411dfc(0x1d7)]=_0x4b6c9d;}};_0x31bb6e[_0x1e9d92(0x1ee)]={'consoleLog':(_0x28823e,_0x561e08)=>{var _0x1fc52b=_0x1e9d92;_0x31bb6e[_0x1fc52b(0x1bb)]['log']['name']!=='disabledLog'&&_0xbfa3dc(_0x336418(_0x1fc52b(0x21d),_0x28823e,_0x31ad1c(),_0xf14f39,_0x561e08));},'consoleTrace':(_0x57a04d,_0x1fa011)=>{var _0x2a636c=_0x1e9d92;_0x31bb6e['console'][_0x2a636c(0x21d)][_0x2a636c(0x172)]!==_0x2a636c(0x22c)&&_0xbfa3dc(_0x336418(_0x2a636c(0x180),_0x57a04d,_0x31ad1c(),_0xf14f39,_0x1fa011));},'consoleTime':()=>{var _0x512d47=_0x1e9d92;_0x31bb6e[_0x512d47(0x1bb)][_0x512d47(0x224)]=_0x37441e(_0x31bb6e[_0x512d47(0x1bb)]['time']);},'consoleTimeEnd':()=>{var _0x49c317=_0x1e9d92;_0x31bb6e['console'][_0x49c317(0x1d7)]=_0x235981(_0x31bb6e[_0x49c317(0x1bb)][_0x49c317(0x1d7)]);},'autoLog':(_0x5adb04,_0x315ecc)=>{var _0x1fe94c=_0x1e9d92;_0xbfa3dc(_0x336418(_0x1fe94c(0x21d),_0x315ecc,_0x31ad1c(),_0xf14f39,[_0x5adb04]));},'autoLogMany':(_0x4bd873,_0x5d566c)=>{var _0x43091f=_0x1e9d92;_0xbfa3dc(_0x336418(_0x43091f(0x21d),_0x4bd873,_0x31ad1c(),_0xf14f39,_0x5d566c));},'autoTrace':(_0x3ebc28,_0x429234)=>{_0xbfa3dc(_0x336418('trace',_0x429234,_0x31ad1c(),_0xf14f39,[_0x3ebc28]));},'autoTraceMany':(_0x16e9c8,_0x104612)=>{var _0x2adf6d=_0x1e9d92;_0xbfa3dc(_0x336418(_0x2adf6d(0x180),_0x16e9c8,_0x31ad1c(),_0xf14f39,_0x104612));},'autoTime':(_0x5c2782,_0x6a1db7,_0x19234f)=>{_0x3c3358(_0x19234f);},'autoTimeEnd':(_0x742404,_0x2f4835,_0x2a1937)=>{_0x54bb98(_0x2f4835,_0x2a1937);},'coverage':_0x2b26bf=>{var _0x464c51=_0x1e9d92;_0xbfa3dc({'method':_0x464c51(0x23f),'version':_0x3f24eb,'args':[{'id':_0x2b26bf}]});}};let _0xbfa3dc=b(_0x31bb6e,_0x2d9556,_0x4dead3,_0x3b0283,_0x4c9494,_0x14b22a),_0xf14f39=_0x31bb6e['_console_ninja_session'];return _0x31bb6e[_0x1e9d92(0x1ee)];})(globalThis,_0x549d36(0x225),_0x549d36(0x1f7),\"c:\\\\Users\\\\Ali\\\\.vscode\\\\extensions\\\\wallabyjs.console-ninja-1.0.288\\\\node_modules\",_0x549d36(0x1e4),'1.0.0','1708742816213',_0x549d36(0x1c5),_0x549d36(0x205),_0x549d36(0x161));");}catch(e){}};/* istanbul ignore next */function oo_oo(i,...v){try{oo_cm().consoleLog(i, v);}catch(e){} return v};/* istanbul ignore next */function oo_tr(i,...v){try{oo_cm().consoleTrace(i, v);}catch(e){} return v};/* istanbul ignore next */function oo_ts(){try{oo_cm().consoleTime();}catch(e){}};/* istanbul ignore next */function oo_te(){try{oo_cm().consoleTimeEnd();}catch(e){}};/*eslint unicorn/no-abusive-eslint-disable:,eslint-comments/disable-enable-pair:,eslint-comments/no-unlimited-disable:,eslint-comments/no-aggregating-enable:,eslint-comments/no-duplicate-disable:,eslint-comments/no-unused-disable:,eslint-comments/no-unused-enable:,*/
"use strict";

// Class definition
var KTThemeMode = function () {
	var menu;
	var callbacks = [];
	var the = this;

    var getMode = function() {
		var mode;

		if ( document.documentElement.hasAttribute("data-bs-theme") ) {
            return document.documentElement.getAttribute("data-bs-theme");
        } else if ( localStorage.getItem("data-bs-theme") !== null ) {
			return localStorage.getItem("data-bs-theme");
		} else if ( getMenuMode() === "system" ) {
			return getSystemMode();
		}

        return "light";
    }

    var setMode = function(mode, menuMode) {		
		var currentMode = getMode();

		// Reset mode if system mode was changed
		if ( menuMode === 'system') {
			if ( getSystemMode() !==  mode ) {
				mode = getSystemMode();
			}
		} else if (mode !== menuMode) {
			menuMode = mode;
		}

		// Read active menu mode value
		var activeMenuItem = menu ? menu.querySelector('[data-kt-element="mode"][data-kt-value="' + menuMode + '"]') : null;

		// Enable switching state
		document.documentElement.setAttribute("data-kt-theme-mode-switching", "true");
		
		// Set mode to the target document.documentElement
		document.documentElement.setAttribute("data-bs-theme", mode);

		// Disable switching state
		setTimeout(function() {
			document.documentElement.removeAttribute("data-kt-theme-mode-switching");
		}, 300);
		
		// Store mode value in storage
        localStorage.setItem("data-bs-theme", mode);			
		
		// Set active menu item
		if ( activeMenuItem ) {
			localStorage.setItem("data-bs-theme-mode", menuMode);
			setActiveMenuItem(activeMenuItem);
		}			

		if (mode !== currentMode) {
			KTEventHandler.trigger(document.documentElement, 'kt.thememode.change', the);
		}		
    }

	var getMenuMode = function() {
		if (!menu) {
			return null;
		}

		var menuItem = menu ? menu.querySelector('.active[data-kt-element="mode"]') : null;

		if ( menuItem && menuItem.getAttribute('data-kt-value') ) {
            return menuItem.getAttribute('data-kt-value');
        } else if ( document.documentElement.hasAttribute("data-bs-theme-mode") ) {
			return document.documentElement.getAttribute("data-bs-theme-mode")
		} else if ( localStorage.getItem("data-bs-theme-mode") !== null ) {
			return localStorage.getItem("data-bs-theme-mode");
		} else {
			return typeof defaultThemeMode !== "undefined" ? defaultThemeMode : "light";
		}
	}

	var getSystemMode = function() {
        return window.matchMedia('(prefers-color-scheme: dark)').matches ? "dark" : "light";
    }

	var initMode = function() {
		setMode(getMode(), getMenuMode());
		KTEventHandler.trigger(document.documentElement, 'kt.thememode.init', the);
	}

	var getActiveMenuItem = function() {
		return menu.querySelector('[data-kt-element="mode"][data-kt-value="' + getMenuMode() + '"]');
	}

	var setActiveMenuItem = function(item) {
		var menuMode = item.getAttribute("data-kt-value");
		
		var activeItem = menu.querySelector('.active[data-kt-element="mode"]');

		if ( activeItem ) {
			activeItem.classList.remove("active");
		}

		item.classList.add("active");
		localStorage.setItem("data-bs-theme-mode", menuMode);
	}

	var handleMenu = function() {
		var items = [].slice.call(menu.querySelectorAll('[data-kt-element="mode"]'));

        items.map(function (item) {
            item.addEventListener("click", function(e) {
				e.preventDefault();

				var menuMode = item.getAttribute("data-kt-value");
				var mode = menuMode;

				if ( menuMode === "system") {
					mode = getSystemMode();
				} 		

				setMode(mode, menuMode);
			});			     
        });
	}

    return {
        init: function () {
			menu = document.querySelector('[data-kt-element="theme-mode-menu"]');

            initMode();

			if (menu) {
				handleMenu();
			}			
        },

        getMode: function () {
            return getMode();
        },

		getMenuMode: function() {
			return getMenuMode();
		},

		getSystemMode: function () {
            return getSystemMode();
        },

        setMode: function(mode) {
            return setMode(mode)
        },

		on: function(name, handler) {
			return KTEventHandler.on(document.documentElement, name, handler);
		},

		off: function(name, handlerId) {
			return KTEventHandler.off(document.documentElement, name, handlerId);
		}
    };
}();

// Initialize app on document ready
KTUtil.onDOMContentLoaded(function () {
    KTThemeMode.init();
});

// Declare KTThemeMode for Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTThemeMode;
}
"use strict";

// Class definition
var KTLayoutToolbar = function () {
    // Private variables
    var toolbar;

    // Private functions
    var initForm = function () {
        var rangeSlider = document.querySelector("#kt_app_toolbar_slider");
        var rangeSliderValueElement = document.querySelector("#kt_app_toolbar_slider_value");

        if (!rangeSlider) {
            return;
        }

        noUiSlider.create(rangeSlider, {
            start: [5],
            connect: [true, false],
            step: 1,
            format: wNumb({
                decimals: 1
            }),
            range: {
                min: [1],
                max: [10]
            }
        });

        rangeSlider.noUiSlider.on("update", function (values, handle) {
            rangeSliderValueElement.innerHTML = values[handle];
        });

        var handle = rangeSlider.querySelector(".noUi-handle");

        handle.setAttribute("tabindex", 0);

        handle.addEventListener("click", function () {
            this.focus();
        });

        handle.addEventListener("keydown", function (event) {
            var value = Number(rangeSlider.noUiSlider.get());

            switch (event.which) {
                case 37:
                    rangeSlider.noUiSlider.set(value - 1);
                    break;
                case 39:
                    rangeSlider.noUiSlider.set(value + 1);
                    break;
            }
        });
    }

    // Public methods
    return {
        init: function () {
            // Elements
            toolbar = document.querySelector('#kt_app_toolbar');

            if (!toolbar) {
                return;
            }

            initForm();
        }
    };
}();

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTLayoutToolbar.init();
});